//
//  SignUpViewController.m
//  Cliicker
//
//  Created by Santanu Mondal on 28/12/18.
//  Copyright © 2018 Santanu Mondal. All rights reserved.
//

#import "SignUpViewController.h"
#import "JJMaterialTextfield.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    NSString *strText1 = @"Already a user? Sign in now !";
    NSString *redText = @"Sign in now !";
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:strText1];
    [attString addAttribute:(NSString*)NSForegroundColorAttributeName
                      value:[UIColor colorWithRed:208.0/255.0 green:14.0/255.0 blue:49.0/255.0 alpha:1.0]
                      range:(NSRange){16,[redText length]}];
    
    self.lblSigninText.attributedText = attString;
    
    self.btnRegister.layer.borderWidth = 0.5;
    self.btnRegister.layer.cornerRadius = 5.0;
    self.btnRegister.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.btnVerificationCode.layer.borderWidth = 0.5;
    self.btnVerificationCode.layer.cornerRadius = 5.0;
    self.btnVerificationCode.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.txtEmail.enableMaterialPlaceHolder = YES;
    self.txtName.enableMaterialPlaceHolder = YES;
    self.txtVerificationCode.enableMaterialPlaceHolder = YES;
    self.txtEmail.lineColor = [UIColor clearColor];
    self.txtName.lineColor = [UIColor clearColor];
    self.txtVerificationCode.lineColor = [UIColor clearColor];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.view addGestureRecognizer:singleTap];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    [self.txtEmail resignFirstResponder];
    [self.txtName resignFirstResponder];
    [self.txtVerificationCode resignFirstResponder];
    
}

- (IBAction)clickSignUp:(id)sender {
}

- (IBAction)clickVerificationCode:(id)sender {
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
