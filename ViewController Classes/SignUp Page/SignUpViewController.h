//
//  SignUpViewController.h
//  Cliicker
//
//  Created by Santanu Mondal on 28/12/18.
//  Copyright © 2018 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JJMaterialTextfield.h"

@interface SignUpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblSigninText;
- (IBAction)clickBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
- (IBAction)clickSignUp:(id)sender;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtName;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtEmail;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtVerificationCode;
@property (weak, nonatomic) IBOutlet UIButton *btnVerificationCode;
- (IBAction)clickVerificationCode:(id)sender;

@end
