//
//  ShopViewController.m
//  Cliicker
//
//  Created by Santanu Mondal on 11/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "ShopViewController.h"
#import "OrderHomeViewController.h"
#import "TemplateTableViewCell.h"
#import "AsyncImageView.h"

#import <PlugNPlay/PlugNPlay.h>
#import <CommonCrypto/CommonDigest.h>

#define kMerchantKey @"gnMiAPd7"
#define kMerchantID @"bUTZg7MXvC+W7pCBwbrQG7UMDpaTlV4UYImUAZMI9CU="
#define kMerchantSalt @"5QYiFZ1BcM"

@interface ShopViewController ()

@end

@implementation ShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIColor *color = [UIColor whiteColor];
    self.txtSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search by name" attributes:@{NSForegroundColorAttributeName: color}];
    
    [self.txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.view addGestureRecognizer:singleTap];
    
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_TEMPLATE_LIST];
        NSLog(@"%@",strURL);
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(getTemplateListComp:)];
        [request setDidFailSelector:@selector(getTemplateListFail:)];
        [request addPostValue:@"" forKey:@"srchtext"];
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getTemplateListComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode);
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            self.arrTemplateList = [respon objectForKey:@"rs_row"];
            
            self.arrTemplateListNew = [self.arrTemplateList copy];
            
            [self.tableViewTemplateStore reloadData];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getTemplateListFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

#pragma mark TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrTemplateListNew count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 250;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    TemplateTableViewCell *cell = (TemplateTableViewCell*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = (TemplateTableViewCell*)[[[NSBundle mainBundle] loadNibNamed:@"TemplateTableViewCell" owner:nil options:nil] firstObject];
    }
    
    if([self.arrTemplateListNew count]>0){
        cell.lblTemplateName.text = [[self.arrTemplateListNew objectAtIndex:indexPath.row] objectForKey:@"theme_name"];
        cell.lblTemplateID.text = [cell.lblTemplateID.text stringByAppendingString:[[self.arrTemplateListNew objectAtIndex:indexPath.row] objectForKey:@"theme_code"]];
        cell.lblTemplateDesc.text = [[self.arrTemplateListNew objectAtIndex:indexPath.row] objectForKey:@"short_description"];
        
        NSString *strImageURL = [[self.arrTemplateListNew objectAtIndex:indexPath.row] objectForKey:@"img_url"];
        strImageURL = [strImageURL stringByAppendingString:[[self.arrTemplateListNew objectAtIndex:indexPath.row] objectForKey:@"theme_thumbnail_image"]];
        
        cell.imgVwTemplateImage.imageURL = [NSURL URLWithString:strImageURL];
        
        [cell.btnView addTarget:self action:@selector(clickViewTemplate:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnView.tag = indexPath.row;
        
        [cell.btnSelect setTitle:@"BUY" forState:UIControlStateNormal];
        [cell.btnSelect addTarget:self action:@selector(clickBuyTemplate:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnSelect.tag = indexPath.row;
    }
    else{
    }
    
    return cell;
}

-(void)clickViewTemplate:(id)sender{
    UIButton *btn = (UIButton*)sender;
    
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:[[self.arrTemplateList objectAtIndex:btn.tag] objectForKey:@"theme_template_url"]];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Opened url");
        }
    }];
}

-(void)clickBuyTemplate:(id)sender{
    UIButton *btn = (UIButton*)sender;
    
    OrderHomeViewController *objDash;
    objDash = [[OrderHomeViewController alloc] initWithNibName:@"OrderHomeViewController" bundle:nil];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:objDash animated:NO];
    
//    [PlugNPlay setMerchantDisplayName:@"Santanu"];
//    //[PlugNPlay setButtonTextColor:UIColorFromRGB([self intFromHexString:_tfButtonTextColor.text])];
//    //[PlugNPlay setButtonColor:UIColorFromRGB([self intFromHexString:_tfButtonColor.text])];
//    //[PlugNPlay setTopTitleTextColor:UIColorFromRGB([self intFromHexString:_tfNavTitleColor.text])];
//    //[PlugNPlay setTopBarColor:UIColorFromRGB([self intFromHexString:_tfTopNavColor.text])];
//    
//    //Customize plug and play's behaviour//optional step
//    //[PlugNPlay setDisableCompletionScreen:_isCompletionDisable];
//    //[PlugNPlay setExitAlertOnBankPageDisabled:_isExitAlertOnBankPageDisable];
//    //[PlugNPlay setExitAlertOnCheckoutPageDisabled:_isExitAlertOnCheckoutPageDisable];
//    
//    [PlugNPlay setOrderDetails:[self testOrderDetailsArray]];
//    
//    PUMTxnParam * txnParam = [self getTxnParam];
//    //[self rememberEnteredDetails];
//    
//    [PlugNPlay presentPaymentViewControllerWithTxnParams:txnParam
//                                        onViewController:self
//                                     withCompletionBlock:^(NSDictionary *paymentResponse, NSError *error, id extraParam) {
//                                         if (error) {
//                                             [UIUtility toastMessageOnScreen:[error localizedDescription]];
//                                         } else {
//                                             NSString *message;
//                                             if ([paymentResponse objectForKey:@"result"] && [[paymentResponse objectForKey:@"result"] isKindOfClass:[NSDictionary class]] ) {
//                                                 message = [[paymentResponse objectForKey:@"result"] valueForKey:@"error_Message"];
//                                                 if ([message isEqual:[NSNull null]] || [message length] == 0 || [message isEqualToString:@"No Error"]) {
//                                                     message = [[paymentResponse objectForKey:@"result"] valueForKey:@"status"];
//                                                 }
//                                             }
//                                             else {
//                                                 message = [paymentResponse valueForKey:@"status"];
//                                             }
//                                             [UIUtility toastMessageOnScreen:message];
//                                         }
//                                     }];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture{
    [self.txtSearch resignFirstResponder];
}

- (IBAction)clickDashBoard:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"dashboard"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnProfile:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"profile"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnOrder:(id)sender {
    OrderHomeViewController *objDash;
    objDash = [[OrderHomeViewController alloc] initWithNibName:@"OrderHomeViewController" bundle:nil];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:objDash animated:NO];
}

- (IBAction)clickbtnFollow:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Follow" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"follow"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

#pragma mark TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidChange:(UITextField *)textfield{
    if([textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0){
        predicate = [NSPredicate predicateWithFormat:@"theme_name contains[c] %@",textfield.text];
        self.arrTemplateListNew = [[self.arrTemplateList filteredArrayUsingPredicate:predicate] copy];
        
        [self.tableViewTemplateStore reloadData];
    }else {
        self.arrTemplateListNew = [self.arrTemplateList copy];
    }
    [self.tableViewTemplateStore reloadData];
}


#pragma mark - Helper methods

- (NSArray *)testOrderDetailsArray {
    return @[@{@"From":@"Delhi"}, @{@"To":@"Pune"}];
}

-(PUMTxnParam*)getTxnParam{
    PUMTxnParam *txnParam= [[PUMTxnParam alloc] init];
    //    PUMRequestParams *params = [PUMRequestParams sharedParams];
    
    //Pass these parameters
    txnParam.phone = @"9903316633";
    txnParam.email = @"santanu@gmail.com";
    txnParam.amount = [NSString stringWithFormat:@"%d",20];
    txnParam.environment = PUMEnvironmentTest;
    txnParam.firstname = @"Santanu";
    txnParam.key =  kMerchantKey;
    txnParam.merchantid = kMerchantID;
    //txnParam.txnid = [NSString stringWithFormat:@"0nf7%@",[self getRandomString:4]];
    txnParam.txnID = @"12";
    txnParam.surl = @"https://www.payumoney.com/mobileapp/payumoney/success.php";
    txnParam.furl = @"https://www.payumoney.com/mobileapp/payumoney/failure.php";
    txnParam.productInfo = @"iPhone7";
    
    txnParam.udf1 = @"as";
    txnParam.udf2 = @"sad";
    txnParam.udf3 = @"";
    txnParam.udf4 = @"";
    txnParam.udf5 = @"";
    txnParam.udf6 = @"";
    txnParam.udf7 = @"";
    txnParam.udf8 = @"";
    txnParam.udf9 = @"";
    txnParam.udf10 = @"";
    txnParam.hashValue = [self getHashForPaymentParams:txnParam];
    //    params.hashValue = @"FDBE365FCE1133A9D7091BEE25D032910B7CE8C2F1AA02F86179209EF4A1652CCB35159ECAEFEDD8DE404E2B27809F0B487749DC8DBD9D264E52142B6D3C3270";
    return txnParam;
}

- (NSString *)getRandomString:(NSInteger)length
{
    NSMutableString *returnString = [NSMutableString stringWithCapacity:length];
    
    NSString *numbers = @"0123456789";
    
    // First number cannot be 0
    [returnString appendFormat:@"%C", [numbers characterAtIndex:(arc4random() % ([numbers length]-1))+1]];
    
    for (int i = 1; i < length; i++) {
        [returnString appendFormat:@"%C", [numbers characterAtIndex:arc4random() % [numbers length]]];
    }
    
    return returnString;
}

//TODO: get rid of this function for test environemnt
-(NSString*)getHashForPaymentParams:(PUMTxnParam*)txnParam {
    NSString *salt = kMerchantSalt;
    NSString *hashSequence = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",txnParam.key,txnParam.txnID,txnParam.amount,txnParam.productInfo,txnParam.firstname,txnParam.email,txnParam.udf1,txnParam.udf2,txnParam.udf3,txnParam.udf4,txnParam.udf5,txnParam.udf6,txnParam.udf7,txnParam.udf8,txnParam.udf9,txnParam.udf10, salt];
    
    NSString *hash = [[[[[self createSHA512:hashSequence] description]stringByReplacingOccurrencesOfString:@"<" withString:@""]stringByReplacingOccurrencesOfString:@">" withString:@""]stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return hash;
}

- (NSString*) createSHA512:(NSString *)source {
    
    const char *s = [source cStringUsingEncoding:NSASCIIStringEncoding];
    
    NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];
    
    uint8_t digest[CC_SHA512_DIGEST_LENGTH] = {0};
    
    CC_SHA512(keyData.bytes, (CC_LONG)keyData.length, digest);
    
    NSData *output = [NSData dataWithBytes:digest length:CC_SHA512_DIGEST_LENGTH];
    NSLog(@"Hash output --------- %@",output);
    NSString *hash =  [[[[output description]stringByReplacingOccurrencesOfString:@"<" withString:@""]stringByReplacingOccurrencesOfString:@">" withString:@""]stringByReplacingOccurrencesOfString:@" " withString:@""];
    return hash;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
