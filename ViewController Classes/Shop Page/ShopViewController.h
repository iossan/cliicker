//
//  ShopViewController.h
//  Cliicker
//
//  Created by Santanu Mondal on 11/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopViewController : UIViewController{
    NSPredicate *predicate;
}

- (IBAction)clickDashBoard:(id)sender;
- (IBAction)clickbtnOrder:(id)sender;
- (IBAction)clickbtnFollow:(id)sender;
- (IBAction)clickbtnProfile:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableViewTemplateStore;
@property (nonatomic, retain) NSMutableArray *arrTemplateList;
@property (nonatomic, retain) NSArray *arrTemplateListNew;

@end

NS_ASSUME_NONNULL_END
