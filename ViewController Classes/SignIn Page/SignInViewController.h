//
//  SignInViewController.h
//  Cliicker
//
//  Created by Santanu Mondal on 28/12/18.
//  Copyright © 2018 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JJMaterialTextfield.h"

@interface SignInViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblSignUpText;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnGoogle;
- (IBAction)clickSignUp:(id)sender;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtEmail;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtPassword;
- (IBAction)clickLogin:(id)sender;

@end
