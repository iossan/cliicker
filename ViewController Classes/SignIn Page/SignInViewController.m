//
//  SignInViewController.m
//  Cliicker
//
//  Created by Santanu Mondal on 28/12/18.
//  Copyright © 2018 Santanu Mondal. All rights reserved.
//

#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "JJMaterialTextfield.h"

@interface SignInViewController ()

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    NSString *strText1 = @"Don't have an account? Sign up now !";
    NSString *redText = @"Sign up now !";
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:strText1];
    [attString addAttribute:(NSString*)NSForegroundColorAttributeName
                      value:[UIColor colorWithRed:208.0/255.0 green:14.0/255.0 blue:49.0/255.0 alpha:1.0]
                      range:(NSRange){23,[redText length]}];
    
    self.lblSignUpText.attributedText = attString;
    
    self.btnLogin.layer.borderWidth = 0.5;
    self.btnLogin.layer.cornerRadius = 5.0;
    self.btnLogin.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.btnFacebook.layer.borderWidth = 0.5;
    self.btnFacebook.layer.cornerRadius = 5.0;
    self.btnFacebook.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.btnGoogle.layer.borderWidth = 0.5;
    self.btnGoogle.layer.cornerRadius = 5.0;
    self.btnGoogle.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.txtEmail.enableMaterialPlaceHolder = YES;
    self.txtPassword.enableMaterialPlaceHolder = YES;
    self.txtEmail.lineColor = [UIColor clearColor];
    self.txtPassword.lineColor = [UIColor clearColor];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.view addGestureRecognizer:singleTap];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    [self.txtEmail resignFirstResponder];
    [self.txtPassword resignFirstResponder];
}

- (IBAction)clickLogin:(id)sender {
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
//    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"dashboard"];
//    [self.navigationController pushViewController:lvc animated:YES];
    
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        if(self.txtEmail.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter email or mobile number."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtPassword.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter password."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else{
            [self.txtEmail resignFirstResponder];
            [self.txtPassword resignFirstResponder];
            
            [self.navigationController.view showActivityView];
            
            NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_LOGIN_URL];
            NSLog(@"%@",strURL);
            
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
            [request setDelegate:self];
            [request setRequestMethod:@"POST"];
            [request addRequestHeader:@"Content-Type" value:@"application/json"];
            [request setDidFinishSelector:@selector(userLoginComp:)];
            [request setDidFailSelector:@selector(userLoginFail:)];
            [request addPostValue:self.txtEmail.text forKey:@"email"];
            [request addPostValue:self.txtPassword.text forKey:@"password"];
            //[request addPostValue:app.strDeviceToken forKey:@"device_token"];
            [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
            //[request addPostValue:IOS forKey:@"mode"];
            [request startAsynchronous];
            
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)userLoginComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            [[NSUserDefaults standardUserDefaults] setObject:[[respon objectForKey:@"rs_row"] objectForKey:@"id"] forKey:@"USERID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
//            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
//            UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"dashboard"];
//            [self.navigationController pushViewController:lvc animated:YES];
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
            UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"profile"];
            [self.navigationController pushViewController:lvc animated:YES];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)userLoginFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

- (IBAction)clickSignUp:(id)sender {
    SignUpViewController *obj_AfterLoginPage;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        obj_AfterLoginPage = [[SignUpViewController alloc] initWithNibName:@"SignUpViewController" bundle:nil];
    }
    else{
        obj_AfterLoginPage = [[SignUpViewController alloc] initWithNibName:@"SignUpViewController_iPad" bundle:nil];
    }
    [self.navigationController pushViewController:obj_AfterLoginPage animated:YES];
}

#pragma mark TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
