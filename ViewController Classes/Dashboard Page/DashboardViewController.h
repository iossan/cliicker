//
//  DashboardViewController.h
//  Cliicker
//
//  Created by Santanu Mondal on 07/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DashboardViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgVwUserimage;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UITextView *txtVwNotes;
- (IBAction)clickbtnProfile:(id)sender;
- (IBAction)clickbtnFollow:(id)sender;
- (IBAction)clickbtnShop:(id)sender;
- (IBAction)clickbtnOrder:(id)sender;

@end

NS_ASSUME_NONNULL_END
