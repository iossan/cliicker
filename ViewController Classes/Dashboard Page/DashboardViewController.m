//
//  DashboardViewController.m
//  Cliicker
//
//  Created by Santanu Mondal on 07/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "DashboardViewController.h"
#import "DashboardTableViewCell.h"
#import "OrderHomeViewController.h"

@interface DashboardViewController ()

@end

@implementation DashboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIColor *color = [UIColor whiteColor];
    self.txtSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: color}];
    
    self.imgVwUserimage.layer.borderWidth = 1.0;
    self.imgVwUserimage.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgVwUserimage.layer.cornerRadius = self.imgVwUserimage.frame.size.width / 2;
    self.imgVwUserimage.clipsToBounds = YES;
    
    self.btnEdit.layer.borderWidth = 0.5;
    self.btnEdit.layer.cornerRadius = 5.0;
    self.btnEdit.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.txtVwNotes.layer.borderWidth = 0.5;
    self.txtVwNotes.layer.cornerRadius = 5.0;
    self.txtVwNotes.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.view addGestureRecognizer:singleTap];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    [self.txtSearch resignFirstResponder];
}

#pragma mark TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    DashboardTableViewCell *cell = (DashboardTableViewCell*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = (DashboardTableViewCell*)[[[NSBundle mainBundle] loadNibNamed:@"DashboardTableViewCell" owner:nil options:nil] firstObject];
    }
    
    cell.imgVwUserPro.layer.borderWidth = 0.5;
    cell.imgVwUserPro.layer.borderColor = [UIColor clearColor].CGColor;
    cell.imgVwUserPro.layer.cornerRadius = self.imgVwUserimage.frame.size.width / 2;
    cell.imgVwUserPro.clipsToBounds = YES;
    
    return cell;
}

- (IBAction)clickbtnProfile:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"profile"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnFollow:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Follow" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"follow"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnShop:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Shop" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"shop"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnOrder:(id)sender {
    OrderHomeViewController *objDash;
    objDash = [[OrderHomeViewController alloc] initWithNibName:@"OrderHomeViewController" bundle:nil];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:objDash animated:NO];
}

#pragma mark TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
