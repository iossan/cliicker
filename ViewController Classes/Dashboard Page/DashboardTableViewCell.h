//
//  DashboardTableViewCell.h
//  Cliicker
//
//  Created by Santanu Mondal on 07/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DashboardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgVwUserPro;

@end

NS_ASSUME_NONNULL_END
