//
//  ProfileViewController.h
//  Cliicker
//
//  Created by Santanu Mondal on 07/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XHMenu.h"
#import "XHScrollMenu.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProfileViewController : UIViewController<XHScrollMenuDelegate>{
    NSArray *arrItemss;
    
    UIRefreshControl *refreshControl;
    
    int pageCount;
}
@property (nonatomic, retain) NSDictionary *dictProfileData;
@property (nonatomic, strong) XHScrollMenu *scrollMenuLower;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
- (IBAction)clickDashBoard:(id)sender;
- (IBAction)clickbtnFollow:(id)sender;
- (IBAction)clickbtnShop:(id)sender;
- (IBAction)clickbtnOrder:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwUserCoverPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnFollow;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwProfilePic;
@property (weak, nonatomic) IBOutlet UIView *viewMiddle;
@property (weak, nonatomic) IBOutlet UIView *viewMain;

@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserCLId;
@property (weak, nonatomic) IBOutlet UILabel *lblUserAvBudget;
@property (weak, nonatomic) IBOutlet UILabel *lblUserFollowing;
@property (weak, nonatomic) IBOutlet UILabel *lblUserFollowers;
@property (weak, nonatomic) IBOutlet UILabel *lblUserReviewRating;
- (IBAction)clickEdit:(id)sender;
@property (nonatomic, retain) NSMutableArray *arrUserPhotos;
@property (weak, nonatomic) IBOutlet UITableView *tableViewUserPhotos;
@property (weak, nonatomic) IBOutlet UIView *viewAbout;
@property (weak, nonatomic) IBOutlet UILabel *lblTextAboutMe;
@property (weak, nonatomic) IBOutlet UILabel *lblUserEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblUserMobile;


@end

NS_ASSUME_NONNULL_END
