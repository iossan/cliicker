//
//  PopUpImageViewController.h
//  HSComply
//
//  Created by Santanu Mondal on 22/08/17.
//  Copyright © 2017 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MJSecondPopupDelegate;

@interface PopUpImageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageVwMain;
- (IBAction)clickCross:(id)sender;
@property (nonatomic, retain) UIImage *imgDoc;
@property (nonatomic, retain) NSString *strImagUrl;
@property (nonatomic, retain) NSString *strWhichClass;
@property (assign, nonatomic) id <MJSecondPopupDelegate>delegate;

@end

@protocol MJSecondPopupDelegate<NSObject>
@optional
- (void)cancelButtonClicked:(PopUpImageViewController*)secondDetailViewController;
@end
