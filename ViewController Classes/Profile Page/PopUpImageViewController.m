//
//  PopUpImageViewController.m
//  HSComply
//
//  Created by Santanu Mondal on 22/08/17.
//  Copyright © 2017 Santanu Mondal. All rights reserved.
//

#import "PopUpImageViewController.h"
#import "AsyncImageView.h"

@interface PopUpImageViewController ()

@end

@implementation PopUpImageViewController

@synthesize delegate,imgDoc,strImagUrl,strWhichClass;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.imageVwMain.imageURL = [NSURL URLWithString:self.strImagUrl];
}

- (IBAction)clickCross:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelButtonClicked:)]) {
        [self.delegate cancelButtonClicked:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
