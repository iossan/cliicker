//
//  EditProfileViewController.h
//  Cliicker
//
//  Created by Santanu Mondal on 11/06/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EditProfileViewController : UIViewController<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>{
    NSData *profileImageData;
    NSData *coverImageData;
    
    BOOL whichCamera;
}
@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)clickBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
- (IBAction)clickUpdate:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewMain;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwCoverPic;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwProfilePic;
@property (nonatomic, retain) NSDictionary *dictProfileData;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtFirstName;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtLastName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailID;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtPhoneNo;
@property (weak, nonatomic) IBOutlet UITextView *txtVwAddress;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtBudget;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtGender;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UITextView *txtVwIntro;
@property (weak, nonatomic) IBOutlet UILabel *lblIntroDesc;
@property (weak, nonatomic) IBOutlet UITextView *txtVwAboutme;
@property (weak, nonatomic) IBOutlet UILabel *lblAboutMe;
- (IBAction)clickPhoto:(id)sender;

@end

NS_ASSUME_NONNULL_END
