//
//  ProfileViewController.m
//  Cliicker
//
//  Created by Santanu Mondal on 07/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "ProfileViewController.h"
#import "OrderHomeViewController.h"
#import "AsyncImageView.h"
#import "EditProfileViewController.h"
#import "PGListViewController.h"
#import "PopUpImageViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface ProfileViewController ()<MJSecondPopupDelegate>

@end

@implementation ProfileViewController

-(void)viewWillAppear:(BOOL)animated{
    //NSLog(@"CallViewWillAppear");
    
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_USER_DETAILS];
        NSLog(@"%@",strURL);
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(getUserDetailsComp:)];
        [request setDidFailSelector:@selector(getUserDetailsFail:)];
        [request addPostValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"USERID"] forKey:@"pid"];
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableViewUserPhotos.hidden = YES;
    self.arrUserPhotos = [NSMutableArray array];
    pageCount = 1;
    
    UIColor *color = [UIColor whiteColor];
    self.txtSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search" attributes:@{NSForegroundColorAttributeName: color}];
    
    self.imgVwProfilePic.layer.borderWidth = 1.0;
    self.imgVwProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgVwProfilePic.layer.cornerRadius = self.imgVwProfilePic.frame.size.width / 2;
    self.imgVwProfilePic.clipsToBounds = YES;
    
    self.btnFollow.layer.borderWidth = 0.5;
    self.btnFollow.layer.cornerRadius = 5.0;
    self.btnFollow.layer.borderColor = [UIColor clearColor].CGColor;
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        self.tableViewUserPhotos.refreshControl = refreshControl;
    } else {
        [self.tableViewUserPhotos addSubview:refreshControl];
    }
    
    arrItemss = [NSArray arrayWithObjects:@"ABOUT",@"PHOTOS",@"VIDEOS",@"ALBUMS", nil];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
        }
        else if(result.width == 375){
            self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 375, 30)];
        }
        else{
            self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 414, 30)];
        }
    }
    else{
        self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 768, 30)];
    }
    
    self.scrollMenuLower.backgroundColor = [UIColor clearColor];//[UIColor colorWithRed:3.0/255.0 green:44.0/255.0 blue:114.0/255.0 alpha:1.0];
    self.scrollMenuLower.indicatorTintColor = [UIColor clearColor];//[UIColor colorWithRed:235.0/255.0 green:128.0/255.0 blue:22.0/255.0 alpha:1.0];
    self.scrollMenuLower.hasShadowForBoth = NO;
    self.scrollMenuLower.shouldUniformizeMenus = YES;
    self.scrollMenuLower.delegate = self;
    [self.viewMiddle addSubview:self.scrollMenuLower];
    
    NSMutableArray *menuss = [NSMutableArray array];
    
    for (int k = 0; k < [arrItemss count]; k ++) {
        XHMenu *menu1 = [[XHMenu alloc] init];
        
        menu1.title = [arrItemss objectAtIndex:k];
        menu1.titleFont = [UIFont fontWithName:@"Bebas" size:14.0];
        menu1.titleNormalColor = [UIColor whiteColor];
        menu1.titleSelectedColor = [UIColor colorWithRed:255.0/255.0 green:188.0/255.0 blue:0.0/255.0 alpha:1.0];
        
        [menuss addObject:menu1];
    }
    
    self.scrollMenuLower.menus = menuss;
    [self.scrollMenuLower reloadData];
    
}

-(void)getUserDetailsComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            self.dictProfileData = [respon objectForKey:@"result"];
            
            self.lblUserName.text = [[respon objectForKey:@"result"] objectForKey:@"first_name"];
            self.lblUserName.text = [self.lblUserName.text stringByAppendingString:@"  "];
            self.lblUserName.text = [self.lblUserName.text stringByAppendingString:[[respon objectForKey:@"result"] objectForKey:@"last_name"]];
            
            self.lblUserCLId.text = @"Cliicker ID : ";
            self.lblUserCLId.text = [self.lblUserCLId.text stringByAppendingString:[[respon objectForKey:@"result"] objectForKey:@"pg_clicker_code"]];
            
            self.imgVwUserCoverPhoto.imageURL = [NSURL URLWithString:[[respon objectForKey:@"result"] objectForKey:@"cover_image_url"]];
            self.imgVwProfilePic.imageURL = [NSURL URLWithString:[[respon objectForKey:@"result"] objectForKey:@"featured_image_url"]];
            
//            if([[[respon objectForKey:@"result"] objectForKey:@"cover_image"] isEqualToString:@""]){
//                NSString *strImageURL = [[respon objectForKey:@"result"] objectForKey:@"default_cover_image"];
//
//                self.imgVwUserCoverPhoto.imageURL = [NSURL URLWithString:strImageURL];
//            }
//            else{
//                NSString *strImageURL = [[respon objectForKey:@"result"] objectForKey:@"img_url"];
//                strImageURL = [strImageURL stringByAppendingString:[[respon objectForKey:@"result"] objectForKey:@"cover_image"]];
//
//                self.imgVwUserCoverPhoto.imageURL = [NSURL URLWithString:strImageURL];
//            }
//
//            if([[[respon objectForKey:@"result"] objectForKey:@"featured_image"] isEqualToString:@""]){
//                NSString *strImageURL = [[respon objectForKey:@"result"] objectForKey:@"default_profile_image"];
//
//                self.imgVwProfilePic.imageURL = [NSURL URLWithString:strImageURL];
//            }
//            else{
//                NSString *strImageURL = [[respon objectForKey:@"result"] objectForKey:@"img_url"];
//                strImageURL = [strImageURL stringByAppendingString:[[respon objectForKey:@"result"] objectForKey:@"featured_image"]];
//
//                self.imgVwProfilePic.imageURL = [NSURL URLWithString:strImageURL];
//            }
            
            self.lblUserAvBudget.text = @"₹";
            self.lblUserAvBudget.text = [self.lblUserAvBudget.text stringByAppendingString:[[respon objectForKey:@"result"] objectForKey:@"budget"]];
            
            if(![[[respon objectForKey:@"result"] objectForKey:@"no_following"] isKindOfClass:[NSNull class]]){
                self.lblUserFollowing.text = [[respon objectForKey:@"result"] objectForKey:@"no_following"];
            }
            else{
                self.lblUserFollowing.text = @"0";
            }
            
            if(![[[respon objectForKey:@"result"] objectForKey:@"no_followers"] isKindOfClass:[NSNull class]]){
                self.lblUserFollowers.text = [[respon objectForKey:@"result"] objectForKey:@"no_followers"];
            }
            else{
                self.lblUserFollowers.text = @"0";
            }
            
            self.lblUserReviewRating.text = [[respon objectForKey:@"result"] objectForKey:@"total_review"];
            
            self.lblTextAboutMe.text = [[respon objectForKey:@"result"] objectForKey:@"about_me"];
            self.lblUserEmail.text = [[respon objectForKey:@"result"] objectForKey:@"email"];
            self.lblUserMobile.text = [[respon objectForKey:@"result"] objectForKey:@"contact_number"];
            
            [self getUserPhotos];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getUserDetailsFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

- (void)refreshTable {
    //TODO: refresh your data
    pageCount = pageCount +1;
    [self getUserPhotos];
}

-(void)getUserPhotos{
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        //[self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_USER_PHOTOLIST];
        NSLog(@"%@",strURL);

        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(getUserPhotosComp:)];
        [request setDidFailSelector:@selector(getUserPhotosFail:)];
        [request addPostValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"USERID"] forKey:@"photographer_id"];
        [request addPostValue:@"10" forKey:@"limit"];
        [request addPostValue:[NSString stringWithFormat:@"%d",pageCount] forKey:@"page_no"];
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getUserPhotosComp:(ASIHTTPRequest *)req{
    
    //[self.navigationController.view hideActivityView];
    [refreshControl endRefreshing];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            if([[respon objectForKey:@"rs_row"] count]>0){
                for(int x=0; x<[[respon objectForKey:@"rs_row"] count]; x++){
                    [self.arrUserPhotos addObject:[[[respon objectForKey:@"rs_row"] objectAtIndex:x] objectForKey:@"image_url"]];
                }
            }
            
            [self.tableViewUserPhotos reloadData];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getUserPhotosFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

#pragma mark - Table View Delegate Method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([self.arrUserPhotos count]>0){
        return 1;
    }
    else{
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        long xx = [self.arrUserPhotos count]/3;
        if([self.arrUserPhotos count]%3 > 0){
            return ((xx*105)+110);
        }
        else{
            return xx*105;
        }
    }
    else{
        return 600;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    cell.backgroundColor = [UIColor clearColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellID = [NSString stringWithFormat:@"cell-%ld",(long)[indexPath row]];
    [tableView dequeueReusableCellWithIdentifier:cellID];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        if([self.arrUserPhotos count]>0){
            int viewHeight = 5;
            int viewWidth = 5;
            
            for(int x=0; x<[self.arrUserPhotos count]; x++){
                UIImageView *imgVw = [[UIImageView alloc] init];
                imgVw.frame = CGRectMake(viewWidth, viewHeight, 100, 100);
                imgVw.layer.borderWidth = 0.5;
                imgVw.layer.cornerRadius = 5.0;
                imgVw.layer.borderColor = [UIColor clearColor].CGColor;
                imgVw.backgroundColor = [UIColor clearColor];
                imgVw.imageURL = [NSURL URLWithString:[self.arrUserPhotos objectAtIndex:x]];
                [cell.contentView addSubview:imgVw];
                
                UIButton *btnDelete= [UIButton buttonWithType:UIButtonTypeCustom];
                btnDelete.frame = CGRectMake(viewWidth, viewHeight, 100, 100);
                btnDelete.tag = x;
                [btnDelete addTarget:self action:@selector(clickForBigImage:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:btnDelete];
                
                viewWidth = viewWidth + 105;
                
                if(viewWidth == 320){
                    viewHeight = viewHeight + 105;
                    viewWidth = 5;
                }
            }
        }
        else{
            UILabel *lblCompName = [[UILabel alloc] init];
            lblCompName.frame = CGRectMake(10, 7, 270, 21);
            lblCompName.text = @"No Record Found!";
            lblCompName.textColor = [UIColor blackColor];
            lblCompName.backgroundColor = [UIColor clearColor];
            lblCompName.font = [UIFont fontWithName:@"Gotham-Light" size:17.0];
            [cell.contentView addSubview:lblCompName];
        }
    }
    else{
        if([self.arrUserPhotos count]>0){
            
        }
        else{
            UILabel *lblCompName = [[UILabel alloc] init];
            lblCompName.frame = CGRectMake(10, 10, 470, 30);
            lblCompName.text = @"No Record Found!";
            lblCompName.textColor = [UIColor blackColor];
            lblCompName.backgroundColor = [UIColor clearColor];
            lblCompName.font = [UIFont fontWithName:@"Gotham-Light" size:13.0];
            [cell.contentView addSubview:lblCompName];
        }
    }
    
    return cell;
}

-(void)clickForBigImage:(id)sender{
    UIButton *btn = (UIButton*)sender;
    
    PopUpImageViewController *obj_Pop;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        obj_Pop = [[PopUpImageViewController alloc] initWithNibName:@"PopUpImageViewController" bundle:nil];
    }
    else{
        obj_Pop = [[PopUpImageViewController alloc] initWithNibName:@"PopUpImageViewController_iPad" bundle:nil];
    }
    obj_Pop.strImagUrl = [self.arrUserPhotos objectAtIndex:btn.tag];
    obj_Pop.delegate = self;
    [self presentPopupViewController:obj_Pop animationType:MJPopupViewAnimationFade];
}

- (void)cancelButtonClicked:(PopUpImageViewController *)aSecondDetailViewController
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

#pragma mark - XHScrollMenu Delegate Methods

- (void)scrollMenuDidSelected:(XHScrollMenu *)scrollMenu menuIndex:(NSUInteger)selectIndex {
    
    NSLog(@"selectIndex : %lu", (unsigned long)selectIndex);
    
    if(selectIndex == 0){
        self.tableViewUserPhotos.hidden = YES;
        self.viewAbout.hidden = NO;
    }
    else if(selectIndex == 1){
        self.tableViewUserPhotos.hidden = NO;
        self.viewAbout.hidden = YES;
    }
}

- (void)scrollMenuDidManagerSelected:(XHScrollMenu *)scrollMenu{
    
}

- (IBAction)clickDashBoard:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"dashboard"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnFollow:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Follow" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"follow"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnShop:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Shop" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"shop"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnOrder:(id)sender {
    OrderHomeViewController *objDash;
    objDash = [[OrderHomeViewController alloc] initWithNibName:@"OrderHomeViewController" bundle:nil];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:objDash animated:NO];
}

- (IBAction)clickEdit:(id)sender {
    EditProfileViewController *obj_ListUser;
    obj_ListUser = [[EditProfileViewController alloc] initWithNibName:@"EditProfileViewController" bundle:nil];
    obj_ListUser.dictProfileData = self.dictProfileData;
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:obj_ListUser];
    navController.modalPresentationStyle=UIModalPresentationFormSheet;
    navController.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
    [self presentViewController:navController animated:YES completion:nil];
}


#pragma mark TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self openUserSearchView];
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)openUserSearchView{
    PGListViewController *obj_ListUser;
    obj_ListUser = [[PGListViewController alloc] initWithNibName:@"PGListViewController" bundle:nil];
    //obj_ListUser.dictProfileData = self.dictProfileData;
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:obj_ListUser];
    navController.modalPresentationStyle=UIModalPresentationFormSheet;
    navController.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
    [self presentViewController:navController animated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
