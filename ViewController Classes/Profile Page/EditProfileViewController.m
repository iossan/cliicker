//
//  EditProfileViewController.m
//  Cliicker
//
//  Created by Santanu Mondal on 11/06/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "EditProfileViewController.h"
#import "AsyncImageView.h"

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController
@synthesize dictProfileData;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    profileImageData = nil;
    coverImageData = nil;
    
    [self.viewMain addSubview:self.scrollViewMain];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.scrollViewMain.frame = CGRectMake(0, 0, 320, 478);
        }
        else if(result.width == 375){
            self.scrollViewMain.frame = CGRectMake(0, 0, 375, 478);
        }
        else{
            self.scrollViewMain.frame = CGRectMake(0, 0, 414, 454);
        }
    }
    else{
        self.scrollViewMain.frame = CGRectMake(0, 0, 768, 865);
    }
    
    self.txtFirstName.enableMaterialPlaceHolder = YES;
    self.txtLastName.enableMaterialPlaceHolder = YES;
    self.txtPhoneNo.enableMaterialPlaceHolder = YES;
    self.txtGender.enableMaterialPlaceHolder = YES;
    self.txtBudget.enableMaterialPlaceHolder = YES;
    self.txtFirstName.lineColor = [UIColor clearColor];
    self.txtLastName.lineColor = [UIColor clearColor];
    self.txtPhoneNo.lineColor = [UIColor clearColor];
    self.txtGender.lineColor = [UIColor clearColor];
    self.txtBudget.lineColor = [UIColor clearColor];
    
    self.txtVwAddress.text = @"Address";
    self.txtVwAddress.textColor = [UIColor lightGrayColor];
    
    self.txtVwIntro.text = @"Introduce yourself with short description";
    self.txtVwIntro.textColor = [UIColor lightGrayColor];
    
    self.txtVwAboutme.text = @"About Me";
    self.txtVwAboutme.textColor = [UIColor lightGrayColor];
    
    self.btnBack.layer.borderWidth = 0.5;
    self.btnBack.layer.cornerRadius = 5.0;
    self.btnBack.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.btnUpdate.layer.borderWidth = 0.5;
    self.btnUpdate.layer.cornerRadius = 5.0;
    self.btnUpdate.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.imgVwProfilePic.layer.borderWidth = 1.0;
    self.imgVwProfilePic.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgVwProfilePic.layer.cornerRadius = self.imgVwProfilePic.frame.size.width / 2;
    self.imgVwProfilePic.clipsToBounds = YES;
    
    NSLog(@"%@",self.dictProfileData);
    
    self.imgVwCoverPic.imageURL = [NSURL URLWithString:[self.dictProfileData objectForKey:@"cover_image_url"]];
    self.imgVwProfilePic.imageURL = [NSURL URLWithString:[self.dictProfileData objectForKey:@"featured_image_url"]];
    
    self.txtFirstName.text = [self.dictProfileData objectForKey:@"first_name"];
    self.txtLastName.text = [self.dictProfileData objectForKey:@"last_name"];
    self.lblEmailID.text = [self.dictProfileData objectForKey:@"email"];
    self.txtPhoneNo.text = [self.dictProfileData objectForKey:@"contact_number"];
    self.txtGender.text = [self.dictProfileData objectForKey:@"gender"];
    self.txtBudget.text = [self.dictProfileData objectForKey:@"budget"];
    self.txtVwAddress.text = [self.dictProfileData objectForKey:@"address"];
    self.txtVwIntro.text = [self.dictProfileData objectForKey:@"intro_text"];
    self.txtVwAboutme.text = [self.dictProfileData objectForKey:@"about_me"];
    
    self.lblAboutMe.hidden = NO;
    self.lblIntroDesc.hidden = NO;
    self.lblAddress.hidden = NO;
    
    self.txtVwAboutme.textColor = [UIColor blackColor];
    self.txtVwIntro.textColor = [UIColor blackColor];
    self.txtVwAddress.textColor = [UIColor blackColor];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.view addGestureRecognizer:singleTap];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture{
    [self.txtFirstName resignFirstResponder];
    [self.txtLastName resignFirstResponder];
    [self.txtPhoneNo resignFirstResponder];
    [self.txtGender resignFirstResponder];
    
    [self.txtBudget resignFirstResponder];
    [self.txtVwAboutme resignFirstResponder];
    [self.txtVwIntro resignFirstResponder];
    [self.txtVwAddress resignFirstResponder];
}

- (IBAction)clickPhoto:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if(btn.tag == 1){
        whichCamera = YES;
    }
    else{
        whichCamera = NO;
    }
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Cancel button tappped do nothing.
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self captureImage];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self PickImageForIndex_iPhone];
    }]];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
    else{
        UIPopoverPresentationController *popPresenter = [actionSheet popoverPresentationController];
        popPresenter.sourceView = sender;
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
}

#pragma mark imagePicker delegate

-(void)captureImage
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *camerapicker = [[UIImagePickerController alloc] init];
        
        camerapicker.delegate = self;
        camerapicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [camerapicker parentViewController];
        [self presentViewController:camerapicker animated:YES completion:nil];
    }
    else
    {
        
    }
}

-(void)PickImageForIndex_iPhone
{
    UIImagePickerControllerSourceType imgCtrlType;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        imgCtrlType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    else {
        imgCtrlType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    imagePicker.sourceType = imgCtrlType;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark PhotoGalary Delegate Method

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editingInfo
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if(whichCamera == YES){
        self.imgVwProfilePic.image = img;
        profileImageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(img,0.4)];
    }
    else{
        self.imgVwCoverPic.image = img;
        coverImageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation(img,0.4)];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)clickUpdate:(id)sender {
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        if(self.txtFirstName.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter first name."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtLastName.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter last name."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtPhoneNo.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter phone number."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtGender.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter gender."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtBudget.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter budget."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else{
            NSString *strAddress;
            if([self.txtVwAddress.text isEqualToString:@"Address"]){
                strAddress = @"";
            }
            else{
                strAddress = self.txtVwAddress.text;
            }
            
            NSString *strIntro;
            if([self.txtVwAddress.text isEqualToString:@"Introduce yourself with short description"]){
                strIntro = @"";
            }
            else{
                strIntro = self.txtVwIntro.text;
            }
            
            NSString *strAboutMe;
            if([self.txtVwAddress.text isEqualToString:@"About Me"]){
                strAboutMe = @"";
            }
            else{
                strAboutMe = self.txtVwAboutme.text;
            }
            
            [self.txtFirstName resignFirstResponder];
            [self.txtLastName resignFirstResponder];
            [self.txtPhoneNo resignFirstResponder];
            [self.txtGender resignFirstResponder];
            
            [self.txtBudget resignFirstResponder];
            [self.txtVwAboutme resignFirstResponder];
            [self.txtVwIntro resignFirstResponder];
            [self.txtVwAddress resignFirstResponder];
            
            [self.navigationController.view showActivityView];
            
            NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_USER_EDIT_DETAILS];
            NSLog(@"%@",strURL);
            
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
            [request setDelegate:self];
            [request setRequestMethod:@"POST"];
            [request addRequestHeader:@"Content-Type" value:@"application/json"];
            [request setDidFinishSelector:@selector(submitEditDetailsComp:)];
            [request setDidFailSelector:@selector(submitEditDetailsFail:)];
            [request addPostValue:self.txtFirstName.text forKey:@"first_name"];
            [request addPostValue:self.txtLastName.text forKey:@"last_name"];
            [request addPostValue:self.txtPhoneNo.text forKey:@"contact_number"];
            [request addPostValue:self.txtGender.text forKey:@"gender"];
            [request addPostValue:self.txtBudget.text forKey:@"budget"];
            [request addPostValue:strAddress forKey:@"address"];
            [request addPostValue:strIntro forKey:@"intro_text"];
            [request addPostValue:strAboutMe forKey:@"about_me"];
            
            if(profileImageData == nil){
                [request addPostValue:[self.dictProfileData objectForKey:@"featured_image"] forKey:@"existing_imagename"];
            }
            else{
                [request addData:profileImageData withFileName:@"xyz.png" andContentType:@"image/png" forKey:@"featured_image"];
            }
            
            if(coverImageData == nil){
                [request addPostValue:[self.dictProfileData objectForKey:@"cover_image"] forKey:@"existing_cover_imagename"];
            }
            else{
                [request addData:coverImageData withFileName:@"xyz.png" andContentType:@"image/png" forKey:@"cover_image"];
            }
            
            [request addPostValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"USERID"] forKey:@"pgID"];
            [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
            //[request addPostValue:IOS forKey:@"mode"];
            [request startAsynchronous];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitEditDetailsComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
        
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Profile Updated Successfully."
                                      duration:3.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeSuccess
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           [self dismissViewControllerAnimated:YES completion:NULL];
                                       }];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitEditDetailsFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

# pragma mark TextView Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == self.txtVwAddress){
        if ([textView.text isEqualToString:@"Address"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
            self.lblAddress.hidden = NO;
        }
    }
    else if(textView == self.txtVwIntro){
        if ([textView.text isEqualToString:@"Introduce yourself with short description"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
            self.lblIntroDesc.hidden = NO;
        }
    }
    else if(textView == self.txtVwAboutme){
        if ([textView.text isEqualToString:@"About Me"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
            self.lblAboutMe.hidden = NO;
        }
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == self.txtVwAddress){
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Address";
            textView.textColor = [UIColor lightGrayColor];
            self.lblAddress.hidden = YES;
        }
    }
    else if(textView == self.txtVwIntro){
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Introduce yourself with short description";
            textView.textColor = [UIColor lightGrayColor];
            self.lblIntroDesc.hidden = YES;
        }
    }
    else if(textView == self.txtVwAboutme){
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"About Me";
            textView.textColor = [UIColor lightGrayColor];
            self.lblAboutMe.hidden = YES;
        }
    }
    [textView resignFirstResponder];
}

#pragma mark TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder] ;
    
    return YES;
}

- (IBAction)clickBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
