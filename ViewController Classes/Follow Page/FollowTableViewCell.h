//
//  FollowTableViewCell.h
//  Cliicker
//
//  Created by Santanu Mondal on 13/06/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FollowTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgVwProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblProID;
@property (weak, nonatomic) IBOutlet UIButton *btnFollow;

@end

NS_ASSUME_NONNULL_END
