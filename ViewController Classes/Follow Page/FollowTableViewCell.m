//
//  FollowTableViewCell.m
//  Cliicker
//
//  Created by Santanu Mondal on 13/06/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "FollowTableViewCell.h"

@implementation FollowTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.imgVwProfile.layer.borderWidth = 1.0;
    self.imgVwProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgVwProfile.layer.cornerRadius = self.imgVwProfile.frame.size.width / 2;
    self.imgVwProfile.clipsToBounds = YES;
    
    self.btnFollow.layer.borderWidth = 0.5;
    self.btnFollow.layer.cornerRadius = 5.0;
    self.btnFollow.layer.borderColor = [UIColor clearColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
@end
