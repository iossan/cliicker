//
//  FollowViewController.h
//  Cliicker
//
//  Created by Santanu Mondal on 07/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XHMenu.h"
#import "XHScrollMenu.h"

NS_ASSUME_NONNULL_BEGIN

@interface FollowViewController : UIViewController<XHScrollMenuDelegate>{
    NSArray *arrItemss;
    
    BOOL isFollowerORFollowing;
    NSArray *arrFollowerIDs;
}
@property (nonatomic, strong) XHScrollMenu *scrollMenuLower;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
- (IBAction)clickDashBoard:(id)sender;
- (IBAction)clickbtnShop:(id)sender;
- (IBAction)clickbtnOrder:(id)sender;
- (IBAction)clickbtnProfile:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewMiddle;
@property (nonatomic, retain) NSMutableArray *arrFollowersList;
@property (nonatomic, retain) NSMutableArray *arrFollowingsList;
@property (weak, nonatomic) IBOutlet UITableView *tableViewMain;

@end

NS_ASSUME_NONNULL_END
