//
//  PGListTableViewCell.m
//  Cliicker
//
//  Created by Santanu Mondal on 24/06/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "PGListTableViewCell.h"

@implementation PGListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.imgVwPGProfile.layer.borderWidth = 1.0;
    self.imgVwPGProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    self.imgVwPGProfile.layer.cornerRadius = self.imgVwPGProfile.frame.size.width / 2;
    self.imgVwPGProfile.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
