//
//  PGListViewController.h
//  Cliicker
//
//  Created by Santanu Mondal on 24/06/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PGListViewController : UIViewController{
    NSPredicate *predicate;
}
- (IBAction)clickBack:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableViewPGList;
@property (nonatomic, retain) NSMutableArray *arrUserList;
@property (nonatomic, retain) NSArray *arrUserListNew;

@end

NS_ASSUME_NONNULL_END
