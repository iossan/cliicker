//
//  PGListViewController.m
//  Cliicker
//
//  Created by Santanu Mondal on 24/06/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "PGListViewController.h"
#import "PGListTableViewCell.h"
#import "AsyncImageView.h"
#import "PGDetailsViewController.h"

@interface PGListViewController ()

@end

@implementation PGListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    UIColor *color = [UIColor whiteColor];
    self.txtSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search by name" attributes:@{NSForegroundColorAttributeName: color}];
    
    [self.txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.view addGestureRecognizer:singleTap];
    
    [self getPGUserList];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture{
    [self.txtSearch resignFirstResponder];
}

-(void)getPGUserList{
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_PGLIST];
        NSLog(@"%@",strURL);
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(getPGListComp:)];
        [request setDidFailSelector:@selector(getPGListFail:)];
        [request addPostValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"USERID"] forKey:@"exclude_id"];
        [request addPostValue:@"" forKey:@"pg_name"];
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getPGListComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            if([[respon objectForKey:@"rs_row"] count]>0){
                self.arrUserList = [respon objectForKey:@"rs_row"];
                
                self.arrUserListNew = [self.arrUserList copy];
                
                [self.tableViewPGList reloadData];
            }
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getPGListFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

#pragma mark TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrUserListNew count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    PGListTableViewCell *cell = (PGListTableViewCell*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = (PGListTableViewCell*)[[[NSBundle mainBundle] loadNibNamed:@"PGListTableViewCell" owner:nil options:nil] firstObject];
    }
    
    if([self.arrUserListNew count]>0){
        cell.lblPGName.text = [[self.arrUserListNew objectAtIndex:indexPath.row] objectForKey:@"first_name"];
        cell.lblPGName.text = [cell.lblPGName.text stringByAppendingString:@" "];
        cell.lblPGName.text = [cell.lblPGName.text stringByAppendingString:[[self.arrUserListNew objectAtIndex:indexPath.row] objectForKey:@"last_name"]];
        cell.lblPGID.text = [cell.lblPGID.text stringByAppendingString:[[self.arrUserListNew objectAtIndex:indexPath.row] objectForKey:@"pg_clicker_code"]];
        cell.lblPGBudget.text = [cell.lblPGBudget.text stringByAppendingString:[[self.arrUserListNew objectAtIndex:indexPath.row] objectForKey:@"budget"]];
        
        NSString *strImageURL = [[self.arrUserListNew objectAtIndex:indexPath.row] objectForKey:@"featured_image_url"];
        cell.imgVwPGProfile.imageURL = [NSURL URLWithString:strImageURL];
        
        [cell.btnUserDetailsNext addTarget:self action:@selector(clickViewUserDetails:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnUserDetailsNext.tag = indexPath.row;
    }
    else{
    }
    
    return cell;
}

-(void)clickViewUserDetails:(id)sender{
    UIButton *btn = (UIButton*)sender;
    
    if([self.arrUserListNew count]>0){
        PGDetailsViewController *obj_ListUser;
        obj_ListUser = [[PGDetailsViewController alloc] initWithNibName:@"PGDetailsViewController" bundle:nil];
        obj_ListUser.strPGID = [[self.arrUserListNew objectAtIndex:btn.tag] objectForKey:@"id"];
        
        UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:obj_ListUser];
        navController.modalPresentationStyle=UIModalPresentationFormSheet;
        navController.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
        [self presentViewController:navController animated:YES completion:nil];
    }
}


#pragma mark TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidChange:(UITextField *)textfield{
    if([textfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length > 0){
        predicate = [NSPredicate predicateWithFormat:@"first_name contains[c] %@",textfield.text];
        self.arrUserListNew = [[self.arrUserList filteredArrayUsingPredicate:predicate] copy];
        
        [self.tableViewPGList reloadData];
    }else {
        self.arrUserListNew = [self.arrUserList copy];
    }
    [self.tableViewPGList reloadData];
}

- (IBAction)clickBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
