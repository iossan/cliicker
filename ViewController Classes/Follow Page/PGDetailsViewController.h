//
//  PGDetailsViewController.h
//  Cliicker
//
//  Created by Santanu Mondal on 24/06/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XHMenu.h"
#import "XHScrollMenu.h"

NS_ASSUME_NONNULL_BEGIN

@interface PGDetailsViewController : UIViewController<XHScrollMenuDelegate>{
    NSArray *arrItemss;
    
    UIRefreshControl *refreshControl;
    
    int pageCount;
    
    NSArray *arrFollowerIDs;
}
@property (nonatomic, retain) NSString *strPGID;
- (IBAction)clickBack:(id)sender;
@property (nonatomic, strong) XHScrollMenu *scrollMenuLower;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwUserCoverPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnFollow;
@property (weak, nonatomic) IBOutlet UIButton *btnUnfollow;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwProfilePic;
@property (weak, nonatomic) IBOutlet UIView *viewMiddle;
@property (weak, nonatomic) IBOutlet UIView *viewMain;
- (IBAction)clickFollow:(id)sender;
- (IBAction)clickUnfollow:(id)sender;

@property (nonatomic, retain) NSString *strUserPGID;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserCLId;
@property (weak, nonatomic) IBOutlet UILabel *lblUserAvBudget;
@property (weak, nonatomic) IBOutlet UILabel *lblUserFollowing;
@property (weak, nonatomic) IBOutlet UILabel *lblUserFollowers;
@property (weak, nonatomic) IBOutlet UILabel *lblUserReviewRating;
@property (nonatomic, retain) NSMutableArray *arrUserPhotos;
@property (weak, nonatomic) IBOutlet UITableView *tableViewUserPhotos;
@property (weak, nonatomic) IBOutlet UIView *viewAbout;
@property (weak, nonatomic) IBOutlet UILabel *lblTextAboutMe;
@property (weak, nonatomic) IBOutlet UILabel *lblUserEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblUserMobile;

@end

NS_ASSUME_NONNULL_END
