//
//  PGListTableViewCell.h
//  Cliicker
//
//  Created by Santanu Mondal on 24/06/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PGListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblPGName;
@property (weak, nonatomic) IBOutlet UILabel *lblPGID;
@property (weak, nonatomic) IBOutlet UILabel *lblPGBudget;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwPGProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnUserDetailsNext;

@end

NS_ASSUME_NONNULL_END
