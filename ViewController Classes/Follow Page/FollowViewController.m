//
//  FollowViewController.m
//  Cliicker
//
//  Created by Santanu Mondal on 07/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "FollowViewController.h"
#import "OrderHomeViewController.h"
#import "FollowTableViewCell.h"
#import "AsyncImageView.h"
#import "PGListViewController.h"

@interface FollowViewController ()

@end

@implementation FollowViewController

-(void)viewWillAppear:(BOOL)animated{
    [self getFollowFollowersList];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isFollowerORFollowing = YES;
    
    arrItemss = [NSArray arrayWithObjects:@"FOLLOWERS",@"FOLLOWING", nil];
    
    self.arrFollowersList = [NSMutableArray array];
    self.arrFollowingsList = [NSMutableArray array];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
        }
        else if(result.width == 375){
            self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 375, 35)];
        }
        else{
            self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 414, 35)];
        }
    }
    else{
        self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 768, 35)];
    }
    
    self.scrollMenuLower.backgroundColor = [UIColor clearColor];//[UIColor colorWithRed:3.0/255.0 green:44.0/255.0 blue:114.0/255.0 alpha:1.0];
    self.scrollMenuLower.indicatorTintColor = [UIColor colorWithRed:255.0/255.0 green:188.0/255.0 blue:0.0/255.0 alpha:1.0];
    self.scrollMenuLower.hasShadowForBoth = NO;
    self.scrollMenuLower.shouldUniformizeMenus = YES;
    self.scrollMenuLower.delegate = self;
    [self.viewMiddle addSubview:self.scrollMenuLower];
    
    NSMutableArray *menuss = [NSMutableArray array];
    
    for (int k = 0; k < [arrItemss count]; k ++) {
        XHMenu *menu1 = [[XHMenu alloc] init];
        
        menu1.title = [arrItemss objectAtIndex:k];
        menu1.titleFont = [UIFont fontWithName:@"Bebas" size:14.0];
        menu1.titleNormalColor = [UIColor whiteColor];
        menu1.titleSelectedColor = [UIColor colorWithRed:255.0/255.0 green:188.0/255.0 blue:0.0/255.0 alpha:1.0];
        
        [menuss addObject:menu1];
    }
    
    self.scrollMenuLower.menus = menuss;
    [self.scrollMenuLower reloadData];
    
}

-(void)getFollowFollowersList{
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_FOLLOW_LIST];
        NSLog(@"%@",strURL);
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(getFollowerListComp:)];
        [request setDidFailSelector:@selector(getFollowerListFail:)];
        [request addPostValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"USERID"] forKey:@"pgID"];
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getFollowerListComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            NSString *strFollowerCount;
            if(![[[respon objectForKey:@"rs_row"] objectForKey:@"no_followers"] isKindOfClass:[NSNull class]]){
                strFollowerCount = [[respon objectForKey:@"rs_row"] objectForKey:@"no_followers"];
                strFollowerCount = [strFollowerCount stringByAppendingString:@"   FOLLOWERS"];
            }
            else{
                strFollowerCount = @"0   FOLLOWERS";
            }
            
            
            NSString *strFollowingCount;
            if(![[[respon objectForKey:@"rs_row"] objectForKey:@"no_following"] isKindOfClass:[NSNull class]]){
                strFollowingCount = [[respon objectForKey:@"rs_row"] objectForKey:@"no_following"];
                strFollowingCount = [strFollowingCount stringByAppendingString:@"   FOLLOWING"];
            }
            else{
                strFollowingCount = @"0   FOLLOWING";
            }
            
            arrItemss = [NSArray arrayWithObjects:strFollowerCount,strFollowingCount, nil];
            NSLog(@"%@",arrItemss);
            
            NSMutableArray *menuss = [NSMutableArray array];
            
            for (int k = 0; k < [arrItemss count]; k ++) {
                XHMenu *menu1 = [[XHMenu alloc] init];
                
                menu1.title = [arrItemss objectAtIndex:k];
                menu1.titleFont = [UIFont fontWithName:@"Bebas" size:14.0];
                menu1.titleNormalColor = [UIColor whiteColor];
                menu1.titleSelectedColor = [UIColor colorWithRed:255.0/255.0 green:188.0/255.0 blue:0.0/255.0 alpha:1.0];
                
                [menuss addObject:menu1];
            }
            
            self.scrollMenuLower.menus = menuss;
            [self.scrollMenuLower reloadData];
            
            self.arrFollowersList = [[respon objectForKey:@"rs_row"] objectForKey:@"followers_arr"];
            self.arrFollowingsList = [[respon objectForKey:@"rs_row"] objectForKey:@"following_arr"];
            
            if(![[[respon objectForKey:@"rs_row"] objectForKey:@"following_ids"] isKindOfClass:[NSNull class]]){
                arrFollowerIDs = [[[respon objectForKey:@"rs_row"] objectForKey:@"following_ids"] componentsSeparatedByString:@","];
            }
            else{
                arrFollowerIDs = nil;
            }
            
            [self.tableViewMain reloadData];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getFollowerListFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

#pragma mark - XHScrollMenu Delegate Methods

- (void)scrollMenuDidSelected:(XHScrollMenu *)scrollMenu menuIndex:(NSUInteger)selectIndex {
    
    NSLog(@"selectIndex : %lu", (unsigned long)selectIndex);
    
    if(selectIndex == 0){
        isFollowerORFollowing = YES;
    }
    else{
        isFollowerORFollowing = NO;
    }
    
    [self.tableViewMain reloadData];
}

- (void)scrollMenuDidManagerSelected:(XHScrollMenu *)scrollMenu{
    
}

#pragma mark TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(isFollowerORFollowing == YES){
        if([self.arrFollowersList count]>0){
            return [self.arrFollowersList count];
        }
        else{
            return 1;
        }
    }
    else{
        if([self.arrFollowingsList count]>0){
            return [self.arrFollowingsList count];
        }
        else{
            return 1;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    FollowTableViewCell *cell = (FollowTableViewCell*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = (FollowTableViewCell*)[[[NSBundle mainBundle] loadNibNamed:@"FollowTableViewCell" owner:nil options:nil] firstObject];
    }
    
    if(isFollowerORFollowing == YES){
        if([self.arrFollowersList count]>0){
            cell.lblName.text = [[self.arrFollowersList objectAtIndex:indexPath.row] objectForKey:@"first_name"];
            cell.lblName.text = [cell.lblName.text stringByAppendingString:@" "];
            cell.lblName.text = [cell.lblName.text stringByAppendingString:[[self.arrFollowersList objectAtIndex:indexPath.row] objectForKey:@"last_name"]];
            cell.lblProID.text = [cell.lblProID.text stringByAppendingString:[[self.arrFollowersList objectAtIndex:indexPath.row] objectForKey:@"pg_clicker_code"]];
            
            NSString *strImageURL = [[self.arrFollowersList objectAtIndex:indexPath.row] objectForKey:@"featured_image_url"];
            cell.imgVwProfile.imageURL = [NSURL URLWithString:strImageURL];
            
            BOOL isFo = NO;
            
            if([arrFollowerIDs count]>0){
                for(int i=0;i<[arrFollowerIDs count];i++){
                    if([[arrFollowerIDs objectAtIndex:i] isEqualToString:[[self.arrFollowersList objectAtIndex:indexPath.row] objectForKey:@"id"]]){
                        isFo = YES;
                        break;
                    }
                }
            }
            
            if(isFo == NO){
                cell.btnFollow.layer.borderWidth = 0.5;
                cell.btnFollow.layer.cornerRadius = 5.0;
                cell.btnFollow.layer.borderColor = [UIColor clearColor].CGColor;
                [cell.btnFollow addTarget:self action:@selector(clickFollow:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnFollow.tag = indexPath.row;
            }
            else{
                cell.btnFollow.layer.borderWidth = 0.5;
                cell.btnFollow.layer.cornerRadius = 5.0;
                cell.btnFollow.layer.borderColor = [UIColor lightGrayColor].CGColor;
                [cell.btnFollow setTitle:@"UNFOLLOW" forState:UIControlStateNormal];
                [cell.btnFollow setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                [cell.btnFollow setBackgroundColor:[UIColor whiteColor]];
                [cell.btnFollow addTarget:self action:@selector(clickUnFollow:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnFollow.tag = indexPath.row;
            }
        }
        else{
            cell.lblName.text = @"No follower list";
            cell.lblProID.text = @"";
            cell.btnFollow.hidden = YES;
        }
    }
    else{
        if([self.arrFollowingsList count]>0){
            cell.lblName.text = [[self.arrFollowingsList objectAtIndex:indexPath.row] objectForKey:@"first_name"];
            cell.lblName.text = [cell.lblName.text stringByAppendingString:@" "];
            cell.lblName.text = [cell.lblName.text stringByAppendingString:[[self.arrFollowingsList objectAtIndex:indexPath.row] objectForKey:@"last_name"]];
            cell.lblProID.text = [cell.lblProID.text stringByAppendingString:[[self.arrFollowingsList objectAtIndex:indexPath.row] objectForKey:@"pg_clicker_code"]];
            
            NSString *strImageURL = [[self.arrFollowingsList objectAtIndex:indexPath.row] objectForKey:@"featured_image_url"];
            cell.imgVwProfile.imageURL = [NSURL URLWithString:strImageURL];
            
            cell.btnFollow.layer.borderWidth = 0.5;
            cell.btnFollow.layer.cornerRadius = 5.0;
            cell.btnFollow.layer.borderColor = [UIColor lightGrayColor].CGColor;
            [cell.btnFollow setTitle:@"UNFOLLOW" forState:UIControlStateNormal];
            [cell.btnFollow setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            [cell.btnFollow setBackgroundColor:[UIColor whiteColor]];
            [cell.btnFollow addTarget:self action:@selector(clickUnFollow:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnFollow.tag = indexPath.row;
        }
        else{
            cell.lblName.text = @"No following list.";
            cell.lblProID.text = @"";
            cell.btnFollow.hidden = YES;
        }
    }
    
    return cell;
}

-(void)clickFollow:(id)sender{
    NSLog(@"Follow");
    
    UIButton *btn = (UIButton*)sender;
    
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];

        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_FOLLOW_photographer];
        NSLog(@"%@",strURL);

        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(submitFollowStatusComp:)];
        [request setDidFailSelector:@selector(submitFollowStatusFail:)];
        [request addPostValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"USERID"] forKey:@"pgID"];
        if(isFollowerORFollowing == YES){
            [request addPostValue:[[self.arrFollowersList objectAtIndex:btn.tag] objectForKey:@"id"] forKey:@"followed_id"];
        }
        else{
            [request addPostValue:[[self.arrFollowingsList objectAtIndex:btn.tag] objectForKey:@"id"] forKey:@"followed_id"];
        }
        [request addPostValue:@"0" forKey:@"curr_status"];
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitFollowStatusComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"] && [[respon objectForKey:@"api_action_message"] isEqualToString:@"success"]){
            [self getFollowFollowersList];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitFollowStatusFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)clickUnFollow:(id)sender{
    NSLog(@"Unfollow");
    
    UIButton *btn = (UIButton*)sender;
    
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_FOLLOW_photographer];
        NSLog(@"%@",strURL);
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(submitUnFollowStatusComp:)];
        [request setDidFailSelector:@selector(submitUnFollowStatusFail:)];
        [request addPostValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"USERID"] forKey:@"pgID"];
        if(isFollowerORFollowing == YES){
            [request addPostValue:[[self.arrFollowersList objectAtIndex:btn.tag] objectForKey:@"id"] forKey:@"followed_id"];
        }
        else{
            [request addPostValue:[[self.arrFollowingsList objectAtIndex:btn.tag] objectForKey:@"id"] forKey:@"followed_id"];
        }
        [request addPostValue:@"1" forKey:@"curr_status"];
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitUnFollowStatusComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"] && [[respon objectForKey:@"api_action_message"] isEqualToString:@"success"]){
            
            [self getFollowFollowersList];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitUnFollowStatusFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

- (IBAction)clickDashBoard:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"dashboard"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnProfile:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"profile"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnShop:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Shop" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"shop"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnOrder:(id)sender {
    OrderHomeViewController *objDash;
    objDash = [[OrderHomeViewController alloc] initWithNibName:@"OrderHomeViewController" bundle:nil];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:objDash animated:NO];
}


#pragma mark TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self openUserSearchView];
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)openUserSearchView{
    PGListViewController *obj_ListUser;
    obj_ListUser = [[PGListViewController alloc] initWithNibName:@"PGListViewController" bundle:nil];
    //obj_ListUser.dictProfileData = self.dictProfileData;
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:obj_ListUser];
    navController.modalPresentationStyle=UIModalPresentationFormSheet;
    navController.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
    [self presentViewController:navController animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
