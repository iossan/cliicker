//
//  OrderHistoryTableViewCell.h
//  Cliicker
//
//  Created by Santanu Mondal on 04/06/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgVwTemplate;
@property (weak, nonatomic) IBOutlet UILabel *lblDomainName;
@property (weak, nonatomic) IBOutlet UILabel *lblTempID;
@property (weak, nonatomic) IBOutlet UILabel *lblClientName;

@end

NS_ASSUME_NONNULL_END
