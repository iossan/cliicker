//
//  OrderHomeViewController.h
//  Cliicker
//
//  Created by Santanu Mondal on 11/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionSheetStringPicker.h"
#import "ActionSheetLocalePicker.h"
#import "ActionSheetPicker.h"
#import "XHMenu.h"
#import "XHScrollMenu.h"
#import "JJMaterialTextfield.h"
#import "TemplateListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderHomeViewController : UIViewController<XHScrollMenuDelegate,SelectTemplateDelegate,UIActionSheetDelegate,UITextFieldDelegate>{
    NSArray *arrItemss;
    
    BOOL isOpenORNot;
}
@property (nonatomic, retain) NSMutableArray *arrOrderHisList;
@property (nonatomic, retain) NSString *strOrderID;
@property (nonatomic, retain) NSString *strCurrentScrollView;
@property (nonatomic, strong) XHScrollMenu *scrollMenuLower;
- (IBAction)clickDashBoard:(id)sender;
- (IBAction)clickbtnShop:(id)sender;
- (IBAction)clickbtnFollow:(id)sender;
- (IBAction)clickbtnProfile:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewUpper;
@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UITableView *tableViewOrderHostory;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollOrderFirst;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollOrderSecond;

//
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtCusFirstName;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtCusLastName;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtCusEmail;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtCusContactNo;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;
@property (weak, nonatomic) IBOutlet UITextView *txtVwNotes;
@property (weak, nonatomic) IBOutlet UIButton *btnNextOne;
- (IBAction)clickNextOne:(id)sender;
//

//
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) AbstractActionSheetPicker *actionSheetPicker;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtBrideName;
@property (weak, nonatomic) IBOutlet UITextView *txtVwAboutBride;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtGroomName;
@property (weak, nonatomic) IBOutlet UITextView *txtVwAboutGroom;
@property (weak, nonatomic) IBOutlet UILabel *lblAboutBride;
@property (weak, nonatomic) IBOutlet UILabel *lblAboutGroom;
@property (weak, nonatomic) IBOutlet JJMaterialTextfield *txtSubdomainName;
- (IBAction)clickValidateURL:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnValidateURL;
@property (weak, nonatomic) IBOutlet UILabel *lblDateFirstMeeting;
- (IBAction)clickDateFirstMeeting:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblDateMarriageFixedOn;
- (IBAction)clickDateMarriageFixedOn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblDateGotEngagedOn;
- (IBAction)clickDateGotEngagedOn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblDateLegallyMarriedOn;
- (IBAction)clickDateLegallyMarriedOn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblDateSocialMarriage;
- (IBAction)clickDateSocialMarriage:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblDateWeddingReception;
- (IBAction)clickDateWeddingReception:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTemplateName;
@property (weak, nonatomic) IBOutlet UIButton *btnChooseTemplate;
- (IBAction)clickChooseTemplate:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewTemplateDetails;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwTemplate;
@property (weak, nonatomic) IBOutlet UILabel *lblTemplateNamee;
@property (weak, nonatomic) IBOutlet UILabel *lblTemplateID;
@property (weak, nonatomic) IBOutlet UIButton *btnTemplateView;
- (IBAction)clickViewTemplate:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnPreviousOne;
@property (weak, nonatomic) IBOutlet UIButton *btnNextTwo;
- (IBAction)clickNextTwo:(id)sender;
- (IBAction)clickPreviousOne:(id)sender;
@property (nonatomic, retain) NSString *strTempID;
@property (nonatomic, retain) NSString *strTempURLBIG;
//

//
@property (nonatomic, retain) NSMutableArray *arrImageCateUpDateCatName;
@property (nonatomic, retain) NSMutableArray *arrImageCateUpDateGDriveLink;
@property (nonatomic, retain) NSMutableArray *arrImageCateUpDateDropboxLink;
@property (nonatomic, retain) NSMutableArray *arrImageCateDetails;
@property (nonatomic, retain) NSString *strImageCateType;
@property (strong, nonatomic) IBOutlet UIScrollView *ScrollOrderThird;
@property (weak, nonatomic) IBOutlet UIView *viewCateUnCategorised;
@property (weak, nonatomic) IBOutlet UIButton *btnPreviousTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnNextThree;
- (IBAction)clickPreviousTwo:(id)sender;
- (IBAction)clickNextThree:(id)sender;
- (IBAction)clickUncategoriesd:(id)sender;
- (IBAction)clickCategorised:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewUnCategorised;
@property (weak, nonatomic) IBOutlet UIView *viewUnCateShareLink;
@property (weak, nonatomic) IBOutlet UIView *viewUnCateNote;

@property (strong, nonatomic) UIScrollView *scrollViewThirdCategorised;
@property (retain, nonatomic) JJMaterialTextfield *txtCreateAlbumName;
@property (retain, nonatomic) JJMaterialTextfield *txtCreateGDrive;
@property (retain, nonatomic) JJMaterialTextfield *txtCreateDropbox;
- (IBAction)clickGDriveUnCatImage:(id)sender;
- (IBAction)clickDropboxUnCatImage:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtGDriveLinkUnCatImage;
@property (weak, nonatomic) IBOutlet UITextField *txtDropboxUnCaImage;
- (IBAction)clickBackUnCatImageType:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBackUnCatImageType;

//
@property (strong, nonatomic) UIScrollView *scrollViewFourVideoCategorised;
@property (nonatomic, retain) NSMutableArray *arrVideoCateUpDateCatName;
@property (nonatomic, retain) NSMutableArray *arrVideoCateUpDateGDriveLink;
@property (nonatomic, retain) NSMutableArray *arrVideoCateUpDateDropboxLink;
@property (nonatomic, retain) NSMutableArray *arrVideoCateDetails;
@property (retain, nonatomic) JJMaterialTextfield *txtCreateVideoAlbumName;
@property (retain, nonatomic) JJMaterialTextfield *txtCreateVideoGDrive;
@property (retain, nonatomic) JJMaterialTextfield *txtCreateVideoDropbox;


//
@property (strong, nonatomic) IBOutlet UIScrollView *scrollOrderConfirmation;
@property (weak, nonatomic) IBOutlet UILabel *lblCusName;
@property (weak, nonatomic) IBOutlet UILabel *lblCusEmailID;
@property (weak, nonatomic) IBOutlet UILabel *lblCusPhNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblCusNotes;
@property (weak, nonatomic) IBOutlet UILabel *lblCusWebsiteName;
@property (weak, nonatomic) IBOutlet UILabel *lblBrideName;
@property (weak, nonatomic) IBOutlet UILabel *lblBrideNotes;
@property (weak, nonatomic) IBOutlet UILabel *lblGroomName;
@property (weak, nonatomic) IBOutlet UILabel *lblGroomNotes;
@property (weak, nonatomic) IBOutlet UILabel *lblDateFirstMeet;
@property (weak, nonatomic) IBOutlet UILabel *lblDateEngagedOn;
@property (weak, nonatomic) IBOutlet UILabel *lblDateMaFixOn;
@property (weak, nonatomic) IBOutlet UILabel *lblDateLegMarOn;
@property (weak, nonatomic) IBOutlet UILabel *lblDateSocMa;
@property (weak, nonatomic) IBOutlet UILabel *lblDateRec;
@property (weak, nonatomic) IBOutlet UIView *viewAlbumDetailsLast;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwAlbumLast;
@property (weak, nonatomic) IBOutlet UILabel *lblAlbumNameLast;
@property (weak, nonatomic) IBOutlet UILabel *lblTemplateIDLast;
@property (weak, nonatomic) IBOutlet UILabel *lblCostItemTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblCoseGSTTax;
@property (weak, nonatomic) IBOutlet UILabel *lblCostOrderTotal;
@property (weak, nonatomic) IBOutlet UIView *viewNotesLast;
@property (weak, nonatomic) IBOutlet UIView *viewDateOne;
@property (weak, nonatomic) IBOutlet UIView *viewDateTwo;
@property (weak, nonatomic) IBOutlet UIView *viewDateThree;
@property (weak, nonatomic) IBOutlet UIView *viewDateFour;
@property (weak, nonatomic) IBOutlet UIView *viewDateFive;
@property (weak, nonatomic) IBOutlet UIView *viewDateSix;
- (IBAction)clickPreviousFive:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnPreviousFive;
- (IBAction)clickProccedToPay:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnProccedToPay;
@property (assign) int ItemAmount;
@property (assign) int OrderAmount;

@end

NS_ASSUME_NONNULL_END
