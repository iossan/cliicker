//
//  OrderHistoryTableViewCell.m
//  Cliicker
//
//  Created by Santanu Mondal on 04/06/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "OrderHistoryTableViewCell.h"

@implementation OrderHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.imgVwTemplate.layer.borderWidth = 0.5;
    self.imgVwTemplate.layer.cornerRadius = 5.0;
    self.imgVwTemplate.layer.borderColor = [UIColor clearColor].CGColor;
    self.imgVwTemplate.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
