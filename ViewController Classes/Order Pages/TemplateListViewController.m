//
//  TemplateListViewController.m
//  Cliicker
//
//  Created by Santanu Mondal on 19/04/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "TemplateListViewController.h"
#import "TemplateTableViewCell.h"
#import "AsyncImageView.h"

@interface TemplateListViewController ()

@end

@implementation TemplateListViewController
@synthesize arrTemplateList;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    self.arrTemplateList = [NSMutableArray array];
    
    UIColor *color = [UIColor whiteColor];
    self.txtSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search by name" attributes:@{NSForegroundColorAttributeName: color}];
    
    self.btnSearch.layer.borderWidth = 0.5;
    self.btnSearch.layer.cornerRadius = 5.0;
    self.btnSearch.layer.borderColor = [UIColor clearColor].CGColor;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.view addGestureRecognizer:singleTap];
    
    [self getTemplateList];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture{
    [self.txtSearch resignFirstResponder];
}

-(void)getTemplateList{
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_TEMPLATE_LIST];
        NSLog(@"%@",strURL);
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(getTemplateListComp:)];
        [request setDidFailSelector:@selector(getTemplateListFail:)];
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        [request addPostValue:self.txtSearch.text forKey:@"srchtext"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getTemplateListComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            self.arrTemplateList = [respon objectForKey:@"rs_row"];
            
            [self.tableViewTemplateList reloadData];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getTemplateListFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

#pragma mark TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrTemplateList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 250;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    TemplateTableViewCell *cell = (TemplateTableViewCell*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = (TemplateTableViewCell*)[[[NSBundle mainBundle] loadNibNamed:@"TemplateTableViewCell" owner:nil options:nil] firstObject];
    }
    
    if([self.arrTemplateList count]>0){
        cell.lblTemplateName.text = [[self.arrTemplateList objectAtIndex:indexPath.row] objectForKey:@"theme_name"];
        cell.lblTemplateID.text = [cell.lblTemplateID.text stringByAppendingString:[[self.arrTemplateList objectAtIndex:indexPath.row] objectForKey:@"theme_code"]];
        cell.lblTemplateDesc.text = [[self.arrTemplateList objectAtIndex:indexPath.row] objectForKey:@"short_description"];
        
        NSString *strImageURL = [[self.arrTemplateList objectAtIndex:indexPath.row] objectForKey:@"img_url"];
        strImageURL = [strImageURL stringByAppendingString:[[self.arrTemplateList objectAtIndex:indexPath.row] objectForKey:@"theme_thumbnail_image"]];
        
        cell.imgVwTemplateImage.imageURL = [NSURL URLWithString:strImageURL];
        
        [cell.btnView addTarget:self action:@selector(clickViewTemplate:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnView.tag = indexPath.row;
        
        [cell.btnSelect addTarget:self action:@selector(clickSelectTemplate:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnSelect.tag = indexPath.row;
    }
    else{
    }
    
    return cell;
}

-(void)clickViewTemplate:(id)sender{
    UIButton *btn = (UIButton*)sender;
    
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:[[self.arrTemplateList objectAtIndex:btn.tag] objectForKey:@"theme_template_url"]];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Opened url");
        }
    }];
}

-(void)clickSelectTemplate:(id)sender{
    UIButton *btn = (UIButton*)sender;
    
    if ([self.delegate respondsToSelector:@selector(selectTemplate:TemplateName:TemplateURL: TemplateCLRDL: TemplateURLBIG:)])
    {
        NSString *strImageURL = [[self.arrTemplateList objectAtIndex:btn.tag] objectForKey:@"img_url"];
        strImageURL = [strImageURL stringByAppendingString:[[self.arrTemplateList objectAtIndex:btn.tag] objectForKey:@"theme_thumbnail_image"]];
        
        [self.delegate selectTemplate:[[self.arrTemplateList objectAtIndex:btn.tag] objectForKey:@"id"] TemplateName:[[self.arrTemplateList objectAtIndex:btn.tag] objectForKey:@"theme_name"] TemplateURL:strImageURL TemplateCLRDL:[[self.arrTemplateList objectAtIndex:btn.tag] objectForKey:@"theme_code"] TemplateURLBIG:[[self.arrTemplateList objectAtIndex:btn.tag] objectForKey:@"theme_template_url"]];
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)clickSearch:(id)sender {
    [self getTemplateList];
}

- (IBAction)clickBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
