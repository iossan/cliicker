//
//  TemplateListViewController.h
//  Cliicker
//
//  Created by Santanu Mondal on 19/04/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SelectTemplateDelegate <NSObject>
-(void)selectTemplate:(NSString*)templateID TemplateName:(NSString*)templatename TemplateURL:(NSString*)templateurl TemplateCLRDL:(NSString*)templateCLRid TemplateURLBIG:(NSString*)templateurlbig;
@end

@interface TemplateListViewController : UIViewController
@property (nonatomic, weak) id <SelectTemplateDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *arrTemplateList;
- (IBAction)clickBack:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
- (IBAction)clickSearch:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableViewTemplateList;

@end

NS_ASSUME_NONNULL_END
