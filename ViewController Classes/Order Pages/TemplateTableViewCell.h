//
//  TemplateTableViewCell.h
//  Cliicker
//
//  Created by Santanu Mondal on 22/04/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TemplateTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgVwTemplateImage;
@property (weak, nonatomic) IBOutlet UILabel *lblTemplateName;
@property (weak, nonatomic) IBOutlet UILabel *lblTemplateID;
@property (weak, nonatomic) IBOutlet UILabel *lblTemplateDesc;
@property (weak, nonatomic) IBOutlet UIButton *btnView;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UIView *viewMain;

@end

NS_ASSUME_NONNULL_END
