//
//  TemplateTableViewCell.m
//  Cliicker
//
//  Created by Santanu Mondal on 22/04/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "TemplateTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation TemplateTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.viewMain.layer.borderWidth = 0.5;
    self.viewMain.layer.cornerRadius = 5.0;
    self.viewMain.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.imgVwTemplateImage.layer.borderWidth = 0.5;
    self.imgVwTemplateImage.layer.cornerRadius = 5.0;
    self.imgVwTemplateImage.layer.borderColor = [UIColor clearColor].CGColor;
    self.imgVwTemplateImage.clipsToBounds = YES;
    
    self.btnView.layer.borderWidth = 0.5;
    self.btnView.layer.cornerRadius = 5.0;
    self.btnView.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.btnSelect.layer.borderWidth = 0.5;
    self.btnSelect.layer.cornerRadius = 5.0;
    self.btnSelect.layer.borderColor = [UIColor clearColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
