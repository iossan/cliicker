//
//  OrderHomeViewController.m
//  Cliicker
//
//  Created by Santanu Mondal on 11/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "OrderHomeViewController.h"
#import "AsyncImageView.h"
#import "TemplateListViewController.h"
#import "OrderHistoryTableViewCell.h"
//#import <stdlib.h>
#import <PlugNPlay/PlugNPlay.h>
#import <CommonCrypto/CommonDigest.h>

#define kMerchantKey @"8IsBo22Q"
#define kMerchantID @"8IsBo22Q"
#define kMerchantSalt @"bMF1huMQ9X"

@interface OrderHomeViewController ()

@end

@implementation OrderHomeViewController
@synthesize strOrderID,strTempID,strTempURLBIG;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isOpenORNot = NO;
    
    self.arrOrderHisList = [NSMutableArray array];
    
    self.arrImageCateUpDateCatName = [NSMutableArray array];
    self.arrImageCateUpDateGDriveLink = [NSMutableArray array];
    self.arrImageCateUpDateDropboxLink = [NSMutableArray array];
    
    self.arrVideoCateUpDateCatName = [NSMutableArray array];
    self.arrVideoCateUpDateGDriveLink = [NSMutableArray array];
    self.arrVideoCateUpDateDropboxLink = [NSMutableArray array];
    
    self.strImageCateType = @"";
    self.strCurrentScrollView = @"1";
    self.strOrderID = @"";
    self.strTempID = @"";
    self.strTempURLBIG = @"";
    
    // SCROLLVIEW FIRST //
    
    self.txtCusFirstName.enableMaterialPlaceHolder = YES;
    self.txtCusLastName.enableMaterialPlaceHolder = YES;
    self.txtCusEmail.enableMaterialPlaceHolder = YES;
    self.txtCusContactNo.enableMaterialPlaceHolder = YES;
    self.txtCusFirstName.lineColor = [UIColor clearColor];
    self.txtCusLastName.lineColor = [UIColor clearColor];
    self.txtCusEmail.lineColor = [UIColor clearColor];
    self.txtCusContactNo.lineColor = [UIColor clearColor];
    
    self.txtVwNotes.text = @"Note (If any)";
    self.txtVwNotes.textColor = [UIColor lightGrayColor];
    
    self.btnNextOne.layer.borderWidth = 0.5;
    self.btnNextOne.layer.cornerRadius = 5.0;
    self.btnNextOne.layer.borderColor = [UIColor clearColor].CGColor;
    
    [self.viewMain addSubview:self.scrollOrderFirst];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.scrollOrderFirst.frame = CGRectMake(0, 0, 320, 458);
        }
        else if(result.width == 375){
            self.scrollOrderFirst.frame = CGRectMake(0, 0, 375, 622);
        }
        else{
            self.scrollOrderFirst.frame = CGRectMake(0, 0, 414, 454);
        }
    }
    else{
        self.scrollOrderFirst.frame = CGRectMake(0, 0, 768, 865);
    }
    
    // END //
    
    // SCROLLVIEW SECOND //
    
    self.txtBrideName.enableMaterialPlaceHolder = YES;
    self.txtGroomName.enableMaterialPlaceHolder = YES;
    self.txtSubdomainName.enableMaterialPlaceHolder = YES;
    self.txtBrideName.lineColor = [UIColor clearColor];
    self.txtGroomName.lineColor = [UIColor clearColor];
    self.txtSubdomainName.lineColor = [UIColor clearColor];
    
    self.txtVwAboutBride.text = @"About the bride...";
    self.txtVwAboutBride.textColor = [UIColor lightGrayColor];
    
    self.txtVwAboutGroom.text = @"About the groom...";
    self.txtVwAboutGroom.textColor = [UIColor lightGrayColor];
    
    self.btnValidateURL.layer.borderWidth = 0.5;
    self.btnValidateURL.layer.cornerRadius = 5.0;
    self.btnValidateURL.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.btnChooseTemplate.layer.borderWidth = 0.5;
    self.btnChooseTemplate.layer.cornerRadius = 5.0;
    self.btnChooseTemplate.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.btnTemplateView.layer.borderWidth = 0.5;
    self.btnTemplateView.layer.cornerRadius = 5.0;
    self.btnTemplateView.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.btnPreviousOne.layer.borderWidth = 0.5;
    self.btnPreviousOne.layer.cornerRadius = 5.0;
    self.btnPreviousOne.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.btnNextTwo.layer.borderWidth = 0.5;
    self.btnNextTwo.layer.cornerRadius = 5.0;
    self.btnNextTwo.layer.borderColor = [UIColor clearColor].CGColor;
    
    [self.viewMain addSubview:self.scrollOrderSecond];
    self.scrollOrderSecond.hidden = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.scrollOrderSecond.frame = CGRectMake(0, 0, 320, 458);
        }
        else if(result.width == 375){
            self.scrollOrderSecond.frame = CGRectMake(0, 0, 375, 622);
        }
        else{
            self.scrollOrderSecond.frame = CGRectMake(0, 0, 414, 454);
        }
    }
    else{
        self.scrollOrderSecond.frame = CGRectMake(0, 0, 768, 865);
    }
    
    // END //
    
    // SCROLLVIEW THIRD //
    
    self.btnPreviousTwo.layer.borderWidth = 0.5;
    self.btnPreviousTwo.layer.cornerRadius = 5.0;
    self.btnPreviousTwo.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.btnNextThree.layer.borderWidth = 0.5;
    self.btnNextThree.layer.cornerRadius = 5.0;
    self.btnNextThree.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.viewUnCateNote.layer.borderWidth = 0.5;
    self.viewUnCateNote.layer.cornerRadius = 5.0;
    self.viewUnCateNote.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.viewUnCateShareLink.layer.borderWidth = 0.5;
    self.viewUnCateShareLink.layer.cornerRadius = 5.0;
    self.viewUnCateShareLink.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [self.viewMain addSubview:self.ScrollOrderThird];
    self.ScrollOrderThird.hidden = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.ScrollOrderThird.frame = CGRectMake(0, 0, 320, 458);
        }
        else if(result.width == 375){
            self.ScrollOrderThird.frame = CGRectMake(0, 0, 375, 622);
        }
        else{
            self.ScrollOrderThird.frame = CGRectMake(0, 0, 414, 454);
        }
    }
    else{
        self.ScrollOrderThird.frame = CGRectMake(0, 0, 768, 865);
    }
    
    // END //
    
    self.btnBackUnCatImageType.layer.borderWidth = 0.5;
    self.btnBackUnCatImageType.layer.cornerRadius = 5.0;
    self.btnBackUnCatImageType.layer.borderColor = [UIColor clearColor].CGColor;
    
    // SCROLLVIEW THIRD CATEGORISED //
    self.arrImageCateDetails = [NSMutableArray array];
    [self createScrollViewThirdCategorised];
    self.scrollViewThirdCategorised.hidden = YES;
    // END //
    
    // SCROLLVIEW FOUR VIDEO CATEGORISED //
    self.arrVideoCateDetails = [NSMutableArray array];
    [self createScrollViewFourCategorised];
    self.scrollViewFourVideoCategorised.hidden = YES;
    // END //
    
    // SCROLLVIEW FIVE //
    
    self.btnPreviousFive.layer.borderWidth = 0.5;
    self.btnPreviousFive.layer.cornerRadius = 5.0;
    self.btnPreviousFive.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.btnProccedToPay.layer.borderWidth = 0.5;
    self.btnProccedToPay.layer.cornerRadius = 5.0;
    self.btnProccedToPay.layer.borderColor = [UIColor clearColor].CGColor;
    
    self.viewAlbumDetailsLast.layer.borderWidth = 0.5;
    self.viewAlbumDetailsLast.layer.cornerRadius = 5.0;
    self.viewAlbumDetailsLast.layer.borderColor = [UIColor colorWithRed:247.0/255.0 green:140.0/255.0 blue:154.0/255.0 alpha:1.0].CGColor;
    
    self.viewNotesLast.layer.borderWidth = 0.5;
    self.viewNotesLast.layer.cornerRadius = 5.0;
    self.viewNotesLast.layer.borderColor = [UIColor colorWithRed:153.0/255.0 green:178.0/255.0 blue:214.0/255.0 alpha:1.0].CGColor;
    
    self.viewDateOne.layer.borderWidth = 0.5;
    self.viewDateOne.layer.cornerRadius = 5.0;
    self.viewDateOne.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    self.viewDateTwo.layer.borderWidth = 0.5;
    self.viewDateTwo.layer.cornerRadius = 5.0;
    self.viewDateTwo.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    self.viewDateThree.layer.borderWidth = 0.5;
    self.viewDateThree.layer.cornerRadius = 5.0;
    self.viewDateThree.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    self.viewDateFour.layer.borderWidth = 0.5;
    self.viewDateFour.layer.cornerRadius = 5.0;
    self.viewDateFour.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    self.viewDateFive.layer.borderWidth = 0.5;
    self.viewDateFive.layer.cornerRadius = 5.0;
    self.viewDateFive.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    self.viewDateSix.layer.borderWidth = 0.5;
    self.viewDateSix.layer.cornerRadius = 5.0;
    self.viewDateSix.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    [self.viewMain addSubview:self.scrollOrderConfirmation];
    self.scrollOrderConfirmation.hidden = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.scrollOrderConfirmation.frame = CGRectMake(0, 0, 320, 458);
        }
        else if(result.width == 375){
            self.scrollOrderConfirmation.frame = CGRectMake(0, 0, 375, 622);
        }
        else{
            self.scrollOrderConfirmation.frame = CGRectMake(0, 0, 414, 454);
        }
    }
    else{
        self.scrollOrderConfirmation.frame = CGRectMake(0, 0, 768, 865);
    }
    
    // END //
    
    
    arrItemss = [NSArray arrayWithObjects:@"PLACE   ORDER",@"ORDER   HISTORY", nil];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
        }
        else if(result.width == 375){
            self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 375, 35)];
        }
        else{
            self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 414, 35)];
        }
    }
    else{
        self.scrollMenuLower = [[XHScrollMenu alloc] initWithFrame:CGRectMake(0, 0, 768, 35)];
    }
    
    self.scrollMenuLower.backgroundColor = [UIColor clearColor];//[UIColor colorWithRed:3.0/255.0 green:44.0/255.0 blue:114.0/255.0 alpha:1.0];
    self.scrollMenuLower.indicatorTintColor = [UIColor colorWithRed:255.0/255.0 green:188.0/255.0 blue:0.0/255.0 alpha:1.0];
    self.scrollMenuLower.hasShadowForBoth = NO;
    self.scrollMenuLower.shouldUniformizeMenus = YES;
    self.scrollMenuLower.delegate = self;
    [self.viewUpper addSubview:self.scrollMenuLower];
    
    NSMutableArray *menuss = [NSMutableArray array];
    
    for (int k = 0; k < [arrItemss count]; k ++) {
        XHMenu *menu1 = [[XHMenu alloc] init];
        
        menu1.title = [arrItemss objectAtIndex:k];
        menu1.titleFont = [UIFont fontWithName:@"Bebas" size:14.0];
        menu1.titleNormalColor = [UIColor whiteColor];
        menu1.titleSelectedColor = [UIColor colorWithRed:255.0/255.0 green:188.0/255.0 blue:0.0/255.0 alpha:1.0];
        
        [menuss addObject:menu1];
    }
    
    self.scrollMenuLower.menus = menuss;
    [self.scrollMenuLower reloadData];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.view addGestureRecognizer:singleTap];
    
    [self getOrderHistoryList];
}

-(void)getOrderHistoryList{
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_ORDER_HISTORY];
        NSLog(@"%@",strURL);
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(getOrderHistoryComp:)];
        [request setDidFailSelector:@selector(getOrderHistoryFail:)];
        
        [request addPostValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"USERID"] forKey:@"photographer_id"];
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getOrderHistoryComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            self.arrOrderHisList = [respon objectForKey:@"order_list"];
            
            [self.tableViewOrderHostory reloadData];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getOrderHistoryFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)createScrollViewFourCategorised{
    for (UIView *v in self.scrollViewFourVideoCategorised.subviews) {
        if ([v isKindOfClass:[UIView class]]) {
            [v removeFromSuperview];
        }
    }
    
    [self.arrVideoCateUpDateCatName removeAllObjects];
    [self.arrVideoCateUpDateGDriveLink removeAllObjects];
    [self.arrVideoCateUpDateDropboxLink removeAllObjects];
    
    self.scrollViewFourVideoCategorised = [[UIScrollView alloc] init];
    self.scrollViewFourVideoCategorised.backgroundColor = [UIColor whiteColor];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.scrollViewFourVideoCategorised.frame = CGRectMake(0, 0, 320, 458);
        }
        else if(result.width == 375){
            self.scrollViewFourVideoCategorised.frame = CGRectMake(0, 0, 375, 622);
        }
        else{
            self.scrollViewFourVideoCategorised.frame = CGRectMake(0, 0, 414, 454);
        }
    }
    else{
        self.scrollViewFourVideoCategorised.frame = CGRectMake(0, 0, 768, 865);
    }
    
    [self.viewMain addSubview:self.scrollViewFourVideoCategorised];
    
    int viewHeight = 0;
    
    UIView *vwOne = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwOne.frame = CGRectMake(0, viewHeight, 320, 40);
        }
        else if(result.width == 375){
            vwOne.frame = CGRectMake(0, viewHeight, 375, 40);
        }
        else{
            vwOne.frame = CGRectMake(0, viewHeight, 414, 40);
        }
    }
    else{
        vwOne.frame = CGRectMake(0, viewHeight, 768, 40);
    }
    vwOne.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    [self.scrollViewFourVideoCategorised addSubview:vwOne];
    
    UIImageView *imgVwOne = [[UIImageView alloc] init];
    imgVwOne.frame = CGRectMake(10, 8, 25, 25);
    imgVwOne.image = [UIImage imageNamed:@"btnright.png"];
    [vwOne addSubview:imgVwOne];
    
    UILabel *lblOne = [[UILabel alloc] init];
    lblOne.frame = CGRectMake(45, 11, 140, 20);
    lblOne.text = @"CUSTOMER  DETAILS";
    lblOne.textColor = [UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0];
    lblOne.font = [UIFont fontWithName:@"Bebas" size:17.0];
    [vwOne addSubview:lblOne];
    
    viewHeight = viewHeight + 41;
    
    UIView *vwTwo = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwTwo.frame = CGRectMake(0, viewHeight, 320, 40);
        }
        else if(result.width == 375){
            vwTwo.frame = CGRectMake(0, viewHeight, 375, 40);
        }
        else{
            vwTwo.frame = CGRectMake(0, viewHeight, 414, 40);
        }
    }
    else{
        vwTwo.frame = CGRectMake(0, viewHeight, 768, 40);
    }
    vwTwo.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    [self.scrollViewFourVideoCategorised addSubview:vwTwo];
    
    UIImageView *imgVwTwo = [[UIImageView alloc] init];
    imgVwTwo.frame = CGRectMake(10, 8, 25, 25);
    imgVwTwo.image = [UIImage imageNamed:@"btnright.png"];
    [vwTwo addSubview:imgVwTwo];
    
    UILabel *lblTwo = [[UILabel alloc] init];
    lblTwo.frame = CGRectMake(45, 11, 200, 20);
    lblTwo.text = @"CREATE  YOUR  ALBUM";
    lblTwo.textColor = [UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0];
    lblTwo.font = [UIFont fontWithName:@"Bebas" size:17.0];
    [vwTwo addSubview:lblTwo];
    
    viewHeight = viewHeight + 41;
    
    UIView *vwThree = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwThree.frame = CGRectMake(0, viewHeight, 320, 40);
        }
        else if(result.width == 375){
            vwThree.frame = CGRectMake(0, viewHeight, 375, 40);
        }
        else{
            vwThree.frame = CGRectMake(0, viewHeight, 414, 40);
        }
    }
    else{
        vwThree.frame = CGRectMake(0, viewHeight, 768, 40);
    }
    vwThree.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    [self.scrollViewFourVideoCategorised addSubview:vwThree];
    
    UIImageView *imgVwThree = [[UIImageView alloc] init];
    imgVwThree.frame = CGRectMake(10, 8, 25, 25);
    imgVwThree.image = [UIImage imageNamed:@"btnright.png"];
    [vwThree addSubview:imgVwThree];
    
    UILabel *lblThree = [[UILabel alloc] init];
    lblThree.frame = CGRectMake(45, 11, 140, 20);
    lblThree.text = @"UPLOAD  IMAGES";
    lblThree.textColor = [UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0];
    lblThree.font = [UIFont fontWithName:@"Bebas" size:17.0];
    [vwThree addSubview:lblThree];
    
    viewHeight = viewHeight + 41;
    
    UIView *vwFour = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwFour.frame = CGRectMake(0, viewHeight, 320, 40);
        }
        else if(result.width == 375){
            vwFour.frame = CGRectMake(0, viewHeight, 375, 40);
        }
        else{
            vwFour.frame = CGRectMake(0, viewHeight, 414, 40);
        }
    }
    else{
        vwFour.frame = CGRectMake(0, viewHeight, 768, 40);
    }
    vwFour.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    [self.scrollViewFourVideoCategorised addSubview:vwFour];
    
    UIImageView *imgVwFour = [[UIImageView alloc] init];
    imgVwFour.frame = CGRectMake(10, 8, 25, 25);
    imgVwFour.image = [UIImage imageNamed:@"btnred4.png"];
    [vwFour addSubview:imgVwFour];
    
    UILabel *lblFour = [[UILabel alloc] init];
    lblFour.frame = CGRectMake(45, 11, 140, 20);
    lblFour.text = @"UPLOAD  VIDEOS";
    lblFour.textColor = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0];
    lblFour.font = [UIFont fontWithName:@"Bebas" size:17.0];
    [vwFour addSubview:lblFour];
    
    viewHeight = viewHeight + 55;
    
    UIView *vwCateNote = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwCateNote.frame = CGRectMake(20, viewHeight, 280, 55);
        }
        else if(result.width == 375){
            vwCateNote.frame = CGRectMake(20, viewHeight, 335, 55);
        }
        else{
            vwCateNote.frame = CGRectMake(20, viewHeight, 374, 55);
        }
    }
    else{
        vwCateNote.frame = CGRectMake(20, viewHeight, 728, 55);
    }
    vwCateNote.layer.borderWidth = 0.5;
    vwCateNote.layer.cornerRadius = 5.0;
    vwCateNote.layer.borderColor = [UIColor lightGrayColor].CGColor;
    vwCateNote.backgroundColor = [UIColor colorWithRed:213.0/255.0 green:237.0/255.0 blue:247.0/255.0 alpha:1.0];
    [self.scrollViewFourVideoCategorised addSubview:vwCateNote];
    
    UILabel *lblNote = [[UILabel alloc] init];
    lblNote.frame = CGRectMake(5, 5, 270, 45);
    lblNote.text = @"Note: You can create your ceremonies and can upload your artworks under each ceremony like ring ceremony, pre wedding, wedding, reception and etc.. ";
    lblNote.textColor = [UIColor lightGrayColor];
    lblNote.numberOfLines = 0;
    lblNote.lineBreakMode = NSLineBreakByWordWrapping;
    lblNote.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:9.0];
    [vwCateNote addSubview:lblNote];
    
    viewHeight = viewHeight + 75;
    
    UIView *vwCateCreate = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwCateCreate.frame = CGRectMake(20, viewHeight, 280, 130);
        }
        else if(result.width == 375){
            vwCateCreate.frame = CGRectMake(20, viewHeight, 335, 130);
        }
        else{
            vwCateCreate.frame = CGRectMake(20, viewHeight, 374, 130);
        }
    }
    else{
        vwCateCreate.frame = CGRectMake(20, viewHeight, 728, 130);
    }
    vwCateCreate.layer.borderWidth = 0.5;
    vwCateCreate.layer.cornerRadius = 5.0;
    vwCateCreate.layer.borderColor = [UIColor lightGrayColor].CGColor;
    vwCateCreate.backgroundColor = [UIColor whiteColor];
    [self.scrollViewFourVideoCategorised addSubview:vwCateCreate];
    
    UIButton *btnCreate = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            btnCreate.frame = CGRectMake(210, 10, 60, 30);
        }
        else if(result.width == 375){
            btnCreate.frame = CGRectMake(265, 10, 60, 30);
        }
        else{
            btnCreate.frame = CGRectMake(304, 10, 60, 30);
        }
    }
    else{
        btnCreate.frame = CGRectMake(658, 10, 60, 30);
    }
    btnCreate.layer.borderWidth = 0.5;
    btnCreate.layer.cornerRadius = 5.0;
    btnCreate.layer.borderColor = [UIColor clearColor].CGColor;
    [btnCreate setTitle:@"CREATE" forState:UIControlStateNormal];
    [btnCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCreate.titleLabel.font = [UIFont fontWithName:@"Bebas" size:12.0];
    btnCreate.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    [btnCreate addTarget:self action:@selector(clickCreateVideoAlbum:) forControlEvents:UIControlEventTouchUpInside];
    [vwCateCreate addSubview:btnCreate];
    
    self.txtCreateVideoAlbumName = [[JJMaterialTextfield alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.txtCreateVideoAlbumName.frame = CGRectMake(5, 5, 200, 30);
        }
        else if(result.width == 375){
            self.txtCreateVideoAlbumName.frame = CGRectMake(5, 5, 255, 30);
        }
        else{
            self.txtCreateVideoAlbumName.frame = CGRectMake(5, 5, 294, 30);
        }
    }
    else{
        self.txtCreateVideoAlbumName.frame = CGRectMake(5, 5, 648, 30);
    }
    self.txtCreateVideoAlbumName.enableMaterialPlaceHolder = NO;
    self.txtCreateVideoAlbumName.lineColor = [UIColor clearColor];
    self.txtCreateVideoAlbumName.borderStyle = UITextBorderStyleNone;
    self.txtCreateVideoAlbumName.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
    [self.txtCreateVideoAlbumName setPlaceholder:@"Video Caption"];
    self.txtCreateVideoAlbumName.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtCreateVideoAlbumName.keyboardType = UIKeyboardTypeDefault;
    self.txtCreateVideoAlbumName.delegate = self;
    [vwCateCreate addSubview:self.txtCreateVideoAlbumName];
    
    UILabel *lblLine1 = [[UILabel alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            lblLine1.frame = CGRectMake(5, 40, 200, 0.5);
        }
        else if(result.width == 375){
            lblLine1.frame = CGRectMake(5, 40, 255, 0.5);
        }
        else{
            lblLine1.frame = CGRectMake(5, 40, 294, 0.5);
        }
    }
    else{
        lblLine1.frame = CGRectMake(5, 40, 648, 0.5);
    }
    lblLine1.backgroundColor = [UIColor lightGrayColor];
    [vwCateCreate addSubview:lblLine1];
    
    
    UIButton *btnCreateGDrive = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            btnCreateGDrive.frame = CGRectMake(240, 50, 25, 25);
        }
        else if(result.width == 375){
            btnCreateGDrive.frame = CGRectMake(295, 50, 25, 25);
        }
        else{
            btnCreateGDrive.frame = CGRectMake(334, 50, 25, 25);
        }
    }
    else{
        btnCreateGDrive.frame = CGRectMake(688, 50, 25, 25);
    }
    [btnCreateGDrive setBackgroundImage:[UIImage imageNamed:@"g_drive.png"] forState:UIControlStateNormal];
    [btnCreateGDrive addTarget:self action:@selector(clickCreateGDrive:) forControlEvents:UIControlEventTouchUpInside];
    [vwCateCreate addSubview:btnCreateGDrive];
    
    self.txtCreateVideoGDrive = [[JJMaterialTextfield alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.txtCreateVideoGDrive.frame = CGRectMake(5, 48, 230, 30);
        }
        else if(result.width == 375){
            self.txtCreateVideoGDrive.frame = CGRectMake(5, 48, 285, 30);
        }
        else{
            self.txtCreateVideoGDrive.frame = CGRectMake(5, 48, 324, 30);
        }
    }
    else{
        self.txtCreateVideoGDrive.frame = CGRectMake(5, 48, 678, 30);
    }
    self.txtCreateVideoGDrive.enableMaterialPlaceHolder = NO;
    self.txtCreateVideoGDrive.lineColor = [UIColor clearColor];
    self.txtCreateVideoGDrive.borderStyle = UITextBorderStyleNone;
    self.txtCreateVideoGDrive.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
    [self.txtCreateVideoGDrive setPlaceholder:@"Share link via G+ drive"];
    self.txtCreateVideoGDrive.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtCreateVideoGDrive.keyboardType = UIKeyboardTypeDefault;
    self.txtCreateVideoGDrive.delegate = self;
    [vwCateCreate addSubview:self.txtCreateVideoGDrive];
    
    UILabel *lblLine2 = [[UILabel alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            lblLine2.frame = CGRectMake(5, 80, 230, 0.5);
        }
        else if(result.width == 375){
            lblLine2.frame = CGRectMake(5, 80, 285, 0.5);
        }
        else{
            lblLine2.frame = CGRectMake(5, 80, 324, 0.5);
        }
    }
    else{
        lblLine2.frame = CGRectMake(5, 80, 678, 0.5);
    }
    lblLine2.backgroundColor = [UIColor lightGrayColor];
    [vwCateCreate addSubview:lblLine2];
    
    
    UIButton *btnCreateDropbox = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            btnCreateDropbox.frame = CGRectMake(240, 90, 25, 25);
        }
        else if(result.width == 375){
            btnCreateDropbox.frame = CGRectMake(295, 90, 25, 25);
        }
        else{
            btnCreateDropbox.frame = CGRectMake(334, 90, 25, 25);
        }
    }
    else{
        btnCreateDropbox.frame = CGRectMake(688, 90, 25, 25);
    }
    [btnCreateDropbox setBackgroundImage:[UIImage imageNamed:@"btndropbox.png"] forState:UIControlStateNormal];
    [btnCreateDropbox addTarget:self action:@selector(clickCreateDropBox:) forControlEvents:UIControlEventTouchUpInside];
    [vwCateCreate addSubview:btnCreateDropbox];
    
    self.txtCreateVideoDropbox = [[JJMaterialTextfield alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.txtCreateVideoDropbox.frame = CGRectMake(5, 88, 230, 30);
        }
        else if(result.width == 375){
            self.txtCreateVideoDropbox.frame = CGRectMake(5, 88, 285, 30);
        }
        else{
            self.txtCreateVideoDropbox.frame = CGRectMake(5, 88, 324, 30);
        }
    }
    else{
        self.txtCreateVideoDropbox.frame = CGRectMake(5, 88, 678, 30);
    }
    self.txtCreateVideoDropbox.enableMaterialPlaceHolder = NO;
    self.txtCreateVideoDropbox.lineColor = [UIColor clearColor];
    self.txtCreateVideoDropbox.borderStyle = UITextBorderStyleNone;
    self.txtCreateVideoDropbox.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
    [self.txtCreateVideoDropbox setPlaceholder:@"Share link via Dropbox"];
    self.txtCreateVideoDropbox.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtCreateVideoDropbox.keyboardType = UIKeyboardTypeDefault;
    self.txtCreateVideoDropbox.delegate = self;
    [vwCateCreate addSubview:self.txtCreateVideoDropbox];
    
    UILabel *lblLine3 = [[UILabel alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            lblLine3.frame = CGRectMake(5, 120, 230, 0.5);
        }
        else if(result.width == 375){
            lblLine3.frame = CGRectMake(5, 120, 285, 0.5);
        }
        else{
            lblLine3.frame = CGRectMake(5, 120, 324, 0.5);
        }
    }
    else{
        lblLine3.frame = CGRectMake(5, 120, 678, 0.5);
    }
    lblLine3.backgroundColor = [UIColor lightGrayColor];
    [vwCateCreate addSubview:lblLine3];
    
    viewHeight = viewHeight + 150;
    
    NSLog(@"%@",self.arrVideoCateDetails);
    
    if([self.arrVideoCateDetails count]>0){
        for(int x=0; x<[self.arrVideoCateDetails count]; x++){
            UIView *vwCateUpdate = [[UIView alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    vwCateUpdate.frame = CGRectMake(20, viewHeight, 280, 130);
                }
                else if(result.width == 375){
                    vwCateUpdate.frame = CGRectMake(20, viewHeight, 335, 130);
                }
                else{
                    vwCateUpdate.frame = CGRectMake(20, viewHeight, 374, 130);
                }
            }
            else{
                vwCateUpdate.frame = CGRectMake(20, viewHeight, 728, 130);
            }
            vwCateUpdate.layer.borderWidth = 0.5;
            vwCateUpdate.layer.cornerRadius = 5.0;
            vwCateUpdate.layer.borderColor = [UIColor lightGrayColor].CGColor;
            vwCateUpdate.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
            [self.scrollViewFourVideoCategorised addSubview:vwCateUpdate];
            
            UIButton *btnUpdate = [UIButton buttonWithType:UIButtonTypeCustom];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    btnUpdate.frame = CGRectMake(175, 10, 60, 30);
                }
                else if(result.width == 375){
                    btnUpdate.frame = CGRectMake(230, 10, 60, 30);
                }
                else{
                    btnUpdate.frame = CGRectMake(269, 10, 60, 30);
                }
            }
            else{
                btnUpdate.frame = CGRectMake(623, 10, 60, 30);
            }
            btnUpdate.layer.borderWidth = 0.5;
            btnUpdate.layer.cornerRadius = 5.0;
            btnUpdate.layer.borderColor = [UIColor clearColor].CGColor;
            [btnUpdate setTitle:@"UPDATE" forState:UIControlStateNormal];
            [btnUpdate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btnUpdate.titleLabel.font = [UIFont fontWithName:@"Bebas" size:12.0];
            btnUpdate.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
            [btnUpdate addTarget:self action:@selector(clickCreateVideoAlbumUpdate:) forControlEvents:UIControlEventTouchUpInside];
            btnUpdate.tag = x;
            [vwCateUpdate addSubview:btnUpdate];
            
            UIButton *btnDeleteAlbum = [UIButton buttonWithType:UIButtonTypeCustom];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    btnDeleteAlbum.frame = CGRectMake(245, 10, 25, 25);
                }
                else if(result.width == 375){
                    btnDeleteAlbum.frame = CGRectMake(335, 10, 25, 25);
                }
                else{
                    btnDeleteAlbum.frame = CGRectMake(374, 10, 25, 25);
                }
            }
            else{
                btnDeleteAlbum.frame = CGRectMake(728, 10, 25, 25);
            }
            [btnDeleteAlbum setBackgroundImage:[UIImage imageNamed:@"btndelete.png"] forState:UIControlStateNormal];
            [btnDeleteAlbum addTarget:self action:@selector(clickCreateVideoAlbumDelete:) forControlEvents:UIControlEventTouchUpInside];
            btnDeleteAlbum.tag = x;
            [vwCateUpdate addSubview:btnDeleteAlbum];
            
            int intAlbumName = arc4random_uniform(9999);
            
            JJMaterialTextfield *txtUpdateAlbumName = [[JJMaterialTextfield alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    txtUpdateAlbumName.frame = CGRectMake(5, 5, 165, 30);
                }
                else if(result.width == 375){
                    txtUpdateAlbumName.frame = CGRectMake(5, 5, 220, 30);
                }
                else{
                    txtUpdateAlbumName.frame = CGRectMake(5, 5, 259, 30);
                }
            }
            else{
                txtUpdateAlbumName.frame = CGRectMake(5, 5, 613, 30);
            }
            txtUpdateAlbumName.enableMaterialPlaceHolder = NO;
            txtUpdateAlbumName.lineColor = [UIColor clearColor];
            txtUpdateAlbumName.borderStyle = UITextBorderStyleNone;
            txtUpdateAlbumName.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
            [txtUpdateAlbumName setPlaceholder:@"Video Caption"];
            txtUpdateAlbumName.text = [[self.arrVideoCateDetails objectAtIndex:x] objectForKey:@"video_title"];
            txtUpdateAlbumName.autocorrectionType = UITextAutocorrectionTypeNo;
            txtUpdateAlbumName.keyboardType = UIKeyboardTypeDefault;
            txtUpdateAlbumName.delegate = self;
            txtUpdateAlbumName.tag = intAlbumName;
            [vwCateUpdate addSubview:txtUpdateAlbumName];
            
            [self.arrVideoCateUpDateCatName addObject:[NSString stringWithFormat:@"%d",intAlbumName]];
            
            NSLog(@"%@",self.arrVideoCateUpDateCatName);
            
            UILabel *lblLine1 = [[UILabel alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    lblLine1.frame = CGRectMake(5, 40, 165, 0.5);
                }
                else if(result.width == 375){
                    lblLine1.frame = CGRectMake(5, 40, 220, 0.5);
                }
                else{
                    lblLine1.frame = CGRectMake(5, 40, 259, 0.5);
                }
            }
            else{
                lblLine1.frame = CGRectMake(5, 40, 613, 0.5);
            }
            lblLine1.backgroundColor = [UIColor lightGrayColor];
            [vwCateUpdate addSubview:lblLine1];
            
            
            UIButton *btnCreateGDrive = [UIButton buttonWithType:UIButtonTypeCustom];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    btnCreateGDrive.frame = CGRectMake(240, 50, 25, 25);
                }
                else if(result.width == 375){
                    btnCreateGDrive.frame = CGRectMake(295, 50, 25, 25);
                }
                else{
                    btnCreateGDrive.frame = CGRectMake(334, 50, 25, 25);
                }
            }
            else{
                btnCreateGDrive.frame = CGRectMake(688, 50, 25, 25);
            }
            [btnCreateGDrive setBackgroundImage:[UIImage imageNamed:@"g_drive.png"] forState:UIControlStateNormal];
            [btnCreateGDrive addTarget:self action:@selector(clickCreateGDrive:) forControlEvents:UIControlEventTouchUpInside];
            [vwCateUpdate addSubview:btnCreateGDrive];
            
            
            int intGDriveLink = arc4random_uniform(9999);
            
            JJMaterialTextfield *txtUpdateGDriveLink = [[JJMaterialTextfield alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    txtUpdateGDriveLink.frame = CGRectMake(5, 48, 230, 30);
                }
                else if(result.width == 375){
                    txtUpdateGDriveLink.frame = CGRectMake(5, 48, 285, 30);
                }
                else{
                    txtUpdateGDriveLink.frame = CGRectMake(5, 48, 324, 30);
                }
            }
            else{
                txtUpdateGDriveLink.frame = CGRectMake(5, 48, 678, 30);
            }
            txtUpdateGDriveLink.enableMaterialPlaceHolder = NO;
            txtUpdateGDriveLink.lineColor = [UIColor clearColor];
            txtUpdateGDriveLink.borderStyle = UITextBorderStyleNone;
            txtUpdateGDriveLink.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
            [txtUpdateGDriveLink setPlaceholder:@"Share link via G+ drive"];
            txtUpdateGDriveLink.text = [[self.arrVideoCateDetails objectAtIndex:x] objectForKey:@"google_drive_link"];
            txtUpdateGDriveLink.autocorrectionType = UITextAutocorrectionTypeNo;
            txtUpdateGDriveLink.keyboardType = UIKeyboardTypeDefault;
            txtUpdateGDriveLink.delegate = self;
            txtUpdateGDriveLink.tag = intGDriveLink;
            [vwCateUpdate addSubview:txtUpdateGDriveLink];
            
            [self.arrVideoCateUpDateGDriveLink addObject:[NSString stringWithFormat:@"%d",intGDriveLink]];
            
            NSLog(@"%@",self.arrVideoCateUpDateGDriveLink);
            
            UILabel *lblLine2 = [[UILabel alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    lblLine2.frame = CGRectMake(5, 80, 230, 0.5);
                }
                else if(result.width == 375){
                    lblLine2.frame = CGRectMake(5, 80, 285, 0.5);
                }
                else{
                    lblLine2.frame = CGRectMake(5, 80, 324, 0.5);
                }
            }
            else{
                lblLine2.frame = CGRectMake(5, 80, 678, 0.5);
            }
            lblLine2.backgroundColor = [UIColor lightGrayColor];
            [vwCateUpdate addSubview:lblLine2];
            
            
            UIButton *btnCreateDropbox = [UIButton buttonWithType:UIButtonTypeCustom];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    btnCreateDropbox.frame = CGRectMake(240, 90, 25, 25);
                }
                else if(result.width == 375){
                    btnCreateDropbox.frame = CGRectMake(295, 90, 25, 25);
                }
                else{
                    btnCreateDropbox.frame = CGRectMake(334, 90, 25, 25);
                }
            }
            else{
                btnCreateDropbox.frame = CGRectMake(688, 90, 25, 25);
            }
            [btnCreateDropbox setBackgroundImage:[UIImage imageNamed:@"btndropbox.png"] forState:UIControlStateNormal];
            [btnCreateDropbox addTarget:self action:@selector(clickCreateDropBox:) forControlEvents:UIControlEventTouchUpInside];
            [vwCateUpdate addSubview:btnCreateDropbox];
            
            int intDropboxLink = arc4random_uniform(9999);
            
            JJMaterialTextfield *txtUpdateDropboxLink = [[JJMaterialTextfield alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    txtUpdateDropboxLink.frame = CGRectMake(5, 88, 230, 30);
                }
                else if(result.width == 375){
                    txtUpdateDropboxLink.frame = CGRectMake(5, 88, 285, 30);
                }
                else{
                    txtUpdateDropboxLink.frame = CGRectMake(5, 88, 324, 30);
                }
            }
            else{
                txtUpdateDropboxLink.frame = CGRectMake(5, 88, 678, 30);
            }
            txtUpdateDropboxLink.enableMaterialPlaceHolder = NO;
            txtUpdateDropboxLink.lineColor = [UIColor clearColor];
            txtUpdateDropboxLink.borderStyle = UITextBorderStyleNone;
            txtUpdateDropboxLink.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
            [txtUpdateDropboxLink setPlaceholder:@"Share link via Dropbox"];
            txtUpdateDropboxLink.text = [[self.arrVideoCateDetails objectAtIndex:x] objectForKey:@"dropbox_link"];
            txtUpdateDropboxLink.autocorrectionType = UITextAutocorrectionTypeNo;
            txtUpdateDropboxLink.keyboardType = UIKeyboardTypeDefault;
            txtUpdateDropboxLink.delegate = self;
            txtUpdateDropboxLink.tag = intDropboxLink;
            [vwCateUpdate addSubview:txtUpdateDropboxLink];
            
            [self.arrVideoCateUpDateDropboxLink addObject:[NSString stringWithFormat:@"%d",intDropboxLink]];
            
            NSLog(@"%@",self.arrVideoCateUpDateDropboxLink);
            
            UILabel *lblLine3 = [[UILabel alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    lblLine3.frame = CGRectMake(5, 120, 230, 0.5);
                }
                else if(result.width == 375){
                    lblLine3.frame = CGRectMake(5, 120, 285, 0.5);
                }
                else{
                    lblLine3.frame = CGRectMake(5, 120, 324, 0.5);
                }
            }
            else{
                lblLine3.frame = CGRectMake(5, 120, 678, 0.5);
            }
            lblLine3.backgroundColor = [UIColor lightGrayColor];
            [vwCateUpdate addSubview:lblLine3];
            
            viewHeight = viewHeight + 150;
        }
    }
    
    UIView *vwPreViousNextBtn = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwPreViousNextBtn.frame = CGRectMake(25, viewHeight, 270, 35);
        }
        else if(result.width == 375){
            vwPreViousNextBtn.frame = CGRectMake(52.5, viewHeight, 270, 35);
        }
        else{
            vwPreViousNextBtn.frame = CGRectMake(72, viewHeight, 270, 35);
        }
    }
    else{
        vwPreViousNextBtn.frame = CGRectMake(249, viewHeight, 270, 35);
    }
    vwPreViousNextBtn.backgroundColor = [UIColor clearColor];
    [self.scrollViewFourVideoCategorised addSubview:vwPreViousNextBtn];
    
    UIButton *btnCatPreviousTwo = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCatPreviousTwo.frame = CGRectMake(0, 0, 130, 35);
    btnCatPreviousTwo.layer.borderWidth = 0.5;
    btnCatPreviousTwo.layer.cornerRadius = 5.0;
    btnCatPreviousTwo.layer.borderColor = [UIColor clearColor].CGColor;
    [btnCatPreviousTwo setTitle:@"PREVIOUS" forState:UIControlStateNormal];
    [btnCatPreviousTwo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCatPreviousTwo.titleLabel.font = [UIFont fontWithName:@"Bebas" size:14.0];
    btnCatPreviousTwo.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
    [btnCatPreviousTwo addTarget:self action:@selector(clickFromPreviosCateVideo:) forControlEvents:UIControlEventTouchUpInside];
    [vwPreViousNextBtn addSubview:btnCatPreviousTwo];
    
    UIButton *btnCatNextTwo = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCatNextTwo.frame = CGRectMake(140, 0, 130, 35);
    btnCatNextTwo.layer.borderWidth = 0.5;
    btnCatNextTwo.layer.cornerRadius = 5.0;
    btnCatNextTwo.layer.borderColor = [UIColor clearColor].CGColor;
    [btnCatNextTwo setTitle:@"NEXT" forState:UIControlStateNormal];
    [btnCatNextTwo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnCatNextTwo.titleLabel.font = [UIFont fontWithName:@"Bebas" size:14.0];
    btnCatNextTwo.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:188.0/255.0 blue:0.0/255.0 alpha:1.0];
    [btnCatNextTwo addTarget:self action:@selector(clickToNextCateVideo:) forControlEvents:UIControlEventTouchUpInside];
    [vwPreViousNextBtn addSubview:btnCatNextTwo];
    
    viewHeight = viewHeight + 50;
    
    UIView *vwFive = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwFive.frame = CGRectMake(0, viewHeight, 320, 40);
        }
        else if(result.width == 375){
            vwFive.frame = CGRectMake(0, viewHeight, 375, 40);
        }
        else{
            vwFive.frame = CGRectMake(0, viewHeight, 414, 40);
        }
    }
    else{
        vwFive.frame = CGRectMake(0, viewHeight, 768, 40);
    }
    vwFive.backgroundColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0];
    [self.scrollViewFourVideoCategorised addSubview:vwFive];
    
    UIImageView *imgVwFive = [[UIImageView alloc] init];
    imgVwFive.frame = CGRectMake(10, 8, 25, 25);
    imgVwFive.image = [UIImage imageNamed:@"btngray5.png"];
    [vwFive addSubview:imgVwFive];
    
    UILabel *lblFive = [[UILabel alloc] init];
    lblFive.frame = CGRectMake(45, 11, 200, 20);
    lblFive.text = @"ORDER  CONFIRMATION";
    lblFive.textColor = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0];
    lblFive.font = [UIFont fontWithName:@"Bebas" size:17.0];
    [vwFive addSubview:lblFive];
    
    viewHeight = viewHeight + 41;
    
    [self.scrollViewFourVideoCategorised setContentSize:CGSizeMake(0, viewHeight)];
}

-(void)createScrollViewThirdCategorised{
    
    for (UIView *v in self.scrollViewThirdCategorised.subviews) {
        if ([v isKindOfClass:[UIView class]]) {
            [v removeFromSuperview];
        }
    }
    
    [self.arrImageCateUpDateCatName removeAllObjects];
    [self.arrImageCateUpDateGDriveLink removeAllObjects];
    [self.arrImageCateUpDateDropboxLink removeAllObjects];
    
    self.scrollViewThirdCategorised = [[UIScrollView alloc] init];
    self.scrollViewThirdCategorised.backgroundColor = [UIColor whiteColor];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.scrollViewThirdCategorised.frame = CGRectMake(0, 0, 320, 458);
        }
        else if(result.width == 375){
            self.scrollViewThirdCategorised.frame = CGRectMake(0, 0, 375, 622);
        }
        else{
            self.scrollViewThirdCategorised.frame = CGRectMake(0, 0, 414, 454);
        }
    }
    else{
        self.scrollViewThirdCategorised.frame = CGRectMake(0, 0, 768, 865);
    }
    
    [self.viewMain addSubview:self.scrollViewThirdCategorised];
    
    int viewHeight = 0;
    
    UIView *vwOne = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwOne.frame = CGRectMake(0, viewHeight, 320, 40);
        }
        else if(result.width == 375){
            vwOne.frame = CGRectMake(0, viewHeight, 375, 40);
        }
        else{
            vwOne.frame = CGRectMake(0, viewHeight, 414, 40);
        }
    }
    else{
        vwOne.frame = CGRectMake(0, viewHeight, 768, 40);
    }
    vwOne.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    [self.scrollViewThirdCategorised addSubview:vwOne];
    
    UIImageView *imgVwOne = [[UIImageView alloc] init];
    imgVwOne.frame = CGRectMake(10, 8, 25, 25);
    imgVwOne.image = [UIImage imageNamed:@"btnright.png"];
    [vwOne addSubview:imgVwOne];
    
    UILabel *lblOne = [[UILabel alloc] init];
    lblOne.frame = CGRectMake(45, 11, 140, 20);
    lblOne.text = @"CUSTOMER  DETAILS";
    lblOne.textColor = [UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0];
    lblOne.font = [UIFont fontWithName:@"Bebas" size:17.0];
    [vwOne addSubview:lblOne];
    
    viewHeight = viewHeight + 41;
    
    UIView *vwTwo = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwTwo.frame = CGRectMake(0, viewHeight, 320, 40);
        }
        else if(result.width == 375){
            vwTwo.frame = CGRectMake(0, viewHeight, 375, 40);
        }
        else{
            vwTwo.frame = CGRectMake(0, viewHeight, 414, 40);
        }
    }
    else{
        vwTwo.frame = CGRectMake(0, viewHeight, 768, 40);
    }
    vwTwo.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    [self.scrollViewThirdCategorised addSubview:vwTwo];
    
    UIImageView *imgVwTwo = [[UIImageView alloc] init];
    imgVwTwo.frame = CGRectMake(10, 8, 25, 25);
    imgVwTwo.image = [UIImage imageNamed:@"btnright.png"];
    [vwTwo addSubview:imgVwTwo];
    
    UILabel *lblTwo = [[UILabel alloc] init];
    lblTwo.frame = CGRectMake(45, 11, 200, 20);
    lblTwo.text = @"CREATE  YOUR  ALBUM";
    lblTwo.textColor = [UIColor colorWithRed:171.0/255.0 green:171.0/255.0 blue:171.0/255.0 alpha:1.0];
    lblTwo.font = [UIFont fontWithName:@"Bebas" size:17.0];
    [vwTwo addSubview:lblTwo];
    
    viewHeight = viewHeight + 41;
    
    UIView *vwThree = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwThree.frame = CGRectMake(0, viewHeight, 320, 40);
        }
        else if(result.width == 375){
            vwThree.frame = CGRectMake(0, viewHeight, 375, 40);
        }
        else{
            vwThree.frame = CGRectMake(0, viewHeight, 414, 40);
        }
    }
    else{
        vwThree.frame = CGRectMake(0, viewHeight, 768, 40);
    }
    vwThree.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    [self.scrollViewThirdCategorised addSubview:vwThree];
    
    UIImageView *imgVwThree = [[UIImageView alloc] init];
    imgVwThree.frame = CGRectMake(10, 8, 25, 25);
    imgVwThree.image = [UIImage imageNamed:@"btnred3.png"];
    [vwThree addSubview:imgVwThree];
    
    UILabel *lblThree = [[UILabel alloc] init];
    lblThree.frame = CGRectMake(45, 11, 140, 20);
    lblThree.text = @"UPLOAD  IMAGES";
    lblThree.textColor = [UIColor whiteColor];
    lblThree.font = [UIFont fontWithName:@"Bebas" size:17.0];
    [vwThree addSubview:lblThree];
    
    viewHeight = viewHeight + 50;
    
    UIButton *btnBackCateImage = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            btnBackCateImage.frame = CGRectMake(240, viewHeight, 60, 30);
        }
        else if(result.width == 375){
            btnBackCateImage.frame = CGRectMake(265, viewHeight, 60, 30);
        }
        else{
            btnBackCateImage.frame = CGRectMake(304, viewHeight, 60, 30);
        }
    }
    else{
        btnBackCateImage.frame = CGRectMake(658, viewHeight, 60, 30);
    }
    btnBackCateImage.layer.borderWidth = 0.5;
    btnBackCateImage.layer.cornerRadius = 5.0;
    btnBackCateImage.layer.borderColor = [UIColor clearColor].CGColor;
    [btnBackCateImage setTitle:@"BACK" forState:UIControlStateNormal];
    [btnBackCateImage setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnBackCateImage.titleLabel.font = [UIFont fontWithName:@"Bebas" size:12.0];
    btnBackCateImage.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    [btnBackCateImage addTarget:self action:@selector(clickBackCateImageType:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollViewThirdCategorised addSubview:btnBackCateImage];
    
    viewHeight = viewHeight + 35;
    
    UIView *vwCateNote = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwCateNote.frame = CGRectMake(20, viewHeight, 280, 55);
        }
        else if(result.width == 375){
            vwCateNote.frame = CGRectMake(20, viewHeight, 335, 55);
        }
        else{
            vwCateNote.frame = CGRectMake(20, viewHeight, 374, 55);
        }
    }
    else{
        vwCateNote.frame = CGRectMake(20, viewHeight, 728, 55);
    }
    vwCateNote.layer.borderWidth = 0.5;
    vwCateNote.layer.cornerRadius = 5.0;
    vwCateNote.layer.borderColor = [UIColor lightGrayColor].CGColor;
    vwCateNote.backgroundColor = [UIColor colorWithRed:213.0/255.0 green:237.0/255.0 blue:247.0/255.0 alpha:1.0];
    [self.scrollViewThirdCategorised addSubview:vwCateNote];
    
    UILabel *lblNote = [[UILabel alloc] init];
    lblNote.frame = CGRectMake(5, 5, 270, 45);
    lblNote.text = @"Note: You can create your ceremonies and can upload your artworks under each ceremony like ring ceremony, pre wedding, wedding, reception and etc..";
    lblNote.textColor = [UIColor lightGrayColor];
    lblNote.numberOfLines = 0;
    lblNote.lineBreakMode = NSLineBreakByWordWrapping;
    lblNote.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:9.0];
    [vwCateNote addSubview:lblNote];
    
    viewHeight = viewHeight + 75;
    
    UIView *vwCateCreate = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwCateCreate.frame = CGRectMake(20, viewHeight, 280, 130);
        }
        else if(result.width == 375){
            vwCateCreate.frame = CGRectMake(20, viewHeight, 335, 130);
        }
        else{
            vwCateCreate.frame = CGRectMake(20, viewHeight, 374, 130);
        }
    }
    else{
        vwCateCreate.frame = CGRectMake(20, viewHeight, 728, 130);
    }
    vwCateCreate.layer.borderWidth = 0.5;
    vwCateCreate.layer.cornerRadius = 5.0;
    vwCateCreate.layer.borderColor = [UIColor lightGrayColor].CGColor;
    vwCateCreate.backgroundColor = [UIColor whiteColor];
    [self.scrollViewThirdCategorised addSubview:vwCateCreate];
    
    UIButton *btnCreate = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            btnCreate.frame = CGRectMake(210, 10, 60, 30);
        }
        else if(result.width == 375){
            btnCreate.frame = CGRectMake(265, 10, 60, 30);
        }
        else{
            btnCreate.frame = CGRectMake(304, 10, 60, 30);
        }
    }
    else{
        btnCreate.frame = CGRectMake(658, 10, 60, 30);
    }
    btnCreate.layer.borderWidth = 0.5;
    btnCreate.layer.cornerRadius = 5.0;
    btnCreate.layer.borderColor = [UIColor clearColor].CGColor;
    [btnCreate setTitle:@"CREATE" forState:UIControlStateNormal];
    [btnCreate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCreate.titleLabel.font = [UIFont fontWithName:@"Bebas" size:12.0];
    btnCreate.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
    [btnCreate addTarget:self action:@selector(clickCreateImageAlbum:) forControlEvents:UIControlEventTouchUpInside];
    [vwCateCreate addSubview:btnCreate];
    
    self.txtCreateAlbumName = [[JJMaterialTextfield alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.txtCreateAlbumName.frame = CGRectMake(5, 5, 200, 30);
        }
        else if(result.width == 375){
            self.txtCreateAlbumName.frame = CGRectMake(5, 5, 255, 30);
        }
        else{
            self.txtCreateAlbumName.frame = CGRectMake(5, 5, 294, 30);
        }
    }
    else{
        self.txtCreateAlbumName.frame = CGRectMake(5, 5, 648, 30);
    }
    self.txtCreateAlbumName.enableMaterialPlaceHolder = NO;
    self.txtCreateAlbumName.lineColor = [UIColor clearColor];
    self.txtCreateAlbumName.borderStyle = UITextBorderStyleNone;
    self.txtCreateAlbumName.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
    [self.txtCreateAlbumName setPlaceholder:@"Ceremony Name"];
    self.txtCreateAlbumName.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtCreateAlbumName.keyboardType = UIKeyboardTypeDefault;
    //self.txtCreateAlbumName.returnKeyType = UIReturnKeyDone;
    //self.txtCreateAlbumName.clearButtonMode = UITextFieldViewModeWhileEditing;
    //self.txtCreateAlbumName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.txtCreateAlbumName.delegate = self;
    [vwCateCreate addSubview:self.txtCreateAlbumName];
    
    UILabel *lblLine1 = [[UILabel alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            lblLine1.frame = CGRectMake(5, 40, 200, 0.5);
        }
        else if(result.width == 375){
            lblLine1.frame = CGRectMake(5, 40, 255, 0.5);
        }
        else{
            lblLine1.frame = CGRectMake(5, 40, 294, 0.5);
        }
    }
    else{
        lblLine1.frame = CGRectMake(5, 40, 648, 0.5);
    }
    lblLine1.backgroundColor = [UIColor lightGrayColor];
    [vwCateCreate addSubview:lblLine1];
    
    
    UIButton *btnCreateGDrive = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            btnCreateGDrive.frame = CGRectMake(240, 50, 25, 25);
        }
        else if(result.width == 375){
            btnCreateGDrive.frame = CGRectMake(295, 50, 25, 25);
        }
        else{
            btnCreateGDrive.frame = CGRectMake(334, 50, 25, 25);
        }
    }
    else{
        btnCreateGDrive.frame = CGRectMake(688, 50, 25, 25);
    }
    [btnCreateGDrive setBackgroundImage:[UIImage imageNamed:@"g_drive.png"] forState:UIControlStateNormal];
    [btnCreateGDrive addTarget:self action:@selector(clickCreateGDrive:) forControlEvents:UIControlEventTouchUpInside];
    [vwCateCreate addSubview:btnCreateGDrive];
    
    self.txtCreateGDrive = [[JJMaterialTextfield alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.txtCreateGDrive.frame = CGRectMake(5, 48, 230, 30);
        }
        else if(result.width == 375){
            self.txtCreateGDrive.frame = CGRectMake(5, 48, 285, 30);
        }
        else{
            self.txtCreateGDrive.frame = CGRectMake(5, 48, 324, 30);
        }
    }
    else{
        self.txtCreateGDrive.frame = CGRectMake(5, 48, 678, 30);
    }
    self.txtCreateGDrive.enableMaterialPlaceHolder = NO;
    self.txtCreateGDrive.lineColor = [UIColor clearColor];
    self.txtCreateGDrive.borderStyle = UITextBorderStyleNone;
    self.txtCreateGDrive.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
    [self.txtCreateGDrive setPlaceholder:@"Share link via G+ drive"];
    self.txtCreateGDrive.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtCreateGDrive.keyboardType = UIKeyboardTypeDefault;
    //self.txtCreateAlbumName.returnKeyType = UIReturnKeyDone;
    //self.txtCreateAlbumName.clearButtonMode = UITextFieldViewModeWhileEditing;
    //self.txtCreateAlbumName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.txtCreateGDrive.delegate = self;
    [vwCateCreate addSubview:self.txtCreateGDrive];
    
    UILabel *lblLine2 = [[UILabel alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            lblLine2.frame = CGRectMake(5, 80, 230, 0.5);
        }
        else if(result.width == 375){
            lblLine2.frame = CGRectMake(5, 80, 285, 0.5);
        }
        else{
            lblLine2.frame = CGRectMake(5, 80, 324, 0.5);
        }
    }
    else{
        lblLine2.frame = CGRectMake(5, 80, 678, 0.5);
    }
    lblLine2.backgroundColor = [UIColor lightGrayColor];
    [vwCateCreate addSubview:lblLine2];
    
    
    UIButton *btnCreateDropbox = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            btnCreateDropbox.frame = CGRectMake(240, 90, 25, 25);
        }
        else if(result.width == 375){
            btnCreateDropbox.frame = CGRectMake(295, 90, 25, 25);
        }
        else{
            btnCreateDropbox.frame = CGRectMake(334, 90, 25, 25);
        }
    }
    else{
        btnCreateDropbox.frame = CGRectMake(688, 90, 25, 25);
    }
    [btnCreateDropbox setBackgroundImage:[UIImage imageNamed:@"btndropbox.png"] forState:UIControlStateNormal];
    [btnCreateDropbox addTarget:self action:@selector(clickCreateDropBox:) forControlEvents:UIControlEventTouchUpInside];
    [vwCateCreate addSubview:btnCreateDropbox];
    
    self.txtCreateDropbox = [[JJMaterialTextfield alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.txtCreateDropbox.frame = CGRectMake(5, 88, 230, 30);
        }
        else if(result.width == 375){
            self.txtCreateDropbox.frame = CGRectMake(5, 88, 285, 30);
        }
        else{
            self.txtCreateDropbox.frame = CGRectMake(5, 88, 324, 30);
        }
    }
    else{
        self.txtCreateDropbox.frame = CGRectMake(5, 88, 678, 30);
    }
    self.txtCreateDropbox.enableMaterialPlaceHolder = NO;
    self.txtCreateDropbox.lineColor = [UIColor clearColor];
    self.txtCreateDropbox.borderStyle = UITextBorderStyleNone;
    self.txtCreateDropbox.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
    [self.txtCreateDropbox setPlaceholder:@"Share link via Dropbox"];
    self.txtCreateDropbox.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtCreateDropbox.keyboardType = UIKeyboardTypeDefault;
    //self.txtCreateAlbumName.returnKeyType = UIReturnKeyDone;
    //self.txtCreateAlbumName.clearButtonMode = UITextFieldViewModeWhileEditing;
    //self.txtCreateAlbumName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.txtCreateDropbox.delegate = self;
    [vwCateCreate addSubview:self.txtCreateDropbox];
    
    UILabel *lblLine3 = [[UILabel alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            lblLine3.frame = CGRectMake(5, 120, 230, 0.5);
        }
        else if(result.width == 375){
            lblLine3.frame = CGRectMake(5, 120, 285, 0.5);
        }
        else{
            lblLine3.frame = CGRectMake(5, 120, 324, 0.5);
        }
    }
    else{
        lblLine3.frame = CGRectMake(5, 120, 678, 0.5);
    }
    lblLine3.backgroundColor = [UIColor lightGrayColor];
    [vwCateCreate addSubview:lblLine3];
    
    viewHeight = viewHeight + 150;
    
    NSLog(@"%@",self.arrImageCateDetails);
    
    if([self.arrImageCateDetails count]>0){
        for(int x=0; x<[self.arrImageCateDetails count]; x++){
            UIView *vwCateUpdate = [[UIView alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    vwCateUpdate.frame = CGRectMake(20, viewHeight, 280, 130);
                }
                else if(result.width == 375){
                    vwCateUpdate.frame = CGRectMake(20, viewHeight, 335, 130);
                }
                else{
                    vwCateUpdate.frame = CGRectMake(20, viewHeight, 374, 130);
                }
            }
            else{
                vwCateUpdate.frame = CGRectMake(20, viewHeight, 728, 130);
            }
            vwCateUpdate.layer.borderWidth = 0.5;
            vwCateUpdate.layer.cornerRadius = 5.0;
            vwCateUpdate.layer.borderColor = [UIColor lightGrayColor].CGColor;
            vwCateUpdate.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
            [self.scrollViewThirdCategorised addSubview:vwCateUpdate];
            
            UIButton *btnUpdate = [UIButton buttonWithType:UIButtonTypeCustom];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    btnUpdate.frame = CGRectMake(175, 10, 60, 30);
                }
                else if(result.width == 375){
                    btnUpdate.frame = CGRectMake(230, 10, 60, 30);
                }
                else{
                    btnUpdate.frame = CGRectMake(269, 10, 60, 30);
                }
            }
            else{
                btnUpdate.frame = CGRectMake(623, 10, 60, 30);
            }
            btnUpdate.layer.borderWidth = 0.5;
            btnUpdate.layer.cornerRadius = 5.0;
            btnUpdate.layer.borderColor = [UIColor clearColor].CGColor;
            [btnUpdate setTitle:@"UPDATE" forState:UIControlStateNormal];
            [btnUpdate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            btnUpdate.titleLabel.font = [UIFont fontWithName:@"Bebas" size:12.0];
            btnUpdate.backgroundColor = [UIColor colorWithRed:74.0/255.0 green:74.0/255.0 blue:74.0/255.0 alpha:1.0];
            [btnUpdate addTarget:self action:@selector(clickCreateImageAlbumUpdate:) forControlEvents:UIControlEventTouchUpInside];
            btnUpdate.tag = x;
            [vwCateUpdate addSubview:btnUpdate];
            
            UIButton *btnDeleteAlbum = [UIButton buttonWithType:UIButtonTypeCustom];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    btnDeleteAlbum.frame = CGRectMake(245, 10, 25, 25);
                }
                else if(result.width == 375){
                    btnDeleteAlbum.frame = CGRectMake(335, 10, 25, 25);
                }
                else{
                    btnDeleteAlbum.frame = CGRectMake(374, 10, 25, 25);
                }
            }
            else{
                btnDeleteAlbum.frame = CGRectMake(728, 10, 25, 25);
            }
            [btnDeleteAlbum setBackgroundImage:[UIImage imageNamed:@"btndelete.png"] forState:UIControlStateNormal];
            [btnDeleteAlbum addTarget:self action:@selector(clickCreateImageAlbumDelete:) forControlEvents:UIControlEventTouchUpInside];
            btnDeleteAlbum.tag = x;
            [vwCateUpdate addSubview:btnDeleteAlbum];
            
            int intAlbumName = arc4random_uniform(9999);
            
            JJMaterialTextfield *txtUpdateAlbumName = [[JJMaterialTextfield alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    txtUpdateAlbumName.frame = CGRectMake(5, 5, 165, 30);
                }
                else if(result.width == 375){
                    txtUpdateAlbumName.frame = CGRectMake(5, 5, 220, 30);
                }
                else{
                    txtUpdateAlbumName.frame = CGRectMake(5, 5, 259, 30);
                }
            }
            else{
                txtUpdateAlbumName.frame = CGRectMake(5, 5, 613, 30);
            }
            txtUpdateAlbumName.enableMaterialPlaceHolder = NO;
            txtUpdateAlbumName.lineColor = [UIColor clearColor];
            txtUpdateAlbumName.borderStyle = UITextBorderStyleNone;
            txtUpdateAlbumName.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
            [txtUpdateAlbumName setPlaceholder:@"Ceremony Name"];
            txtUpdateAlbumName.text = [[self.arrImageCateDetails objectAtIndex:x] objectForKey:@"image_category_name"];
            txtUpdateAlbumName.autocorrectionType = UITextAutocorrectionTypeNo;
            txtUpdateAlbumName.keyboardType = UIKeyboardTypeDefault;
            txtUpdateAlbumName.delegate = self;
            txtUpdateAlbumName.tag = intAlbumName;
            [vwCateUpdate addSubview:txtUpdateAlbumName];
            
            [self.arrImageCateUpDateCatName addObject:[NSString stringWithFormat:@"%d",intAlbumName]];
            
            NSLog(@"%@",self.arrImageCateUpDateCatName);
            
            UILabel *lblLine1 = [[UILabel alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    lblLine1.frame = CGRectMake(5, 40, 165, 0.5);
                }
                else if(result.width == 375){
                    lblLine1.frame = CGRectMake(5, 40, 220, 0.5);
                }
                else{
                    lblLine1.frame = CGRectMake(5, 40, 259, 0.5);
                }
            }
            else{
                lblLine1.frame = CGRectMake(5, 40, 613, 0.5);
            }
            lblLine1.backgroundColor = [UIColor lightGrayColor];
            [vwCateUpdate addSubview:lblLine1];
            
            
            UIButton *btnCreateGDrive = [UIButton buttonWithType:UIButtonTypeCustom];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    btnCreateGDrive.frame = CGRectMake(240, 50, 25, 25);
                }
                else if(result.width == 375){
                    btnCreateGDrive.frame = CGRectMake(295, 50, 25, 25);
                }
                else{
                    btnCreateGDrive.frame = CGRectMake(334, 50, 25, 25);
                }
            }
            else{
                btnCreateGDrive.frame = CGRectMake(688, 50, 25, 25);
            }
            [btnCreateGDrive setBackgroundImage:[UIImage imageNamed:@"g_drive.png"] forState:UIControlStateNormal];
            [btnCreateGDrive addTarget:self action:@selector(clickCreateGDrive:) forControlEvents:UIControlEventTouchUpInside];
            [vwCateUpdate addSubview:btnCreateGDrive];
            
            
            int intGDriveLink = arc4random_uniform(9999);
            
            JJMaterialTextfield *txtUpdateGDriveLink = [[JJMaterialTextfield alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    txtUpdateGDriveLink.frame = CGRectMake(5, 48, 230, 30);
                }
                else if(result.width == 375){
                    txtUpdateGDriveLink.frame = CGRectMake(5, 48, 285, 30);
                }
                else{
                    txtUpdateGDriveLink.frame = CGRectMake(5, 48, 324, 30);
                }
            }
            else{
                txtUpdateGDriveLink.frame = CGRectMake(5, 48, 678, 30);
            }
            txtUpdateGDriveLink.enableMaterialPlaceHolder = NO;
            txtUpdateGDriveLink.lineColor = [UIColor clearColor];
            txtUpdateGDriveLink.borderStyle = UITextBorderStyleNone;
            txtUpdateGDriveLink.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
            [txtUpdateGDriveLink setPlaceholder:@"Share link via G+ drive"];
            txtUpdateGDriveLink.text = [[self.arrImageCateDetails objectAtIndex:x] objectForKey:@"google_drive_link"];
            txtUpdateGDriveLink.autocorrectionType = UITextAutocorrectionTypeNo;
            txtUpdateGDriveLink.keyboardType = UIKeyboardTypeDefault;
            txtUpdateGDriveLink.delegate = self;
            txtUpdateGDriveLink.tag = intGDriveLink;
            [vwCateUpdate addSubview:txtUpdateGDriveLink];
            
            [self.arrImageCateUpDateGDriveLink addObject:[NSString stringWithFormat:@"%d",intGDriveLink]];
            
            NSLog(@"%@",self.arrImageCateUpDateGDriveLink);
            
            UILabel *lblLine2 = [[UILabel alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    lblLine2.frame = CGRectMake(5, 80, 230, 0.5);
                }
                else if(result.width == 375){
                    lblLine2.frame = CGRectMake(5, 80, 285, 0.5);
                }
                else{
                    lblLine2.frame = CGRectMake(5, 80, 324, 0.5);
                }
            }
            else{
                lblLine2.frame = CGRectMake(5, 80, 678, 0.5);
            }
            lblLine2.backgroundColor = [UIColor lightGrayColor];
            [vwCateUpdate addSubview:lblLine2];
            
            
            UIButton *btnCreateDropbox = [UIButton buttonWithType:UIButtonTypeCustom];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    btnCreateDropbox.frame = CGRectMake(240, 90, 25, 25);
                }
                else if(result.width == 375){
                    btnCreateDropbox.frame = CGRectMake(295, 90, 25, 25);
                }
                else{
                    btnCreateDropbox.frame = CGRectMake(334, 90, 25, 25);
                }
            }
            else{
                btnCreateDropbox.frame = CGRectMake(688, 90, 25, 25);
            }
            [btnCreateDropbox setBackgroundImage:[UIImage imageNamed:@"btndropbox.png"] forState:UIControlStateNormal];
            [btnCreateDropbox addTarget:self action:@selector(clickCreateDropBox:) forControlEvents:UIControlEventTouchUpInside];
            [vwCateUpdate addSubview:btnCreateDropbox];
            
            int intDropboxLink = arc4random_uniform(9999);
            
            JJMaterialTextfield *txtUpdateDropboxLink = [[JJMaterialTextfield alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    txtUpdateDropboxLink.frame = CGRectMake(5, 88, 230, 30);
                }
                else if(result.width == 375){
                    txtUpdateDropboxLink.frame = CGRectMake(5, 88, 285, 30);
                }
                else{
                    txtUpdateDropboxLink.frame = CGRectMake(5, 88, 324, 30);
                }
            }
            else{
                txtUpdateDropboxLink.frame = CGRectMake(5, 88, 678, 30);
            }
            txtUpdateDropboxLink.enableMaterialPlaceHolder = NO;
            txtUpdateDropboxLink.lineColor = [UIColor clearColor];
            txtUpdateDropboxLink.borderStyle = UITextBorderStyleNone;
            txtUpdateDropboxLink.font = [UIFont fontWithName:@"TitilliumWeb-Regular" size:12.0];
            [txtUpdateDropboxLink setPlaceholder:@"Share link via Dropbox"];
            txtUpdateDropboxLink.text = [[self.arrImageCateDetails objectAtIndex:x] objectForKey:@"dropbox_link"];
            txtUpdateDropboxLink.autocorrectionType = UITextAutocorrectionTypeNo;
            txtUpdateDropboxLink.keyboardType = UIKeyboardTypeDefault;
            txtUpdateDropboxLink.delegate = self;
            txtUpdateDropboxLink.tag = intDropboxLink;
            [vwCateUpdate addSubview:txtUpdateDropboxLink];
            
            [self.arrImageCateUpDateDropboxLink addObject:[NSString stringWithFormat:@"%d",intDropboxLink]];
            
            NSLog(@"%@",self.arrImageCateUpDateDropboxLink);
            
            UILabel *lblLine3 = [[UILabel alloc] init];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if(result.width == 320){
                    lblLine3.frame = CGRectMake(5, 120, 230, 0.5);
                }
                else if(result.width == 375){
                    lblLine3.frame = CGRectMake(5, 120, 285, 0.5);
                }
                else{
                    lblLine3.frame = CGRectMake(5, 120, 324, 0.5);
                }
            }
            else{
                lblLine3.frame = CGRectMake(5, 120, 678, 0.5);
            }
            lblLine3.backgroundColor = [UIColor lightGrayColor];
            [vwCateUpdate addSubview:lblLine3];
            
            viewHeight = viewHeight + 150;
        }
    }
    
    UIView *vwPreViousNextBtn = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwPreViousNextBtn.frame = CGRectMake(25, viewHeight, 270, 35);
        }
        else if(result.width == 375){
            vwPreViousNextBtn.frame = CGRectMake(52.5, viewHeight, 270, 35);
        }
        else{
            vwPreViousNextBtn.frame = CGRectMake(72, viewHeight, 270, 35);
        }
    }
    else{
        vwPreViousNextBtn.frame = CGRectMake(249, viewHeight, 270, 35);
    }
    vwPreViousNextBtn.backgroundColor = [UIColor clearColor];
    [self.scrollViewThirdCategorised addSubview:vwPreViousNextBtn];
    
    UIButton *btnCatPreviousTwo = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCatPreviousTwo.frame = CGRectMake(0, 0, 130, 35);
    btnCatPreviousTwo.layer.borderWidth = 0.5;
    btnCatPreviousTwo.layer.cornerRadius = 5.0;
    btnCatPreviousTwo.layer.borderColor = [UIColor clearColor].CGColor;
    [btnCatPreviousTwo setTitle:@"PREVIOUS" forState:UIControlStateNormal];
    [btnCatPreviousTwo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnCatPreviousTwo.titleLabel.font = [UIFont fontWithName:@"Bebas" size:14.0];
    btnCatPreviousTwo.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
    [btnCatPreviousTwo addTarget:self action:@selector(clickFromPreviosCateImage:) forControlEvents:UIControlEventTouchUpInside];
    [vwPreViousNextBtn addSubview:btnCatPreviousTwo];
    
    UIButton *btnCatNextTwo = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCatNextTwo.frame = CGRectMake(140, 0, 130, 35);
    btnCatNextTwo.layer.borderWidth = 0.5;
    btnCatNextTwo.layer.cornerRadius = 5.0;
    btnCatNextTwo.layer.borderColor = [UIColor clearColor].CGColor;
    [btnCatNextTwo setTitle:@"NEXT" forState:UIControlStateNormal];
    [btnCatNextTwo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnCatNextTwo.titleLabel.font = [UIFont fontWithName:@"Bebas" size:14.0];
    btnCatNextTwo.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:188.0/255.0 blue:0.0/255.0 alpha:1.0];
    [btnCatNextTwo addTarget:self action:@selector(clickToNextCateImage:) forControlEvents:UIControlEventTouchUpInside];
    [vwPreViousNextBtn addSubview:btnCatNextTwo];
    
    viewHeight = viewHeight + 50;
    
    UIView *vwFour = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwFour.frame = CGRectMake(0, viewHeight, 320, 40);
        }
        else if(result.width == 375){
            vwFour.frame = CGRectMake(0, viewHeight, 375, 40);
        }
        else{
            vwFour.frame = CGRectMake(0, viewHeight, 414, 40);
        }
    }
    else{
        vwFour.frame = CGRectMake(0, viewHeight, 768, 40);
    }
    vwFour.backgroundColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0];
    [self.scrollViewThirdCategorised addSubview:vwFour];
    
    UIImageView *imgVwFour = [[UIImageView alloc] init];
    imgVwFour.frame = CGRectMake(10, 8, 25, 25);
    imgVwFour.image = [UIImage imageNamed:@"btngray4.png"];
    [vwFour addSubview:imgVwFour];
    
    UILabel *lblFour = [[UILabel alloc] init];
    lblFour.frame = CGRectMake(45, 11, 140, 20);
    lblFour.text = @"UPLOAD  VIDEOS";
    lblFour.textColor = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0];
    lblFour.font = [UIFont fontWithName:@"Bebas" size:17.0];
    [vwFour addSubview:lblFour];
    
    viewHeight = viewHeight + 41;
    
    UIView *vwFive = [[UIView alloc] init];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            vwFive.frame = CGRectMake(0, viewHeight, 320, 40);
        }
        else if(result.width == 375){
            vwFive.frame = CGRectMake(0, viewHeight, 375, 40);
        }
        else{
            vwFive.frame = CGRectMake(0, viewHeight, 414, 40);
        }
    }
    else{
        vwFive.frame = CGRectMake(0, viewHeight, 768, 40);
    }
    vwFive.backgroundColor = [UIColor colorWithRed:163.0/255.0 green:163.0/255.0 blue:163.0/255.0 alpha:1.0];
    [self.scrollViewThirdCategorised addSubview:vwFive];
    
    UIImageView *imgVwFive = [[UIImageView alloc] init];
    imgVwFive.frame = CGRectMake(10, 8, 25, 25);
    imgVwFive.image = [UIImage imageNamed:@"btngray5.png"];
    [vwFive addSubview:imgVwFive];
    
    UILabel *lblFive = [[UILabel alloc] init];
    lblFive.frame = CGRectMake(45, 11, 200, 20);
    lblFive.text = @"ORDER  CONFIRMATION";
    lblFive.textColor = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0];
    lblFive.font = [UIFont fontWithName:@"Bebas" size:17.0];
    [vwFive addSubview:lblFive];
    
    viewHeight = viewHeight + 41;
    
    [self.scrollViewThirdCategorised setContentSize:CGSizeMake(0, viewHeight)];
}

- (IBAction)clickGDriveUnCatImage:(id)sender {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:@"googledrive://https://drive.google.com/open?"];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Opened url");
        }
        else{
            NSLog(@"Not Opened url");
            UIApplication *application = [UIApplication sharedApplication];
            NSURL *URL = [NSURL URLWithString:@"https://itunes.apple.com/us/app/google-drive/id507874739?mt=8"];
            [application openURL:URL options:@{} completionHandler:^(BOOL success) {
                if (success) {
                    NSLog(@"Opened url");
                }
            }];
        }
    }];
}

- (IBAction)clickDropboxUnCatImage:(id)sender {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:@"dbapi-2://"];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Opened url");
        }
        else{
            NSLog(@"Not Opened url");
            UIApplication *application = [UIApplication sharedApplication];
            NSURL *URL = [NSURL URLWithString:@"https://itunes.apple.com/in/app/dropbox/id327630330?mt=8"];
            [application openURL:URL options:@{} completionHandler:^(BOOL success) {
                if (success) {
                    NSLog(@"Opened url");
                }
            }];
        }
    }];
}

-(void)clickCreateGDrive:(id)sender{
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:@"googledrive://https://drive.google.com/open?"];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Opened url");
        }
        else{
            NSLog(@"Not Opened url");
            UIApplication *application = [UIApplication sharedApplication];
            NSURL *URL = [NSURL URLWithString:@"https://itunes.apple.com/us/app/google-drive/id507874739?mt=8"];
            [application openURL:URL options:@{} completionHandler:^(BOOL success) {
                if (success) {
                    NSLog(@"Opened url");
                }
            }];
        }
    }];
}

-(void)clickCreateDropBox:(id)sender{
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:@"dbapi-2://"];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Opened url");
        }
        else{
            NSLog(@"Not Opened url");
            UIApplication *application = [UIApplication sharedApplication];
            NSURL *URL = [NSURL URLWithString:@"https://itunes.apple.com/in/app/dropbox/id327630330?mt=8"];
            [application openURL:URL options:@{} completionHandler:^(BOOL success) {
                if (success) {
                    NSLog(@"Opened url");
                }
            }];
        }
    }];
}

-(void)clickCreateVideoAlbum:(id)sender{
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        if(self.txtCreateVideoAlbumName.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter video caption."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtCreateVideoGDrive.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select or enter google drive link."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtCreateVideoDropbox.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select or enter dropbox link."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else{
            
            [self.txtCreateVideoAlbumName resignFirstResponder];
            [self.txtCreateVideoGDrive resignFirstResponder];
            [self.txtCreateVideoDropbox resignFirstResponder];
            
            [self.navigationController.view showActivityView];
            
            int r = arc4random_uniform(99999);
            NSString *strUnique = @"VID";
            strUnique = [strUnique stringByAppendingString:[NSString stringWithFormat:@"%d",r]];
            NSLog(@"%@",strUnique);
            
            NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_CREATE_CATEGORIESD_VIDEO_ALBUM];
            NSLog(@"%@",strURL);
            
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
            [request setDelegate:self];
            [request setRequestMethod:@"POST"];
            [request addRequestHeader:@"Content-Type" value:@"application/json"];
            [request setDidFinishSelector:@selector(submitCreateCatVideoComp:)];
            [request setDidFailSelector:@selector(submitCreateCatVideoFail:)];
            [request addPostValue:self.txtCreateVideoAlbumName.text forKey:@"video_title"];
            [request addPostValue:self.txtCreateVideoGDrive.text forKey:@"google_drive_link"];
            [request addPostValue:self.txtCreateVideoDropbox.text forKey:@"dropbox_link"];
            [request addPostValue:strUnique forKey:@"unique_identifier_code"];
            
            [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
            [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
            //[request addPostValue:IOS forKey:@"mode"];
            [request startAsynchronous];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitCreateCatVideoComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            [self getOrderDetailsAfterCreateVideoAlbum];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitCreateCatVideoFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)clickCreateImageAlbum:(id)sender{
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        if(self.txtCreateAlbumName.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter album name."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtCreateGDrive.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select or enter google drive link."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtCreateDropbox.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select or enter dropbox link."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else{
            
            [self.txtCreateAlbumName resignFirstResponder];
            [self.txtCreateGDrive resignFirstResponder];
            [self.txtCreateDropbox resignFirstResponder];
            
            [self.navigationController.view showActivityView];
            
            int r = arc4random_uniform(99999);
            NSString *strUnique = @"CAT";
            strUnique = [strUnique stringByAppendingString:[NSString stringWithFormat:@"%d",r]];
            NSLog(@"%@",strUnique);
            
            NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_CREATE_CATEGORIESD_ALBUM];
            NSLog(@"%@",strURL);
            
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
            [request setDelegate:self];
            [request setRequestMethod:@"POST"];
            [request addRequestHeader:@"Content-Type" value:@"application/json"];
            [request setDidFinishSelector:@selector(submitCreateCatImageComp:)];
            [request setDidFailSelector:@selector(submitCreateCatImageFail:)];
            [request addPostValue:self.txtCreateAlbumName.text forKey:@"image_category_name"];
            [request addPostValue:self.txtCreateGDrive.text forKey:@"google_drive_link"];
            [request addPostValue:self.txtCreateDropbox.text forKey:@"dropbox_link"];
            [request addPostValue:strUnique forKey:@"unique_identifier_code"];
            
            [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
            [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
            //[request addPostValue:IOS forKey:@"mode"];
            [request startAsynchronous];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitCreateCatImageComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            [self getOrderDetailsAfterCreateImageAlbum];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitCreateCatImageFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)getOrderDetailsAfterCreateVideoAlbum{
    [self.navigationController.view showActivityView];
    
    NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_GET_ORDER_DETAILS];
    NSLog(@"%@",strURL);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
    [request setDelegate:self];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setDidFinishSelector:@selector(getOrderDetailsAfterCreateVideoAlbumComp:)];
    [request setDidFailSelector:@selector(getOrderDetailsAfterCreateVideoAlbumFail:)];
    [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
    [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
    //[request addPostValue:IOS forKey:@"mode"];
    [request startAsynchronous];
}

-(void)getOrderDetailsAfterCreateVideoAlbumComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            self.arrVideoCateDetails = [[respon objectForKey:@"details"] objectForKey:@"video_details"];
            NSLog(@"%@",self.arrVideoCateDetails);
            
            [self createScrollViewFourCategorised];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getOrderDetailsAfterCreateVideoAlbumFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)getOrderDetailsAfterCreateImageAlbum{
    [self.navigationController.view showActivityView];
    
    NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_GET_ORDER_DETAILS];
    NSLog(@"%@",strURL);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
    [request setDelegate:self];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setDidFinishSelector:@selector(getOrderDetailsAfterCreateImageAlbumComp:)];
    [request setDidFailSelector:@selector(getOrderDetailsAfterCreateImageAlbumFail:)];
    [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
    [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
    //[request addPostValue:IOS forKey:@"mode"];
    [request startAsynchronous];
}

-(void)getOrderDetailsAfterCreateImageAlbumComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            self.arrImageCateDetails = [[respon objectForKey:@"details"] objectForKey:@"image_category_details"];
            
            [self createScrollViewThirdCategorised];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getOrderDetailsAfterCreateImageAlbumFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)clickCreateVideoAlbumUpdate:(id)sender{
    UIButton *btn = (UIButton *)sender;
    
    NSLog(@"%d",[[self.arrVideoCateUpDateCatName objectAtIndex:btn.tag] intValue]);
    
    UITextField *textField1  = (UITextField *)[self.scrollViewFourVideoCategorised viewWithTag:[[self.arrVideoCateUpDateCatName objectAtIndex:btn.tag] intValue]];
    
    NSLog(@"%@",textField1.text);
    
    UITextField *textField2  = (UITextField *)[self.scrollViewFourVideoCategorised viewWithTag:[[self.arrVideoCateUpDateGDriveLink objectAtIndex:btn.tag] intValue]];
    
    NSLog(@"%@",textField2.text);
    
    UITextField *textField3  = (UITextField *)[self.scrollViewFourVideoCategorised viewWithTag:[[self.arrVideoCateUpDateDropboxLink objectAtIndex:btn.tag] intValue]];
    
    NSLog(@"%@",textField3.text);
    
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        if(textField1.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter video caption."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(textField2.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select or enter google drive link."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(textField3.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select or enter dropbox link."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else{
            
            //            [self.txtCreateAlbumName resignFirstResponder];
            //            [self.txtCreateGDrive resignFirstResponder];
            //            [self.txtCreateDropbox resignFirstResponder];
            
            [self.navigationController.view showActivityView];
            
            NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_UPDATE_CATEGORIESD_VIDEO_ALBUM];
            NSLog(@"%@",strURL);
            
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
            [request setDelegate:self];
            [request setRequestMethod:@"POST"];
            [request addRequestHeader:@"Content-Type" value:@"application/json"];
            [request setDidFinishSelector:@selector(submitUpdateCatVideoComp:)];
            [request setDidFailSelector:@selector(submitUpdateCatVideoFail:)];
            [request addPostValue:textField1.text forKey:@"video_title"];
            [request addPostValue:textField2.text forKey:@"google_drive_link"];
            [request addPostValue:textField3.text forKey:@"dropbox_link"];
            
            [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
            [request addPostValue:[[self.arrVideoCateDetails objectAtIndex:btn.tag] objectForKey:@"id"] forKey:@"video_catid"];
            [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
            //[request addPostValue:IOS forKey:@"mode"];
            [request startAsynchronous];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitUpdateCatVideoComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            [self getOrderDetailsAfterCreateVideoAlbum];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitUpdateCatVideoFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)clickCreateImageAlbumUpdate:(id)sender{
    UIButton *btn = (UIButton *)sender;
    
    NSLog(@"%d",[[self.arrImageCateUpDateCatName objectAtIndex:btn.tag] intValue]);
    
    UITextField *textField1  = (UITextField *)[self.scrollViewThirdCategorised viewWithTag:[[self.arrImageCateUpDateCatName objectAtIndex:btn.tag] intValue]];
    
    NSLog(@"%@",textField1.text);
    
    UITextField *textField2  = (UITextField *)[self.scrollViewThirdCategorised viewWithTag:[[self.arrImageCateUpDateGDriveLink objectAtIndex:btn.tag] intValue]];
    
    NSLog(@"%@",textField2.text);
    
    UITextField *textField3  = (UITextField *)[self.scrollViewThirdCategorised viewWithTag:[[self.arrImageCateUpDateDropboxLink objectAtIndex:btn.tag] intValue]];
    
    NSLog(@"%@",textField3.text);
    
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        if(textField1.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter album name."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(textField2.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select or enter google drive link."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(textField3.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select or enter dropbox link."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else{
            
//            [self.txtCreateAlbumName resignFirstResponder];
//            [self.txtCreateGDrive resignFirstResponder];
//            [self.txtCreateDropbox resignFirstResponder];
            
            [self.navigationController.view showActivityView];
            
            NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_UPDATE_CATEGORIESD_ALBUM];
            NSLog(@"%@",strURL);
            
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
            [request setDelegate:self];
            [request setRequestMethod:@"POST"];
            [request addRequestHeader:@"Content-Type" value:@"application/json"];
            [request setDidFinishSelector:@selector(submitUpdateCatImageComp:)];
            [request setDidFailSelector:@selector(submitUpdateCatImageFail:)];
            [request addPostValue:textField1.text forKey:@"image_category_name"];
            [request addPostValue:textField2.text forKey:@"google_drive_link"];
            [request addPostValue:textField3.text forKey:@"dropbox_link"];
            
            [request addPostValue:[[self.arrImageCateDetails objectAtIndex:btn.tag] objectForKey:@"id"] forKey:@"img_catid"];
            [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
            //[request addPostValue:IOS forKey:@"mode"];
            [request startAsynchronous];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitUpdateCatImageComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            [self getOrderDetailsAfterCreateImageAlbum];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitUpdateCatImageFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)clickCreateVideoAlbumDelete:(id)sender{
    UIButton *btn = (UIButton *)sender;
    
    NSLog(@"%@",[self.arrVideoCateDetails objectAtIndex:btn.tag]);
    
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_DELETE_CATEGORIESD_VIDEO_ALBUM];
        NSLog(@"%@",strURL);
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(deleteVideoAlbumComp:)];
        [request setDidFailSelector:@selector(deleteVideoAlbumFail:)];
        [request addPostValue:[[self.arrVideoCateDetails objectAtIndex:btn.tag] objectForKey:@"unique_identifier_code"] forKey:@"identifier_code"];
        [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)deleteVideoAlbumComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            [self getOrderDetailsAfterCreateVideoAlbum];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)deleteVideoAlbumFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)clickCreateImageAlbumDelete:(id)sender{
    UIButton *btn = (UIButton *)sender;
    
    NSLog(@"%@",[self.arrImageCateDetails objectAtIndex:btn.tag]);
    
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];

        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_DELETE_CATEGORIESD_ALBUM];
        NSLog(@"%@",strURL);

        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(deleteImageAlbumComp:)];
        [request setDidFailSelector:@selector(deleteImageAlbumFail:)];
        [request addPostValue:[[self.arrImageCateDetails objectAtIndex:btn.tag] objectForKey:@"unique_identifier_code"] forKey:@"identifier_code"];
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)deleteImageAlbumComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            [self getOrderDetailsAfterCreateImageAlbum];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)deleteImageAlbumFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture{
    [self.txtCusFirstName resignFirstResponder];
    [self.txtCusLastName resignFirstResponder];
    [self.txtCusEmail resignFirstResponder];
    [self.txtCusContactNo resignFirstResponder];
    [self.txtVwNotes resignFirstResponder];
    
    [self.txtGroomName resignFirstResponder];
    [self.txtBrideName resignFirstResponder];
    [self.txtVwAboutGroom resignFirstResponder];
    [self.txtVwAboutBride resignFirstResponder];
}

#pragma mark - XHScrollMenu Delegate Methods

- (void)scrollMenuDidSelected:(XHScrollMenu *)scrollMenu menuIndex:(NSUInteger)selectIndex {
    
    NSLog(@"selectIndex : %lu", (unsigned long)selectIndex);
    
    if(selectIndex == 0){
        self.tableViewOrderHostory.hidden = YES;
        if([self.strCurrentScrollView isEqualToString:@"1"]){
            self.scrollOrderFirst.hidden = NO;
        }
        else if([self.strCurrentScrollView isEqualToString:@"2"]){
            self.scrollOrderSecond.hidden = NO;
        }
        else if([self.strCurrentScrollView isEqualToString:@"3"]){
            if([self.strImageCateType isEqualToString:@""]){
                self.ScrollOrderThird.hidden = NO;
            }
            else if ([self.strImageCateType isEqualToString:@"C"]){
                self.scrollViewThirdCategorised.hidden = NO;
            }
            else if ([self.strImageCateType isEqualToString:@"NC"]){
                self.ScrollOrderThird.hidden = NO;
                self.viewCateUnCategorised.hidden = YES;
                self.viewUnCategorised.hidden = NO;
            }
        }
        else if([self.strCurrentScrollView isEqualToString:@"4"]){
            self.scrollViewFourVideoCategorised.hidden = NO;
        }
        else if([self.strCurrentScrollView isEqualToString:@"5"]){
            self.scrollOrderConfirmation.hidden = NO;
        }
    }
    else{
        self.tableViewOrderHostory.hidden = NO;
        self.scrollOrderFirst.hidden = YES;
        self.scrollOrderSecond.hidden = YES;
        self.ScrollOrderThird.hidden = YES;
        self.scrollViewThirdCategorised.hidden = YES;
        self.scrollViewFourVideoCategorised.hidden = YES;
        self.scrollOrderConfirmation.hidden = YES;
    }
}

- (void)scrollMenuDidManagerSelected:(XHScrollMenu *)scrollMenu{
    
}

- (IBAction)clickNextOne:(id)sender {
//    self.strCurrentScrollView = @"2";
//    self.scrollOrderFirst.hidden = YES;
//    self.scrollOrderSecond.hidden = NO;
//
//    [self getOrderDetails];
    
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        if(self.txtCusFirstName.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter customer first name."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtCusLastName.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter customer last name."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtCusEmail.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter customer email id."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtCusContactNo.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter customer phone number."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else{
            NSString *strCusNote;
            if([self.txtVwNotes.text isEqualToString:@"Note (If any)"]){
                strCusNote = @"";
            }
            else{
                strCusNote = self.txtVwNotes.text;
            }

            [self.txtCusFirstName resignFirstResponder];
            [self.txtCusLastName resignFirstResponder];
            [self.txtCusEmail resignFirstResponder];
            [self.txtCusContactNo resignFirstResponder];
            [self.txtVwNotes resignFirstResponder];

            [self.navigationController.view showActivityView];

            NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_SUBMIT_CUSTOMER_DETAILS];
            NSLog(@"%@",strURL);

            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
            [request setDelegate:self];
            [request setRequestMethod:@"POST"];
            [request addRequestHeader:@"Content-Type" value:@"application/json"];
            [request setDidFinishSelector:@selector(submitCustomerDetailsComp:)];
            [request setDidFailSelector:@selector(submitCustomerDetailsFail:)];
            [request addPostValue:self.txtCusFirstName.text forKey:@"customer_fname"];
            [request addPostValue:self.txtCusLastName.text forKey:@"customer_lname"];
            [request addPostValue:self.txtCusEmail.text forKey:@"customer_email"];
            [request addPostValue:self.txtCusContactNo.text forKey:@"customer_phone"];
            [request addPostValue:self.txtVwNotes.text forKey:@"customer_note"];
            [request addPostValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"USERID"] forKey:@"photographer_id"];
            [request addPostValue:self.strOrderID forKey:@"order_id"];
            [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
            //[request addPostValue:IOS forKey:@"mode"];
            [request startAsynchronous];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitCustomerDetailsComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            self.strOrderID = [respon objectForKey:@"order_id"];
            
            self.strCurrentScrollView = @"2";
            self.scrollOrderFirst.hidden = YES;
            self.scrollOrderSecond.hidden = NO;
            
            [self getOrderDetails];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitCustomerDetailsFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)getOrderDetails{
    [self.navigationController.view showActivityView];
    
    NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_GET_ORDER_DETAILS];
    NSLog(@"%@",strURL);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
    [request setDelegate:self];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setDidFinishSelector:@selector(getOrderDetailsComp:)];
    [request setDidFailSelector:@selector(getOrderDetailsFail:)];
    [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
    [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
    //[request addPostValue:IOS forKey:@"mode"];
    [request startAsynchronous];
}

-(void)getOrderDetailsComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            if(![[[respon objectForKey:@"details"] objectForKey:@"bride_name"] isKindOfClass:[NSNull class]]){
                self.txtBrideName.text = [[respon objectForKey:@"details"] objectForKey:@"bride_name"];
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"about_bride"] isKindOfClass:[NSNull class]]){
                self.txtVwAboutBride.text = [[respon objectForKey:@"details"] objectForKey:@"about_bride"];
                self.txtVwAboutBride.textColor = [UIColor blackColor];
                self.lblAboutBride.hidden = NO;
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"groom_name"] isKindOfClass:[NSNull class]]){
                self.txtGroomName.text = [[respon objectForKey:@"details"] objectForKey:@"groom_name"];
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"about_groom"] isKindOfClass:[NSNull class]]){
                self.txtVwAboutGroom.text = [[respon objectForKey:@"details"] objectForKey:@"about_groom"];
                self.txtVwAboutGroom.textColor = [UIColor blackColor];
                self.lblAboutGroom.hidden = NO;
            }
            
            
            if(![[[respon objectForKey:@"details"] objectForKey:@"upload_file_type"] isKindOfClass:[NSNull class]]){
                self.strImageCateType = [[respon objectForKey:@"details"] objectForKey:@"upload_file_type"];
            }
            else{
                self.strImageCateType = @"";
            }
            
            
            if(![[[respon objectForKey:@"details"] objectForKey:@"google_drive_link"] isKindOfClass:[NSNull class]]){
                self.txtGDriveLinkUnCatImage.text = [[respon objectForKey:@"details"] objectForKey:@"google_drive_link"];
            }
            
            if(![[[respon objectForKey:@"details"] objectForKey:@"dropbox_link"] isKindOfClass:[NSNull class]]){
                self.txtDropboxUnCaImage.text = [[respon objectForKey:@"details"] objectForKey:@"dropbox_link"];
            }
            
            if(isOpenORNot == NO){
                self.arrImageCateDetails = [[respon objectForKey:@"details"] objectForKey:@"image_category_details"];
                [self createScrollViewThirdCategorised];
                self.scrollViewThirdCategorised.hidden = YES;
                
                self.arrVideoCateDetails = [[respon objectForKey:@"details"] objectForKey:@"video_details"];
                [self createScrollViewFourCategorised];
                self.scrollViewFourVideoCategorised.hidden = YES;
            }
            
            if(![[[respon objectForKey:@"details"] objectForKey:@"preferred_subdomain_name"] isKindOfClass:[NSNull class]]){
                self.txtSubdomainName.text = [[respon objectForKey:@"details"] objectForKey:@"preferred_subdomain_name"];
            }
            
            if([[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] isKindOfClass:[NSDictionary class]]){
                self.lblDateFirstMeeting.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_1"];
                self.lblDateFirstMeeting.textColor = [UIColor blackColor];
                
                self.lblDateMarriageFixedOn.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_2"];
                self.lblDateMarriageFixedOn.textColor = [UIColor blackColor];
                
                self.lblDateGotEngagedOn.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_3"];
                self.lblDateGotEngagedOn.textColor = [UIColor blackColor];
                
                self.lblDateLegallyMarriedOn.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_4"];
                self.lblDateLegallyMarriedOn.textColor = [UIColor blackColor];
                
                self.lblDateSocialMarriage.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_5"];
                self.lblDateSocialMarriage.textColor = [UIColor blackColor];
                
                self.lblDateWeddingReception.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_6"];
                self.lblDateWeddingReception.textColor = [UIColor blackColor];
            }
            
            if(![[[respon objectForKey:@"details"] objectForKey:@"theme_name"] isKindOfClass:[NSNull class]]){
                self.viewTemplateDetails.hidden = NO;
                self.strTempID = [[respon objectForKey:@"details"] objectForKey:@"selected_template_id"];
                self.lblTemplateName.textColor = [UIColor blackColor];
                self.lblTemplateName.text = [[respon objectForKey:@"details"] objectForKey:@"theme_name"];
                self.lblTemplateNamee.text = [[respon objectForKey:@"details"] objectForKey:@"theme_name"];
                if(![[[respon objectForKey:@"details"] objectForKey:@"theme_code"] isKindOfClass:[NSNull class]]){
                    self.lblTemplateID.hidden = NO;
                    self.lblTemplateID.text = [self.lblTemplateID.text stringByAppendingString:[[respon objectForKey:@"details"] objectForKey:@"theme_code"]];
                }
                
                if(![[[respon objectForKey:@"details"] objectForKey:@"theme_thumbimage_url"] isKindOfClass:[NSNull class]]){
                    NSString *strImageURL = [[respon objectForKey:@"details"] objectForKey:@"theme_thumbimage_url"];
                    self.imgVwTemplate.imageURL = [NSURL URLWithString:strImageURL];
                }
            }
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getOrderDetailsFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

- (IBAction)clickValidateURL:(id)sender {
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        if(self.txtSubdomainName.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter subdomain name."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else{
            
            BOOL isValid = [self validateAlphanumericDash:self.txtSubdomainName.text];
            
            if(isValid == YES){
                [self.navigationController.view showActivityView];
                
                NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_VALIDATE_SUBDOMAIN];
                NSLog(@"%@",strURL);
                
                ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
                [request setDelegate:self];
                [request setRequestMethod:@"POST"];
                [request addRequestHeader:@"Content-Type" value:@"application/json"];
                [request setDidFinishSelector:@selector(validateSubdomainComp:)];
                [request setDidFailSelector:@selector(validateSubdomainFail:)];
                [request addPostValue:self.txtSubdomainName.text forKey:@"domain_name"];
                [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
                [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
                //[request addPostValue:IOS forKey:@"mode"];
                [request startAsynchronous];
            }
            else{
                [ISMessages showCardAlertWithTitle:@"Cliicker"
                                           message:@"Subdomain name only contains alphanumeric and dash(-)"
                                          duration:5.0
                                       hideOnSwipe:YES
                                         hideOnTap:YES
                                         alertType:ISAlertTypeWarning
                                     alertPosition:ISAlertPositionTop
                                           didHide:^(BOOL finished) {
                                               NSLog(@"Alert did hide.");
                                           }];
            }
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)validateSubdomainComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"is_available"]] isEqualToString:@"1"]){
            
            self.btnValidateURL.backgroundColor = [UIColor greenColor];
            self.btnValidateURL.titleLabel.textColor = [UIColor blackColor];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)validateSubdomainFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

- (IBAction)clickDateFirstMeeting:(id)sender {
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] target:self action:@selector(dateFirstMeetSelected:element:) origin:self.lblDateFirstMeeting];
    self.actionSheetPicker.hideCancel = YES;
    [self.actionSheetPicker showActionSheetPicker];
}

- (IBAction)clickDateMarriageFixedOn:(id)sender {
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] target:self action:@selector(dateMarriageFixedOnSelected:element:) origin:self.lblDateMarriageFixedOn];
    self.actionSheetPicker.hideCancel = YES;
    [self.actionSheetPicker showActionSheetPicker];
}

- (IBAction)clickDateGotEngagedOn:(id)sender {
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] target:self action:@selector(dateGotEngagedOnSelected:element:) origin:self.lblDateGotEngagedOn];
    self.actionSheetPicker.hideCancel = YES;
    [self.actionSheetPicker showActionSheetPicker];
}

- (IBAction)clickDateLegallyMarriedOn:(id)sender {
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] target:self action:@selector(dateLegallyMarriedOnSelected:element:) origin:self.lblDateLegallyMarriedOn];
    self.actionSheetPicker.hideCancel = YES;
    [self.actionSheetPicker showActionSheetPicker];
}

- (IBAction)clickDateSocialMarriage:(id)sender {
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] target:self action:@selector(dateSocialMarriageSelected:element:) origin:self.lblDateSocialMarriage];
    self.actionSheetPicker.hideCancel = YES;
    [self.actionSheetPicker showActionSheetPicker];
}

- (IBAction)clickDateWeddingReception:(id)sender {
    _actionSheetPicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] target:self action:@selector(dateWeddingReceptionSelected:element:) origin:self.lblDateWeddingReception];
    self.actionSheetPicker.hideCancel = YES;
    [self.actionSheetPicker showActionSheetPicker];
}

- (void)dateFirstMeetSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *value = [dateFormatter stringFromDate:self.selectedDate];
    
    self.lblDateFirstMeeting.textColor = [UIColor blackColor];
    self.lblDateFirstMeeting.text = value;
}

- (void)dateMarriageFixedOnSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *value = [dateFormatter stringFromDate:self.selectedDate];
    
    self.lblDateMarriageFixedOn.textColor = [UIColor blackColor];
    self.lblDateMarriageFixedOn.text = value;
}

- (void)dateGotEngagedOnSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *value = [dateFormatter stringFromDate:self.selectedDate];
    
    self.lblDateGotEngagedOn.textColor = [UIColor blackColor];
    self.lblDateGotEngagedOn.text = value;
}

- (void)dateLegallyMarriedOnSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *value = [dateFormatter stringFromDate:self.selectedDate];
    
    self.lblDateLegallyMarriedOn.textColor = [UIColor blackColor];
    self.lblDateLegallyMarriedOn.text = value;
}

- (void)dateSocialMarriageSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *value = [dateFormatter stringFromDate:self.selectedDate];
    
    self.lblDateSocialMarriage.textColor = [UIColor blackColor];
    self.lblDateSocialMarriage.text = value;
}

- (void)dateWeddingReceptionSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *value = [dateFormatter stringFromDate:self.selectedDate];
    
    self.lblDateWeddingReception.textColor = [UIColor blackColor];
    self.lblDateWeddingReception.text = value;
}

- (IBAction)clickChooseTemplate:(id)sender {
    TemplateListViewController *obj_ListUser;
    obj_ListUser = [[TemplateListViewController alloc] initWithNibName:@"TemplateListViewController" bundle:nil];
    obj_ListUser.delegate = self;
    
    UINavigationController *navController=[[UINavigationController alloc] initWithRootViewController:obj_ListUser];
    navController.modalPresentationStyle=UIModalPresentationFormSheet;
    navController.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
    [self presentViewController:navController animated:YES completion:nil];
}

-(void)selectTemplate:(NSString*)templateID TemplateName:(NSString*)templatename TemplateURL:(NSString*)templateurl TemplateCLRDL:(NSString*)templateCLRid TemplateURLBIG:(NSString*)templateurlbig{
    self.viewTemplateDetails.hidden = NO;
    
    self.strTempID = templateID;
    self.strTempURLBIG = templateurlbig;
    
    self.lblTemplateName.textColor = [UIColor blackColor];
    self.lblTemplateName.text = templatename;
    self.lblTemplateNamee.text = templatename;
    self.lblTemplateID.text = templateCLRid;
    
    self.imgVwTemplate.imageURL = [NSURL URLWithString:templateurl];
}

- (IBAction)clickViewTemplate:(id)sender {
    if(![self.strTempURLBIG isEqualToString:@""]){
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:self.strTempURLBIG];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            if (success) {
                NSLog(@"Opened url");
            }
        }];
    }
}

- (IBAction)clickPreviousOne:(id)sender {
    self.strCurrentScrollView = @"1";
    self.scrollOrderFirst.hidden = NO;
    self.scrollOrderSecond.hidden = YES;
}

- (IBAction)clickNextTwo:(id)sender {
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        if(self.txtBrideName.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter bride's name."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtGroomName.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter groom's name."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if(self.txtSubdomainName.text.length == 0){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please enter your sub domain name."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if([self.lblDateFirstMeeting.text isEqualToString:@"First meeting"]){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select first meeting date."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if([self.lblDateMarriageFixedOn.text isEqualToString:@"Marriage was fixed on"]){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select marriage fixed on date."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if([self.lblDateGotEngagedOn.text isEqualToString:@"Got engaged on"]){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select got engaged on date."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if([self.lblDateLegallyMarriedOn.text isEqualToString:@"Legally got married on"]){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select legally married on date."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if([self.lblDateSocialMarriage.text isEqualToString:@"Social marriage"]){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select social marriage date."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if([self.lblDateWeddingReception.text isEqualToString:@"Wedding reception"]){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select wedding reception date."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else if([self.strTempID isEqualToString:@""]){
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Please select one template."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
        else{
            NSString *strAboutBride;
            if([self.txtVwAboutBride.text isEqualToString:@"About the bride..."]){
                strAboutBride = @"";
            }
            else{
                strAboutBride = self.txtVwAboutBride.text;
            }
            
            NSString *strAboutGroom;
            if([self.txtVwAboutGroom.text isEqualToString:@"About the groom..."]){
                strAboutGroom = @"";
            }
            else{
                strAboutGroom = self.txtVwAboutGroom.text;
            }
            
            [self.txtBrideName resignFirstResponder];
            [self.txtGroomName resignFirstResponder];
            [self.txtSubdomainName resignFirstResponder];
            [self.txtVwAboutBride resignFirstResponder];
            [self.txtVwAboutGroom resignFirstResponder];
            
            [self.navigationController.view showActivityView];
            
            NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_SUBMIT_ALBUM_DETAILS];
            NSLog(@"%@",strURL);
            
            ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
            [request setDelegate:self];
            [request setRequestMethod:@"POST"];
            [request addRequestHeader:@"Content-Type" value:@"application/json"];
            [request setDidFinishSelector:@selector(submitAlbumDetailsComp:)];
            [request setDidFailSelector:@selector(submitAlbumDetailsFail:)];
            [request addPostValue:self.txtBrideName.text forKey:@"bride_name"];
            [request addPostValue:self.txtGroomName.text forKey:@"groom_name"];
            [request addPostValue:self.txtSubdomainName.text forKey:@"preferred_subdomain_name"];
            [request addPostValue:self.txtVwAboutBride.text forKey:@"about_bride"];
            [request addPostValue:self.txtVwAboutGroom.text forKey:@"about_groom"];
            [request addPostValue:self.strTempID forKey:@"selected_template_id"];
            
            [request addPostValue:self.lblDateFirstMeeting.text forKey:@"WSD1"];
            [request addPostValue:self.lblDateMarriageFixedOn.text forKey:@"WSD2"];
            [request addPostValue:self.lblDateGotEngagedOn.text forKey:@"WSD3"];
            [request addPostValue:self.lblDateLegallyMarriedOn.text forKey:@"WSD4"];
            [request addPostValue:self.lblDateSocialMarriage.text forKey:@"WSD5"];
            [request addPostValue:self.lblDateWeddingReception.text forKey:@"WSD6"];
            
            [request addPostValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"USERID"] forKey:@"photographer_id"];
            [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
            [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
            //[request addPostValue:IOS forKey:@"mode"];
            [request startAsynchronous];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitAlbumDetailsComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            //self.strOrderID = [respon objectForKey:@"order_id"];
            
            if([self.strImageCateType isEqualToString:@"C"]){
                self.strCurrentScrollView = @"3";
                self.scrollOrderSecond.hidden = YES;
                self.scrollViewThirdCategorised.hidden = NO;
                isOpenORNot = YES;
            }
            else if([self.strImageCateType isEqualToString:@"NC"]){
                self.strCurrentScrollView = @"3";
                self.scrollOrderSecond.hidden = YES;
                self.ScrollOrderThird.hidden = NO;
                self.viewCateUnCategorised.hidden = YES;
                self.viewUnCategorised.hidden = NO;
                isOpenORNot = YES;
            }
            else{
                self.strCurrentScrollView = @"3";
                self.scrollOrderSecond.hidden = YES;
                self.ScrollOrderThird.hidden = NO;
                isOpenORNot = YES;
            }
            
            [self getOrderDetails];
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitAlbumDetailsFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

- (IBAction)clickPreviousTwo:(id)sender {
    self.strCurrentScrollView = @"2";
    self.scrollOrderSecond.hidden = NO;
    self.ScrollOrderThird.hidden = YES;
}

- (IBAction)clickNextThree:(id)sender {
    if([self.strImageCateType isEqualToString:@""]){
    }
    else{
        if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
            if(self.txtGDriveLinkUnCatImage.text.length == 0){
                [ISMessages showCardAlertWithTitle:@"Cliicker"
                                           message:@"Please select or enter google drive link."
                                          duration:5.0
                                       hideOnSwipe:YES
                                         hideOnTap:YES
                                         alertType:ISAlertTypeWarning
                                     alertPosition:ISAlertPositionTop
                                           didHide:^(BOOL finished) {
                                               NSLog(@"Alert did hide.");
                                           }];
            }
            else if(self.txtDropboxUnCaImage.text.length == 0){
                [ISMessages showCardAlertWithTitle:@"Cliicker"
                                           message:@"Please select or enter dropbox link."
                                          duration:5.0
                                       hideOnSwipe:YES
                                         hideOnTap:YES
                                         alertType:ISAlertTypeWarning
                                     alertPosition:ISAlertPositionTop
                                           didHide:^(BOOL finished) {
                                               NSLog(@"Alert did hide.");
                                           }];
            }
            else{
                [self.txtGDriveLinkUnCatImage resignFirstResponder];
                [self.txtDropboxUnCaImage resignFirstResponder];
                
                [self.navigationController.view showActivityView];
                
//                int r = arc4random_uniform(99999);
//                NSString *strUnique = @"CAT";
//                strUnique = [strUnique stringByAppendingString:[NSString stringWithFormat:@"%d",r]];
//                NSLog(@"%@",strUnique);
                
                NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_CREATE_UNCATEGORIESD_ALBUM];
                NSLog(@"%@",strURL);
                
                ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
                [request setDelegate:self];
                [request setRequestMethod:@"POST"];
                [request addRequestHeader:@"Content-Type" value:@"application/json"];
                [request setDidFinishSelector:@selector(submitCreateUnCatImageComp:)];
                [request setDidFailSelector:@selector(submitCreateUnCatImageFail:)];
                [request addPostValue:self.txtGDriveLinkUnCatImage.text forKey:@"google_drive_link"];
                [request addPostValue:self.txtDropboxUnCaImage.text forKey:@"dropbox_link"];
                
                [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
                [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
                //[request addPostValue:IOS forKey:@"mode"];
                [request startAsynchronous];
            }
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:@"Network connection failure, Please try again later."
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
}

-(void)submitCreateUnCatImageComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            self.strCurrentScrollView = @"4";
            self.ScrollOrderThird.hidden = YES;
            self.scrollViewFourVideoCategorised.hidden = NO;
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitCreateUnCatImageFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)clickFromPreviosCateImage:(id)sender{
    self.strCurrentScrollView = @"2";
    self.scrollViewThirdCategorised.hidden = YES;
    self.scrollOrderSecond.hidden = NO;
}

-(void)clickToNextCateImage:(id)sender{
    self.strCurrentScrollView = @"4";
    self.scrollViewThirdCategorised.hidden = YES;
    self.scrollViewFourVideoCategorised.hidden = NO;
}

-(void)clickFromPreviosCateVideo:(id)sender{
    self.strCurrentScrollView = @"3";
    if([self.strImageCateType isEqualToString:@""]){
        self.scrollViewFourVideoCategorised.hidden = YES;
        self.ScrollOrderThird.hidden = NO;
    }
    else if([self.strImageCateType isEqualToString:@"C"]){
        self.scrollViewFourVideoCategorised.hidden = YES;
        self.scrollViewThirdCategorised.hidden = NO;
    }
    else if([self.strImageCateType isEqualToString:@"NC"]){
        self.scrollViewFourVideoCategorised.hidden = YES;
        self.ScrollOrderThird.hidden = NO;
        self.viewCateUnCategorised.hidden = YES;
        self.viewUnCategorised.hidden = NO;
    }
}

-(void)clickToNextCateVideo:(id)sender{
    [self getUserDetailsAfterVideo];
}

- (IBAction)clickPreviousFive:(id)sender {
    self.strCurrentScrollView = @"4";
    self.scrollOrderConfirmation.hidden = YES;
    self.scrollViewFourVideoCategorised.hidden = NO;
}

- (IBAction)clickProccedToPay:(id)sender {
    [PlugNPlay setMerchantDisplayName:self.txtCusFirstName.text];
    //[PlugNPlay setButtonTextColor:UIColorFromRGB([self intFromHexString:_tfButtonTextColor.text])];
    //[PlugNPlay setButtonColor:UIColorFromRGB([self intFromHexString:_tfButtonColor.text])];
    //[PlugNPlay setTopTitleTextColor:UIColorFromRGB([self intFromHexString:_tfNavTitleColor.text])];
    //[PlugNPlay setTopBarColor:UIColorFromRGB([self intFromHexString:_tfTopNavColor.text])];
    
    //Customize plug and play's behaviour//optional step
    //[PlugNPlay setDisableCompletionScreen:_isCompletionDisable];
    //[PlugNPlay setExitAlertOnBankPageDisabled:_isExitAlertOnBankPageDisable];
    //[PlugNPlay setExitAlertOnCheckoutPageDisabled:_isExitAlertOnCheckoutPageDisable];
    
    //[PlugNPlay setOrderDetails:[self testOrderDetailsArray]];
    
    PUMTxnParam * txnParam = [self getTxnParam];
    //[self rememberEnteredDetails];
    
    [PlugNPlay presentPaymentViewControllerWithTxnParams:txnParam
                                        onViewController:self
                                     withCompletionBlock:^(NSDictionary *paymentResponse, NSError *error, id extraParam) {
                                         if (error) {
                                             [UIUtility toastMessageOnScreen:[error localizedDescription]];
                                         } else {
                                             NSString *message;
                                             if ([paymentResponse objectForKey:@"result"] && [[paymentResponse objectForKey:@"result"] isKindOfClass:[NSDictionary class]] ) {
                                                 message = [[paymentResponse objectForKey:@"result"] valueForKey:@"error_Message"];
                                                 if ([message isEqual:[NSNull null]] || [message length] == 0 || [message isEqualToString:@"No Error"]) {
                                                     message = [[paymentResponse objectForKey:@"result"] valueForKey:@"status"];
                                                 }
                                             }
                                             else {
                                                 message = [paymentResponse valueForKey:@"status"];
                                             }
                                             [UIUtility toastMessageOnScreen:message];
                                         }
                                     }];
}

- (IBAction)clickUncategoriesd:(id)sender {
    self.strImageCateType = @"NC";
    
    [self submitImageAlbumType];
}

- (IBAction)clickCategorised:(id)sender {
    self.strImageCateType = @"C";
    
    [self submitImageAlbumType];
}

-(void)submitImageAlbumType{
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_SUBMIT_IMAGEALBUM_CATEGORYTYPE];
        NSLog(@"%@",strURL);
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(submitImageAlbumCateTypeComp:)];
        [request setDidFailSelector:@selector(submitImageAlbumCateTypeFail:)];
        [request addPostValue:self.strImageCateType forKey:@"upload_file_type"];
        [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitImageAlbumCateTypeComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            if([self.strImageCateType isEqualToString:@"C"]){
                self.ScrollOrderThird.hidden = YES;
                self.scrollViewThirdCategorised.hidden = NO;
            }
            else{
                self.viewCateUnCategorised.hidden = YES;
                self.viewUnCategorised.hidden = NO;
            }
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)submitImageAlbumCateTypeFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

-(void)getUserDetailsAfterVideo{
    [self.navigationController.view showActivityView];
    
    NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_GET_ORDER_DETAILS];
    NSLog(@"%@",strURL);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
    [request setDelegate:self];
    [request setRequestMethod:@"POST"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request setDidFinishSelector:@selector(getOrderDetailsAfterCreateVideoAlbumLastComp:)];
    [request setDidFailSelector:@selector(getOrderDetailsAfterCreateVideoAlbumLastFail:)];
    [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
    [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
    //[request addPostValue:IOS forKey:@"mode"];
    [request startAsynchronous];
}

-(void)getOrderDetailsAfterCreateVideoAlbumLastComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            if(![[[respon objectForKey:@"details"] objectForKey:@"customer_fname"] isKindOfClass:[NSNull class]]){
                self.lblCusName.text = [[respon objectForKey:@"details"] objectForKey:@"customer_fname"];
                self.lblCusName.text = [self.lblCusName.text stringByAppendingString:@" "];
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"customer_lname"] isKindOfClass:[NSNull class]]){
                self.lblCusName.text = [self.lblCusName.text stringByAppendingString:[[respon objectForKey:@"details"] objectForKey:@"customer_lname"]];
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"customer_email"] isKindOfClass:[NSNull class]]){
                self.lblCusEmailID.text = [[respon objectForKey:@"details"] objectForKey:@"customer_email"];
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"customer_phone"] isKindOfClass:[NSNull class]]){
                self.lblCusPhNumber.text = [[respon objectForKey:@"details"] objectForKey:@"customer_phone"];
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"customer_note"] isKindOfClass:[NSNull class]]){
                self.lblCusNotes.text = [[respon objectForKey:@"details"] objectForKey:@"customer_note"];
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"preferred_subdomain_name"] isKindOfClass:[NSNull class]]){
                self.lblCusWebsiteName.text = [[respon objectForKey:@"details"] objectForKey:@"preferred_subdomain_name"];
                self.lblCusWebsiteName.text = [self.lblCusWebsiteName.text stringByAppendingString:@".cliicker.com"];
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"bride_name"] isKindOfClass:[NSNull class]]){
                self.lblBrideName.text = [[respon objectForKey:@"details"] objectForKey:@"bride_name"];
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"about_bride"] isKindOfClass:[NSNull class]]){
                self.lblBrideNotes.text = [[respon objectForKey:@"details"] objectForKey:@"about_bride"];
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"groom_name"] isKindOfClass:[NSNull class]]){
                self.lblGroomName.text = [[respon objectForKey:@"details"] objectForKey:@"groom_name"];
            }
            if(![[[respon objectForKey:@"details"] objectForKey:@"about_groom"] isKindOfClass:[NSNull class]]){
                self.lblGroomNotes.text = [[respon objectForKey:@"details"] objectForKey:@"about_groom"];
            }
            
            if([[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] isKindOfClass:[NSDictionary class]]){
                self.lblDateFirstMeet.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_1"];
                
                self.lblDateMaFixOn.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_2"];
                
                self.lblDateEngagedOn.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_3"];
                
                self.lblDateLegMarOn.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_4"];
                
                self.lblDateSocMa.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_5"];
                
                self.lblDateRec.text = [[[respon objectForKey:@"details"] objectForKey:@"special_days_value"] objectForKey:@"sp_6"];
            }
            
            if(![[[respon objectForKey:@"details"] objectForKey:@"theme_name"] isKindOfClass:[NSNull class]]){
                self.lblAlbumNameLast.text = [[respon objectForKey:@"details"] objectForKey:@"theme_name"];
                if(![[[respon objectForKey:@"details"] objectForKey:@"theme_code"] isKindOfClass:[NSNull class]]){
                    self.lblTemplateIDLast.text = [self.lblTemplateIDLast.text stringByAppendingString:[[respon objectForKey:@"details"] objectForKey:@"theme_code"]];
                }
                
                if(![[[respon objectForKey:@"details"] objectForKey:@"theme_thumbimage_url"] isKindOfClass:[NSNull class]]){
                    NSString *strImageURL = [[respon objectForKey:@"details"] objectForKey:@"theme_thumbimage_url"];
                    self.imgVwAlbumLast.imageURL = [NSURL URLWithString:strImageURL];
                }
            }
            
            if(![[[respon objectForKey:@"details"] objectForKey:@"order_amount"] isKindOfClass:[NSNull class]]){
                self.ItemAmount = [[[respon objectForKey:@"details"] objectForKey:@"order_amount"] intValue];
                self.lblCostItemTotal.text = [[respon objectForKey:@"details"] objectForKey:@"order_amount"];
                self.lblCostItemTotal.text = [self.lblCostItemTotal.text stringByAppendingString:@" (INR)"];
                
                int gstTax = ((self.ItemAmount * 18)/100);
                
                self.lblCoseGSTTax.text = [NSString stringWithFormat:@"%d",gstTax];
                self.lblCoseGSTTax.text = [self.lblCoseGSTTax.text stringByAppendingString:@" (INR)"];
                
                self.OrderAmount = gstTax + self.ItemAmount;
                
                self.lblCostOrderTotal.text = [NSString stringWithFormat:@"%d",self.OrderAmount];
                self.lblCostOrderTotal.text = [self.lblCostOrderTotal.text stringByAppendingString:@" (INR)"];
            }
            
            self.strCurrentScrollView = @"5";
            self.scrollViewFourVideoCategorised.hidden = YES;
            self.scrollOrderConfirmation.hidden = NO;
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)getOrderDetailsAfterCreateVideoAlbumLastFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

#pragma mark TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrOrderHisList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 250;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    OrderHistoryTableViewCell *cell = (OrderHistoryTableViewCell*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = (OrderHistoryTableViewCell*)[[[NSBundle mainBundle] loadNibNamed:@"OrderHistoryTableViewCell" owner:nil options:nil] firstObject];
    }
    
    if([self.arrOrderHisList count]>0){
        cell.lblDomainName.text = [[self.arrOrderHisList objectAtIndex:indexPath.row] objectForKey:@"preferred_subdomain_name"];
        cell.lblDomainName.text = [cell.lblDomainName.text stringByAppendingString:@".cliicker.com"];
        cell.lblClientName.text = [cell.lblClientName.text stringByAppendingString:[[self.arrOrderHisList objectAtIndex:indexPath.row] objectForKey:@"customer_fname"]];
        cell.lblClientName.text = [cell.lblClientName.text stringByAppendingString:@" "];
        cell.lblClientName.text = [cell.lblClientName.text stringByAppendingString:[[self.arrOrderHisList objectAtIndex:indexPath.row] objectForKey:@"customer_lname"]];
        cell.lblTempID.text = [cell.lblTempID.text stringByAppendingString:[[self.arrOrderHisList objectAtIndex:indexPath.row] objectForKey:@"theme_code"]];
        
        NSString *strImageURL = [[self.arrOrderHisList objectAtIndex:indexPath.row] objectForKey:@"theme_thumbimage_url"];
        cell.imgVwTemplate.imageURL = [NSURL URLWithString:strImageURL];
    }
    else{
    }
    
    return cell;
}

# pragma mark TextView Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == self.txtVwNotes){
        if ([textView.text isEqualToString:@"Note (If any)"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
            self.lblNote.hidden = NO;
        }
    }
    else if(textView == self.txtVwAboutBride){
        if ([textView.text isEqualToString:@"About the bride..."]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
            self.lblAboutBride.hidden = NO;
        }
    }
    else if(textView == self.txtVwAboutGroom){
        if ([textView.text isEqualToString:@"About the groom..."]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
            self.lblAboutGroom.hidden = NO;
        }
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == self.txtVwNotes){
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Note (If any)";
            textView.textColor = [UIColor lightGrayColor];
            self.lblNote.hidden = YES;
        }
    }
    else if(textView == self.txtVwAboutBride){
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"About the bride...";
            textView.textColor = [UIColor lightGrayColor];
            self.lblAboutBride.hidden = YES;
        }
    }
    else if(textView == self.txtVwAboutGroom){
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"About the groom...";
            textView.textColor = [UIColor lightGrayColor];
            self.lblAboutGroom.hidden = YES;
        }
    }
    [textView resignFirstResponder];
}

#pragma mark TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField==self.txtCusFirstName){
        [self.txtCusLastName becomeFirstResponder];
    }
    else if(textField==self.txtCusLastName){
        [self.txtCusEmail becomeFirstResponder];
    }
    else if(textField==self.txtCusEmail){
        [self.txtCusContactNo becomeFirstResponder];
    }
    else if(textField==self.txtCusContactNo){
        [self.txtVwNotes becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder] ;
    }
    
    return YES;
}

- (IBAction)clickDashBoard:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"dashboard"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnProfile:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"profile"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnShop:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Shop" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"shop"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickbtnFollow:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Follow" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"follow"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (BOOL) validateAlphanumericDash: (NSString *) candidate {
    NSMutableCharacterSet *characterSet = [NSMutableCharacterSet alphanumericCharacterSet];
    [characterSet addCharactersInString:@"-"];
    return [self validateStringInCharacterSet:candidate characterSet:characterSet];
}

- (BOOL) validateStringInCharacterSet: (NSString *) string characterSet: (NSMutableCharacterSet *) characterSet {
    // Since we invert the character set if it is == NSNotFound then it is in the character set.
    return ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) ? NO : YES;
}

-(void)clickBackCateImageType:(id)sender{
    self.strImageCateType = @"";
    
    [self deleteImageAlbumTypeCate];
}

-(void)deleteImageAlbumTypeCate{
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_DELETE_IMAGEALBUM_CATEGORYTYPE];
        NSLog(@"%@",strURL);
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(deleteImageAlbumCateTypeCComp:)];
        [request setDidFailSelector:@selector(deleteImageAlbumCateCTypeFail:)];
        [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)deleteImageAlbumCateTypeCComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            self.scrollViewThirdCategorised.hidden = YES;
            self.ScrollOrderThird.hidden = NO;
            self.viewCateUnCategorised.hidden = NO;
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)deleteImageAlbumCateCTypeFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

- (IBAction)clickBackUnCatImageType:(id)sender {
    self.strImageCateType = @"";
    
    [self deleteImageAlbumType];
}

-(void)deleteImageAlbumType{
    if (!([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)){
        [self.navigationController.view showActivityView];
        
        NSString* strURL = [NSString stringWithFormat:@"%@%@",BASE_URL,CLIICKER_DELETE_IMAGEALBUM_CATEGORYTYPE];
        NSLog(@"%@",strURL);
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
        [request setDelegate:self];
        [request setRequestMethod:@"POST"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        [request setDidFinishSelector:@selector(deleteImageAlbumCateTypeComp:)];
        [request setDidFailSelector:@selector(deleteImageAlbumCateTypeFail:)];
        [request addPostValue:self.strOrderID forKey:@"order_id"];//self.strOrderID
        [request addPostValue:@"CLIICKERS2018" forKey:@"apitoken"];
        //[request addPostValue:IOS forKey:@"mode"];
        [request startAsynchronous];
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"Network connection failure, Please try again later."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)deleteImageAlbumCateTypeComp:(ASIHTTPRequest *)req{
    
    [self.navigationController.view hideActivityView];
    
    int statusCode = [req responseStatusCode];
    NSLog(@"Response : %d",statusCode) ;
    NSData *responseData = [req responseData];
    NSString *resp = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSDictionary *respon = [resp JSONValue];
    NSLog(@"%@", respon);
    
    if(statusCode == 200){
        if([[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_action_success"]] isEqualToString:@"1"] && [[NSString stringWithFormat:@"%@",[respon objectForKey:@"api_syntax_success"]] isEqualToString:@"1"]){
            
            self.viewCateUnCategorised.hidden = NO;
            self.viewUnCategorised.hidden = YES;
        }
        else{
            [ISMessages showCardAlertWithTitle:@"Cliicker"
                                       message:[respon objectForKey:@"error"]
                                      duration:5.0
                                   hideOnSwipe:YES
                                     hideOnTap:YES
                                     alertType:ISAlertTypeWarning
                                 alertPosition:ISAlertPositionTop
                                       didHide:^(BOOL finished) {
                                           NSLog(@"Alert did hide.");
                                       }];
        }
    }
    else{
        [ISMessages showCardAlertWithTitle:@"Cliicker"
                                   message:@"There was a problem processing your request, Please try after sometime."
                                  duration:5.0
                               hideOnSwipe:YES
                                 hideOnTap:YES
                                 alertType:ISAlertTypeWarning
                             alertPosition:ISAlertPositionTop
                                   didHide:^(BOOL finished) {
                                       NSLog(@"Alert did hide.");
                                   }];
    }
}

-(void)deleteImageAlbumCateTypeFail:(ASIHTTPRequest *)req{
    [self.navigationController.view hideActivityView];
    [ISMessages showCardAlertWithTitle:@"Cliicker"
                               message:@"There was a problem processing your request, Please try after sometime."
                              duration:5.0
                           hideOnSwipe:YES
                             hideOnTap:YES
                             alertType:ISAlertTypeWarning
                         alertPosition:ISAlertPositionTop
                               didHide:^(BOOL finished) {
                                   NSLog(@"Alert did hide.");
                               }];
}

#pragma mark - Helper methods

-(PUMTxnParam*)getTxnParam{
    PUMTxnParam *txnParam= [[PUMTxnParam alloc] init];
    //    PUMRequestParams *params = [PUMRequestParams sharedParams];
    
    //Pass these parameters
    txnParam.phone = self.txtCusContactNo.text;
    txnParam.email = self.txtCusEmail.text;
    txnParam.amount = [NSString stringWithFormat:@"%d",self.OrderAmount];
    txnParam.environment = PUMEnvironmentTest;
    txnParam.firstname = self.txtCusFirstName.text;
    txnParam.key =  kMerchantKey;
    txnParam.merchantid = kMerchantID;
    //txnParam.txnid = [NSString stringWithFormat:@"0nf7%@",[self getRandomString:4]];
    txnParam.txnID = @"12";
    txnParam.surl = @"https://www.payumoney.com/mobileapp/payumoney/success.php";
    txnParam.furl = @"https://www.payumoney.com/mobileapp/payumoney/failure.php";
    txnParam.productInfo = @"iPhone7";
    
    txnParam.udf1 = @"as";
    txnParam.udf2 = @"sad";
    txnParam.udf3 = @"";
    txnParam.udf4 = @"";
    txnParam.udf5 = @"";
    txnParam.udf6 = @"";
    txnParam.udf7 = @"";
    txnParam.udf8 = @"";
    txnParam.udf9 = @"";
    txnParam.udf10 = @"";
    txnParam.hashValue = [self getHashForPaymentParams:txnParam];
    //    params.hashValue = @"FDBE365FCE1133A9D7091BEE25D032910B7CE8C2F1AA02F86179209EF4A1652CCB35159ECAEFEDD8DE404E2B27809F0B487749DC8DBD9D264E52142B6D3C3270";
    return txnParam;
}

- (NSString *)getRandomString:(NSInteger)length
{
    NSMutableString *returnString = [NSMutableString stringWithCapacity:length];
    
    NSString *numbers = @"0123456789";
    
    // First number cannot be 0
    [returnString appendFormat:@"%C", [numbers characterAtIndex:(arc4random() % ([numbers length]-1))+1]];
    
    for (int i = 1; i < length; i++) {
        [returnString appendFormat:@"%C", [numbers characterAtIndex:arc4random() % [numbers length]]];
    }
    
    return returnString;
}

//TODO: get rid of this function for test environemnt
-(NSString*)getHashForPaymentParams:(PUMTxnParam*)txnParam {
    NSString *salt = kMerchantSalt;
    NSString *hashSequence = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@|%@",txnParam.key,txnParam.txnID,txnParam.amount,txnParam.productInfo,txnParam.firstname,txnParam.email,txnParam.udf1,txnParam.udf2,txnParam.udf3,txnParam.udf4,txnParam.udf5,txnParam.udf6,txnParam.udf7,txnParam.udf8,txnParam.udf9,txnParam.udf10, salt];
    
    NSString *hash = [[[[[self createSHA512:hashSequence] description]stringByReplacingOccurrencesOfString:@"<" withString:@""]stringByReplacingOccurrencesOfString:@">" withString:@""]stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return hash;
}

- (NSString*) createSHA512:(NSString *)source {
    
    const char *s = [source cStringUsingEncoding:NSASCIIStringEncoding];
    
    NSData *keyData = [NSData dataWithBytes:s length:strlen(s)];
    
    uint8_t digest[CC_SHA512_DIGEST_LENGTH] = {0};
    
    CC_SHA512(keyData.bytes, (CC_LONG)keyData.length, digest);
    
    NSData *output = [NSData dataWithBytes:digest length:CC_SHA512_DIGEST_LENGTH];
    NSLog(@"Hash output --------- %@",output);
    NSString *hash =  [[[[output description]stringByReplacingOccurrencesOfString:@"<" withString:@""]stringByReplacingOccurrencesOfString:@">" withString:@""]stringByReplacingOccurrencesOfString:@" " withString:@""];
    return hash;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
