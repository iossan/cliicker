//
//  RequestHandler.h
//  Omlis
//
//  Created by Enduetech on 11/04/13.
//  Copyright (c) 2013 Enduetech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
@protocol RequestHandlerDelegates <NSObject>

-(NSDictionary*)postData:(int) anId;
-(void)notifyRequesterWithData:(id)aData andId:(int)reqId;
-(void)notifyRequesterWithData:(id)aData andId:(int)reqId andAlbumId:(NSString*)albumId;
- (void)startLoaderWithLabel:(NSString *)label;
-(void)stopLoader;
- (IBAction)showError:(NSString *)errorMsg;
@end

@interface RequestHandler : NSObject<ASIHTTPRequestDelegate>
+(RequestHandler*) getInstance;
-(void)sendRequestFor:(int)reqId andRequester:(id<RequestHandlerDelegates>)aRequester;

-(void)sendRequestWithJSON:(int)reqId andRequester:(id<RequestHandlerDelegates>)aRequester;
-(void)sendRequestFor:(int)reqId andRequester:(id<RequestHandlerDelegates>)aRequester andAlbumId:(NSString *)albumID;
-(void)sendRequestForAppdelegate:(int)reqId andRequester:(id<RequestHandlerDelegates>)aRequester;
@end
