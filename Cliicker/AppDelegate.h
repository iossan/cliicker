//
//  AppDelegate.h
//  Cliicker
//
//  Created by Santanu Mondal on 28/12/18.
//  Copyright © 2018 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

