//
//  main.m
//  Cliicker
//
//  Created by Santanu Mondal on 28/12/18.
//  Copyright © 2018 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
