//
//  Constant.h
//  HSComply
//
//  Created by Santanu Mondal on 07/06/17.
//  Copyright © 2017 Santanu Mondal. All rights reserved.
//

#ifndef __CONST_H_
#define __CONST_H_

#define CONNECTION_TIME_OUT 180

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) // iPhone and       iPod touch style UI

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define IS_IPHONE_5_IOS7 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_6_IOS7 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0f)
#define IS_IPHONE_6P_IOS7 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0f)
#define IS_IPHONE_4_AND_OLDER_IOS7 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height < 568.0f)

#define IS_IPHONE_5_IOS8 (IS_IPHONE && ([[UIScreen mainScreen] nativeBounds].size.height/[[UIScreen mainScreen] nativeScale]) == 568.0f)
#define IS_IPHONE_6_IOS8 (IS_IPHONE && ([[UIScreen mainScreen] nativeBounds].size.height/[[UIScreen mainScreen] nativeScale]) == 667.0f)
#define IS_IPHONE_6P_IOS8 (IS_IPHONE && ([[UIScreen mainScreen] nativeBounds].size.height/[[UIScreen mainScreen] nativeScale]) == 736.0f)
#define IS_IPHONE_4_AND_OLDER_IOS8 (IS_IPHONE && ([[UIScreen mainScreen] nativeBounds].size.height/[[UIScreen mainScreen] nativeScale]) < 568.0f)

#define IS_IPHONE_5 ( ( [ [ UIScreen mainScreen ] respondsToSelector: @selector( nativeBounds ) ] ) ? IS_IPHONE_5_IOS8 : IS_IPHONE_5_IOS7 )
#define IS_IPHONE_6 ( ( [ [ UIScreen mainScreen ] respondsToSelector: @selector( nativeBounds ) ] ) ? IS_IPHONE_6_IOS8 : IS_IPHONE_6_IOS7 )
#define IS_IPHONE_6P ( ( [ [ UIScreen mainScreen ] respondsToSelector: @selector( nativeBounds ) ] ) ? IS_IPHONE_6P_IOS8 : IS_IPHONE_6P_IOS7 )
#define IS_IPHONE_4_AND_OLDER ( ( [ [ UIScreen mainScreen ] respondsToSelector: @selector( nativeBounds ) ] ) ? IS_IPHONE_4_AND_OLDER_IOS8 : IS_IPHONE_4_AND_OLDER_IOS7 )

#define IS_OS_5_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0)
#define IS_OS_6_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IS_OS_9_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)

#define IOS @"1"

#define BASE_URL        @"https://cliicker.com/restserver/"

#define CLIICKER_LOGIN_URL   @"photographer/photographer_login/"
#define CLIICKER_USER_DETAILS   @"photographer/get_photographer_details/"
#define CLIICKER_USER_EDIT_DETAILS @"photographer/edit_profile/"
#define CLIICKER_TEMPLATE_LIST   @"template/get_templates_lists/"


#define CLIICKER_SUBMIT_CUSTOMER_DETAILS @"order/submit_customer_details/"
#define CLIICKER_VALIDATE_SUBDOMAIN @"order/check_domain_availability/"
#define CLIICKER_SUBMIT_ALBUM_DETAILS @"order/submit_album_details/"
#define CLIICKER_GET_ORDER_DETAILS @"order/get_order_details/"

#define CLIICKER_SUBMIT_IMAGEALBUM_CATEGORYTYPE @"order/submit_image_category_type/"
#define CLIICKER_DELETE_IMAGEALBUM_CATEGORYTYPE @"order/remove_image_upload_type/"
#define CLIICKER_CREATE_CATEGORIESD_ALBUM @"order/add_image_category_data/"
#define CLIICKER_UPDATE_CATEGORIESD_ALBUM @"order/update_image_category_data/"
#define CLIICKER_DELETE_CATEGORIESD_ALBUM @"order/delete_image_category/"
#define CLIICKER_CREATE_UNCATEGORIESD_ALBUM @"order/submit_noncategory_image_links/"

#define CLIICKER_CREATE_CATEGORIESD_VIDEO_ALBUM @"order/add_video_data"
#define CLIICKER_UPDATE_CATEGORIESD_VIDEO_ALBUM @"order/update_video_data"
#define CLIICKER_DELETE_CATEGORIESD_VIDEO_ALBUM @"order/delete_video_record"

#define CLIICKER_ORDER_HISTORY @"order/get_past_orders"
#define CLIICKER_PGLIST @"photographer/get_photographer_lists/"

#define CLIICKER_FOLLOW_LIST @"photographer/get_photographer_followers_followings_left/"
#define CLIICKER_FOLLOW_photographer @"photographer/api_ajax_pg_follow_status_changed/"
#define CLIICKER_USER_PHOTOLIST @"photographer/get_photographer_uploaded_media_with_pagination/"

#endif
