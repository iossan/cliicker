//
//  RequestHandler.m
//  Omlis
//
//  Created by Enduetech on 11/04/13.
//  Copyright (c) 2013 Enduetech. All rights reserved.
//

#import "RequestHandler.h"
#import "ASIFormDataRequest.h"
#import "ASIDataDecompressor.h"
#import "JSON.h"

@implementation RequestHandler

+(RequestHandler*) getInstance
{
    static RequestHandler* instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[RequestHandler alloc] init];
    });
    
    return instance;
}

-(void)sendRequestWithJSON:(int)reqId andRequester:(id<RequestHandlerDelegates>)aRequester{
    
    NSError *error;
    NSDictionary *dictPostData=[aRequester postData:reqId];
    NSLog(@"dictPostData:%@",dictPostData);
    //BOOL xx = [NSJSONSerialization isValidJSONObject:dictPostData];
    NSData *data=[NSJSONSerialization dataWithJSONObject:dictPostData options:NSJSONWritingPrettyPrinted error:&error];
    NSMutableData * mData=[[NSMutableData alloc] init];
    [mData appendData:data];
    
    //[aRequester startLoaderWithLabel:@"Loading"];
    NSString *urlString=[self urlForId:reqId];
    NSLog(@"urlString:%@",urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    //__block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    __block ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    request.postBody=mData;
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    
    [request setCompletionBlock:^{
        NSLog(@"response Complete");
        // Use when fetching text data
        NSString *responseString = [request responseString];
        // Use when fetching binary data
        //NSData *responseData = [request responseData];
        NSDictionary *respon = [responseString JSONValue];
        NSLog(@"response JSON:%@",respon);
        NSLog(@"response XML:%@",responseString);
        [aRequester stopLoader];
        [aRequester notifyRequesterWithData:respon andId:reqId];
        
    }];
    [request setFailedBlock:^{
         NSLog(@"response Failed");
        NSError *error = [request error];
        NSLog(@"response:%@",[error description]);
        [aRequester stopLoader];
        [aRequester showError:[error description]];
    }];
    [request startAsynchronous];
    
}

-(void)sendRequestFor:(int)reqId andRequester:(id<RequestHandlerDelegates>)aRequester{
   
    NSDictionary *dictPostData=[self addCommonParameter:[aRequester postData:reqId]];
   
    
    NSString *urlString=[self urlForId:reqId];
                            
    NSURL *url = [NSURL URLWithString:urlString];
    //__block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    __block ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.timeOutSeconds=30;
    for (NSString *key in [dictPostData allKeys]) {
        
//        if([key isEqualToString:@"event_image"]){
//            NSLog(@"%@",[dictPostData valueForKey:key]);
//            [request addData:UIImagePNGRepresentation([dictPostData valueForKey:key]) withFileName:@"xyz.png" andContentType:@"image/png" forKey:key];
//        }
//        else{
            [request setPostValue:[dictPostData valueForKey:key] forKey:key];
       // }
    }
    
        [request setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request responseString];
        // Use when fetching binary data
        //NSData *responseData = [request responseData];
        NSDictionary *respon = [responseString JSONValue];
        NSLog(@"response JSON:%@",respon);
        NSLog(@"response XML:%@",[request responseString]);
        [aRequester stopLoader];
        [aRequester notifyRequesterWithData:respon andId:reqId];
            
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"response:%@",[error description]);
        [aRequester stopLoader];
        [aRequester showError:[error description]];
    }];
    [request startAsynchronous];
    
}
-(void)sendRequestForAppdelegate:(int)reqId andRequester:(id<RequestHandlerDelegates>)aRequester{
    
    NSDictionary *dictPostData=[self addCommonParameter:[aRequester postData:reqId]];
    
    
    NSString *urlString=[self urlForId:reqId];
    
    NSURL *url = [NSURL URLWithString:urlString];
    //__block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    __block ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.timeOutSeconds=30;
    for (NSString *key in [dictPostData allKeys]) {
        
        //        if([key isEqualToString:@"event_image"]){
        //            NSLog(@"%@",[dictPostData valueForKey:key]);
        //            [request addData:UIImagePNGRepresentation([dictPostData valueForKey:key]) withFileName:@"xyz.png" andContentType:@"image/png" forKey:key];
        //        }
        //        else{
        [request setPostValue:[dictPostData valueForKey:key] forKey:key];
        // }
    }
    
    [request setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request responseString];
        // Use when fetching binary data
        //NSData *responseData = [request responseData];
        NSDictionary *respon = [responseString JSONValue];
        NSLog(@"response JSON:%@",respon);
        NSLog(@"response XML:%@",[request responseString]);
        [aRequester notifyRequesterWithData:respon andId:reqId];
        
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"response:%@",[error description]);
        [aRequester showError:[error description]];
    }];
    [request startAsynchronous];
    
}
-(void)sendRequestFor:(int)reqId andRequester:(id<RequestHandlerDelegates>)aRequester andAlbumId:(NSString *)albumID{
    
    
    NSDictionary *dictPostData=[self addCommonParameter:[aRequester postData:reqId]];
    
    
    NSString *urlString=[self requestURL:albumID];
    
    NSURL *url = [NSURL URLWithString:urlString];
    //__block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    __block ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    for (NSString *key in [dictPostData allKeys]) {
        [request setPostValue:[dictPostData valueForKey:key] forKey:key];
    }
    
    [request setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request responseString];
        // Use when fetching binary data
        //NSData *responseData = [request responseData];
        NSDictionary *respon = [responseString JSONValue];
        NSLog(@"response JSON:%@",respon);
       //NSLog(@"response XML:%@",[request responseString]);
        [aRequester stopLoader];
        [aRequester notifyRequesterWithData:respon andId:reqId andAlbumId:albumID];
        
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"response:%@",[error description]);
        [aRequester stopLoader];
        [aRequester showError:[error description]];
    }];
    [request startAsynchronous];
    
}

-(NSDictionary*)addCommonParameter:(NSDictionary*)params{
    
    NSMutableDictionary* aDict = [NSMutableDictionary dictionaryWithDictionary:params];
    NSLog(@"Request Params: %@",aDict);
    return aDict;
    
}

-(NSString*)querryString:(NSDictionary*) aDict{
    NSMutableString* as = [NSMutableString string];
    for (NSString* aKey in [aDict allKeys]) {
        NSString* aVal = [aDict objectForKey:aKey];
        [as appendString:[NSString stringWithFormat:@"%@=%@",aKey,aVal]];
    }
    NSLog(@"Post String : %@",as);
    
    return [NSString stringWithString:as];
}
-(NSString *)urlForId:(int)reqId {
    switch (reqId) {
        default:
            return @"";
            break;
    }
    return @"";
}

-(NSString*)requestURL:(NSString *)idNo{
    
    NSString *stringURL=[NSString stringWithFormat:@"https://graph.facebook.com/%@/photos",idNo];
    return stringURL;
}
@end
